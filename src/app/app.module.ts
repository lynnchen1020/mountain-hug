import { ShareModule } from 'src/app/module/share/share.module';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FacebookModule } from 'ngx-facebook';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    HttpClientModule,
    ShareModule,
    AppRoutingModule,
    FontAwesomeModule,
    FacebookModule.forRoot()
  ],
  // entryComponents: [DatePickerComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
