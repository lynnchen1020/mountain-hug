import { GtmService } from './service/gtm.service';
import { MetaDataService } from './service/meta-data.service';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { FacebookService, InitParams } from 'ngx-facebook';
import { filter, map, mergeMap } from 'rxjs';
import { environment } from 'src/environments/environment';

// export interface IOrder {
//   timestampUID: string;
//   name: string;
//   mobile: string;
//   email: string;
//   startDate: string;
//   endDate: string;
//   nights: number;
//   tents: number;
//   people: number;
//   fee: number;
//   paid?: boolean;
// }


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {

  constructor(
    private facebookService: FacebookService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private metaDataService: MetaDataService,
    private gtmService: GtmService
  ) {}

  ngOnInit(): void {
    this.gtmService.init();
    this.bindEvent();
    if(environment.production) {
      this.initFacebookService();
    }
  }

  bindEvent() {
    // SSR 可以直接使用這個 seo metadata 的方法
    this.router.events.pipe(
      filter(e => e instanceof NavigationEnd),
      map(e => this.activatedRoute),
      map((route) => {
        while (route.firstChild) route = route.firstChild;
        return route;
      }),
      filter((route) => route.outlet === 'primary'),
      mergeMap((route) => route.data),
    ).subscribe((data: any) => {
      let seoData = data['seo'];
      this.metaDataService.updateTitle(`山的抱抱 - ${seoData['title']}`);
      this.metaDataService.updateMetaTags(seoData['metaTags']);
    });
  }

  private initFacebookService(): void {
    const initParams: InitParams = { xfbml:true, version:'v3.2'};
    this.facebookService.init(initParams);
  }




  // handleDateStart($event: any) {
  //   this.dateStart = $event;
  //   // NOTE: 若起始日變動，會檢查結束日，若結束日小於起始日，應以起始日為值
  //   const start: Date = new Date($event);
  //   if(this.dateEnd) {
  //     const end: Date = new Date(this.dateEnd);
  //     if (end <= start) {
  //       const month = start.getMonth() + 1 < 10 ? `0${start.getMonth() + 1}` : `${start.getMonth() + 1}`;
  //       const data = start.getDate() < 10 ? `0${start.getDate()}` : `${start.getDate()}`;
  //       const newEnd = `${start.getFullYear()}-${month}-${data}`;
  //       this.dateEnd = newEnd;
  //     }
  //   }
  // }

  // handleDateEnd($event: any) {
  //   this.dateEnd = $event;
  // }

  // hadleDataSearchStatusChange() {
  //   this.sreachStatus = SreachStatus.DATE;
  // }
}
