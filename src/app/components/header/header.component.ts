import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ElementService } from 'src/app/service/element.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  @Input() mode? = '';
  isOpenNav: boolean;

  constructor(private router: Router, private elementService: ElementService) {
    this.isOpenNav = false;
  }

  ngOnInit(): void {
  }

  public openNav() {
    this.elementService.isNotScrollable = true
    this.isOpenNav = true
  }

  close() {
    this.elementService.isNotScrollable = false;
    this.isOpenNav = false;
  }

  navTo(route: string) {
    this.elementService.isNotScrollable = false
    this.isOpenNav = false;
    this.router.navigate([route])
  }

}
