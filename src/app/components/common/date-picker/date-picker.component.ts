import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderType } from 'src/app/enum/order-type';
import { cloneDeep, getNumber, getString, min } from 'src/app/helper/lodash';
import { TimeService } from 'src/app/service/time.service';

interface DatePickerItem {
  date: string;
  disable: boolean;
  timestamp: string;
}

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.sass']
})
export class DatePickerComponent implements OnInit, OnChanges {
  @Output() dateChanged = new EventEmitter<string>();
  @Input() bookedDate: number[] = [];
  @Input() isTypeStart: boolean;
  @Input() isOrder?: boolean;
  @Input() isShowSelectedDate? = false;
  @Input() stayType?: string; // 'tent' | 'homestay' | 'tent-homestay'
  @Input() campArea?: string; // 'A1 | A2 | A'
  @Input() dateCheckIn!: number
  OrderType = OrderType;
  tuesdays: number[] = []; // 星期二公休日
  fridays: number[] = []; // 非包場客人不能續訂的日期
  finalDate: number = 0;

  // 日期格式2020-05-22
  private _value = '';
  @Input()
  set value(input_data: string) {
    this._value = this.parseDateString(input_data);
  }
  get value(): string {
    return this._value;
  }

  // 日期格式2020-05-22
  private _minDate = '';
  @Input()
  set minDate(input_data: string) {
    this._minDate = this.parseDateString(input_data);
  }
  get minDate(): string {
    return this._minDate;
  }

  // 日期格式2020-05-22
  private _maxDate = '';
  @Input()
  set maxDate(input_data: string) {
    this._maxDate = this.parseDateString(input_data);
  }
  get maxDate(): string {
    return this._maxDate;
  }

  showValue = '';
  isShowPicker = true;
  pickerDays: DatePickerItem[] = [];
  dayTS: number;

  get year(): number {
    return getNumber(this.showValue.split('-'), '0');
  }

  get month(): number {
    return getNumber(this.showValue.split('-'), '1');
  }

  get day(): number {
    const time = getString(this.showValue.split(' '), '0');
    return getNumber(time.split('-'), '2');
  }

  constructor(public timeService: TimeService, private router: Router, private route: ActivatedRoute) {
    this.isTypeStart = false;
    this.dayTS = 86400000;
  }

  ngOnInit() {
    // this.showDatePicker();
    this.showValue = this.value;
    this.pickerDays = this.getPickerDays(this.showValue);

    this.getDaysInMonth(new Date(this.dateCheckIn).getMonth(),new Date(this.dateCheckIn).getFullYear())

  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes['bookedDate']) {
      this.getAvailableFinalDate();
    }
  }


  getDaysInMonth(month: number, year: number) {
    var date = new Date(year, month, 1);
    var date2 = new Date(year, month, 1);
    const nextMonth = date.getMonth() + 2

    // 算出星期二
    var days = [];
    while (date.getMonth() === month) {
      days.push(new Date(date));
      date.setDate(date.getDate() + 1);
    }
    while (date.getMonth() + 1 === nextMonth) {
      days.push(new Date(date));
      date.setDate(date.getDate() + 1);
    }
    days = days.filter(day => {
      return day.getDay() === 2;
    })
    this.tuesdays = days.filter(day => {
      return day.getDay() === 2;
    }).map(d => new Date(d).getTime())


    // 算出星期五
    var firdays = [];
    while (date2.getMonth() === month) {
      firdays.push(new Date(date2));
      date2.setDate(date2.getDate() + 1);
    }
    while (date2.getMonth() + 1 === nextMonth) {
      firdays.push(new Date(date2));
      date2.setDate(date2.getDate() + 1);
    }

    firdays = firdays.filter(day => {
      return day.getDay() === 5;
    })

    this.fridays = firdays.filter(day => {
      return day.getDay() === 5;
    }).map(d => new Date(d).getTime())
    return days;
  }

  showDatePicker() {
    this.isShowPicker = true;
    this.showValue = this.value;
    this.pickerDays = this.getPickerDays(this.showValue);
  }

  hideDatePicker() {
    this.isShowPicker = false;
  }

  handleMonth(count: number) {
    if (this.isDisableMonth(count)) {
      return;
    }
    const d = new Date(this.showValue);
    const currentMonth = d.getMonth();
    d.setDate(20);
    d.setMonth(currentMonth + count);
    const newYear = d.getFullYear();
    const newMonth = d.getMonth() + 1 < 10 ? `0${d.getMonth() + 1}` : `${d.getMonth() + 1}`;
    const newLastDate = this.getLastDateInThisMonth(`${newYear}-${newMonth}-01`).getDate();
    const selectedDate = Number(this.value.split('-')[2]);
    const newDate = newLastDate < selectedDate ? newLastDate : selectedDate;
    const newDateParse = newDate < 10 ? `0${newDate}` : `${newDate}`;
    this.showValue = `${newYear}-${newMonth}-${newDateParse}`;
    this.pickerDays = this.getPickerDays(this.showValue);
  }

  stopPropagation(event: { preventDefault: () => void; returnValue: boolean; }) {
    if (event.preventDefault) {
      event.preventDefault();
    }
    if (event.returnValue) {
      event.returnValue = false;
    }
  }

  changeDay(date: DatePickerItem) {
    if (date.disable === true || date.date === '') {
      return;
    }
    const month = this.month < 10 ? `0${this.month}` : `${this.month}`;
    const day = Number(date.date) < 10 ? `0${date.date}` : `${date.date}`;

    // TypeStart
    if(this.isTypeStart) {
      // this.dateChanged.emit(`${this.year}-${month}-${day}`);

      if(this.stayType === OrderType.TENT) {
        this.router.navigate([`/start-date/${this.campArea}/order`], {
          queryParams: {
            startDate: `${this.year}-${month}-${day}`
          }
        })
      } else if (this.stayType === OrderType.HOMESTAY) {
        this.router.navigate([`/homestay-start-date/homestay-order`], {
          queryParams: {
            startDate: `${this.year}-${month}-${day}`
          }
        })
      } else {
        this.router.navigate([`/tent-homestay-start-date/tent-homestay-order`], {
          queryParams: {
            startDate: `${this.year}-${month}-${day}`
          }
        })
      }
    } else {
      this.hideDatePicker();
      this.dateChanged.emit(`${this.year}-${month}-${day}`);
    }
  }

  // custom 假日只開放包場客人
  checkTentHomestayEnabled(ts: string) {
    return new Date(+ts).getDay() === 5 || new Date(+ts).getDay() === 6;
  }

  // custom 公休日星期二，未來可以新增特殊休假日(只有顯示用)
  isDayOff(ts: string) {
    return new Date(+ts).getDay() === 2;
  }

  // custom order頁面的離開日期可以選擇客滿日期
  clickableBookedLeaveDate(bookedDate: any[], pickerDayTimestamp: string, isOrder: boolean) {
    if(isOrder) {
      return bookedDate.includes(pickerDayTimestamp); //pickerDayTimestamp 故意型別不對 hack
    } else {
      return bookedDate.includes(+pickerDayTimestamp);
    }
  }

  getPickerDays(date: string): DatePickerItem[] {
    const days: DatePickerItem[] = [];
    const lastDay = this.getLastDateInThisMonth(date);
    this.fillEmptyDays(lastDay.getDate(), lastDay.getDay(), days);
    this.setDaysFromThisMonth(lastDay.getDate(), days);
    return days;
  }

  isDisableMonth(count: number) {
    const d = new Date(this.showValue);
    const currentMonth = d.getMonth();
    d.setMonth(currentMonth + count);
    const newYear = d.getFullYear();
    const newMonth = d.getMonth() + 1 < 10 ? `0${d.getMonth() + 1}` : `${d.getMonth() + 1}`;

    const clickedMonth = {
      year: Number(newYear),
      month: Number(newMonth)
    };
    const minYear = Number(this.minDate.split('-')[0]);
    const maxYear = Number(this.maxDate.split('-')[0]);
    const minMonth = Number(this.minDate.split('-')[1]);
    const maxMonth = Number(this.maxDate.split('-')[1]);

    if (count < 0) {
      if (this.minDate) {
        if (clickedMonth.year < minYear) {
          return true;
        }
        if (clickedMonth.year === minYear && clickedMonth.month < minMonth) {
          return true;
        }
      }
    }

    if (count > 0) {
      if (this.maxDate) {
        if (clickedMonth.year > maxYear) {
          return true;
        }
        if (clickedMonth.year === maxYear && clickedMonth.month > maxMonth) {
          return true;
        }
      }
    }
    return false;
  }

  isActive(date: string): boolean {
    const leaveDate = String(new Date(this.value).getDate());
    if (date === leaveDate && this.isCurrentMonth() && this.isCurrentYear() && this.isOrder) {
      return true;
    }
    return false;
  }

  // getAvailableFinalDate(): any {
  //   if(this.isOrder) {
  //     const latestTue = this.tuesdays.filter(ts => {
  //       return ts > new Date(this.dateCheckIn).getTime();
  //     })
  //     const latestBookedDate = this.bookedDate.sort().filter(ts => {
  //       return ts > new Date(this.dateCheckIn).getTime();
  //     })

  //     const finalDate = latestTue[0] < latestBookedDate[0] || !latestBookedDate[0] ? latestTue[0] : latestBookedDate[0];

  //     this.finalDate = finalDate;
  //   }
  // }

  // else 是好的
  getAvailableFinalDate(): any {
    const latestTue = this.tuesdays.filter(ts => {
      return ts > new Date(this.dateCheckIn).getTime();
    })
    const latestBookedDate = this.bookedDate.sort().filter(ts => {
      return ts > new Date(this.dateCheckIn).getTime();
    })

    if(this.isOrder && this.stayType === OrderType.TENT_HOMESTAY) {
      const compareTS = [];
      compareTS.push(latestTue[0])
      compareTS.push(latestBookedDate[0])
      const finalDate = min(compareTS)
      this.finalDate = finalDate;
      return
    }

    if(this.isOrder && this.stayType !== OrderType.TENT_HOMESTAY) {
      const latestFri = this.fridays.filter(ts => {
        return ts > new Date(this.dateCheckIn).getTime();
      })
      const compareTS = [];
      compareTS.push(latestTue[0])
      compareTS.push(latestBookedDate[0])
      compareTS.push(latestFri[0])

      const finalDate = min(compareTS)
      this.finalDate = finalDate;
      return
    }
  }

  // Order page 顯示最小 & 最大可預約日期
  isAvailableBookedRange(pickerDayTS: number, isOrder: boolean) {
    if(isOrder) {
      return pickerDayTS > this.finalDate || (!this.finalDate && pickerDayTS > new Date(this.dateCheckIn).getTime());
    }
    return false;
  }

  public getMinDateTS(): number {
    return new Date(this.minDate).getTime() - this.dayTS;
  }

  private isCurrentMonth() {
    const currentMonth = new Date(this.value).getMonth();
    const showMonth = new Date(this.showValue).getMonth();
    return currentMonth === showMonth;
  }

  private isCurrentYear(){
    const currentYear = new Date(this.value).getFullYear();
    const showYear = new Date(this.showValue).getFullYear();
    return currentYear === showYear;
  }

  private setDaysFromThisMonth(dateCount: number, days: DatePickerItem[]) {
    for (let i = 1; i <= dateCount; i++) {
      days.push({
        date: String(i),
        disable: this.isDisableItem(i),
        timestamp: this.getTS(this.year, this.month - 1, i)
      });
    }
  }

  private getTS(year: number, month: number, day: number): string {
    return String(new Date(year, month, day).getTime());
  }

  private fillEmptyDays(total: number, count: number, days: DatePickerItem[]) {
    let emptyDays = count - (total % 7) + 1;

    if (emptyDays < 0) {
      emptyDays = 7 + emptyDays;
    }

    for (let i = 0; i < emptyDays; i++) {
      days.push({
        date: '',
        disable: false,
        timestamp: ''
      });
    }
  }

  private getLastDateInThisMonth(date: string) {
    const separateDay = date.split('-');
    const newDate = `${separateDay[0]}-${separateDay[1]}-20`;
    const time = new Date(newDate);
    time.setMonth(time.getMonth() + 1);
    time.setDate(0);
    return time;
  }

  private isDisableItem(date: number) {
    if (this.isOverMax(date) || this.isOverMin(date)) {
      return true;
    }
    return false;
  }

  private isOverMin(date: number) {
    const min = new Date(this.minDate);

    if (this.minDate === '') {
      return false;
    }
    if (min.getFullYear() !== this.year) {
      return false;
    }
    if ((min.getMonth() + 1) !== this.month) {
      return false;
    }
    return date < min.getDate();
  }

  private isOverMax(date: number) {
    const min = new Date(this.maxDate);

    if (this.maxDate === '') {
      return false;
    }
    if (min.getFullYear() !== this.year) {
      return false;
    }
    if ((min.getMonth() + 1) !== this.month) {
      return false;
    }
    return date > min.getDate();
  }

  isCorrectFormat(input_data: string): boolean {
    const reg = /^[1-9]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;
    return new RegExp(reg).test(input_data);
  }

  isCorrectDate(input_data: string): boolean {
    const time = getString(input_data.split(' '), '0');
    const date = getNumber(time.split('-'), '2');
    return new Date(input_data).getDate() === date;
  }

  parseDateString(dateString: string): string {
    if (
      !this.isCorrectFormat(dateString) ||
      !this.isCorrectDate(dateString)) {
      return this.timeService.dateTime.toString().slice(0, 10);
    }
    return dateString;
  }
}
