import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Common } from 'src/app/enum/common';
import { getNumber, getString } from 'src/app/helper/lodash';
import { TimeService } from 'src/app/service/time.service';
import { getOfficialHolidays } from 'src/app/helper/helper';
import { flatten } from 'lodash';
import * as moment from 'moment-timezone';

interface DatePickerItem {
  date: string;
  disable: boolean;
  timestamp: string;
}

@Component({
  selector: 'app-calendar-date-picker',
  templateUrl: './calendar-date-picker.component.html',
  styleUrls: ['./calendar-date-picker.component.sass']
})

export class CalendarDatePickerComponent implements OnInit, OnChanges {
  @Output() dateChanged = new EventEmitter<string>();
  @Output() onShowPicker = new EventEmitter<boolean>();

  @Input() holidays: number[] = [];
  @Input() initHolidays: boolean = false;
  // @Input() isDayBefore3of2OfficialHolidays: boolean = false;

  isBooked: {
    [key: string]: boolean
  } = {}

  // 1031開發起始
  // 當天已被預訂的帳數
  // 再傳進來之前算好各個天數被預訂的總帳數
  @Input() allBookedTents: {
    [key: string]: number
  } = {}
  @Input() totalTents: number = 0;
  arr: number[] = [];
  // 1031開發結束

  // 日期格式2020-05-22
  private _value = '';
  @Input()
  set value(input_data: string) {
    this._value = this.parseDateString(input_data);
  }
  get value(): string {
    return this._value;
  }

  // 日期格式2020-05-22
  private _minDate = '';
  @Input()
  set minDate(input_data: string) {
    this._minDate = this.parseDateString(input_data);
  }
  get minDate(): string {
    return this._minDate;
  }

  // 日期格式2020-05-22
  private _maxDate = '';
  @Input()
  set maxDate(input_data: string) {
    this._maxDate = this.parseDateString(input_data);
  }
  get maxDate(): string {
    return this._maxDate;
  }

  showValue = '';
  isShowPicker = true;
  pickerDays: DatePickerItem[] = [];
  dayTS: number;
  clickedMonth = {
    year: 0,
    month: 0
  };

  get year(): number {
    return getNumber(this.showValue.split('/'), '0');
  }

  get month(): number {
    return getNumber(this.showValue.split('/'), '1');
  }

  get day(): number {
    const time = getString(this.showValue.split(' '), '0');
    return getNumber(time.split('/'), '2');
  }

  constructor(public timeService: TimeService, private router: Router, private route: ActivatedRoute) {
    // this.isTypeStart = false;
    this.dayTS = 86400000;
  }

  ngOnInit() {
    this.showDatePicker();
    this.showValue = this.value;
    // Lynn -> 顯示當月日曆
    // this.showValue = '2023/9/1';
    this.pickerDays = this.getPickerDays(this.showValue);

  }

  ngOnChanges(changes: SimpleChanges) {
    // if(changes['bookedDate']) {

    // }
  }

  public isDayBefore3of2OfficialHolidays(ts: number) {
    const FirstDayOfEveryOfficalHolidays_2024 = getOfficialHolidays().year_2024
    const FirstDayOfEveryOfficalHolidays_2025 = getOfficialHolidays().year_2025
    const FirstDayOfEveryOfficalHolidaysAll = FirstDayOfEveryOfficalHolidays_2024.concat(FirstDayOfEveryOfficalHolidays_2025)

    let firstDayOfEveryOfficalHolidays = FirstDayOfEveryOfficalHolidaysAll
      .map(days => days)
      .map((day: string[]) => {
      if(day.length > 1) {
        return new Date(day[0]).getTime() + this.timezoneOffsetAndDayLightTime(ts);
      }
      return 0;
    })

    // TODO: 如果連假長度超過 5 天，把第三天也放入3天2夜的限制
    const thiDayOfEveryOfficialHolidays = getOfficialHolidays().year_2024
      .filter(days => days.length > 4)
      .map(day => {
      return new Date(day[2]).getTime() + this.timezoneOffsetAndDayLightTime(ts);
    })

    const combine3of2OfficialHolidays = firstDayOfEveryOfficalHolidays.concat(thiDayOfEveryOfficialHolidays)

    return combine3of2OfficialHolidays.map(ts => ts + this.dayTS).includes(ts)
  }

  getBookedTents(area: any) {
    return
  }
  disabledBeforeToday(ts: number) {
    return new Date(this.minDate).getTime() + this.timezoneOffsetAndDayLightTime(new Date(this.minDate).getTime()) > ts;
  }

  showDatePicker() {
    this.isShowPicker = true;
    this.showValue = this.value;
    this.pickerDays = this.getPickerDays(this.showValue);
  }

  hideDatePicker() {
    this.isShowPicker = false;
  }

  handleMonth(count: number) {
    if (this.isDisableMonth(count)) {
      return;
    }

    const d = new Date(this.showValue);
    const currentMonth = d.getMonth();
    d.setDate(1);
    d.setMonth(currentMonth + count);
    const newYear = d.getFullYear();
    const newMonth = d.getMonth() + 1;
    const newLastDate = this.getLastDateInThisMonth(`${newYear}/${newMonth}/01`).getDate();
    const selectedDate = Number(this.value.split('/')[2]);
    const newDate = newLastDate < selectedDate ? newLastDate : selectedDate;
    const newDateParse = newDate < 10 ? `${newDate}` : `${newDate}`;
    this.clickedMonth = {
      year: Number(newYear),
      month: Number(newMonth)
    };

    this.showValue = `${newYear}/${newMonth}/${newDateParse}`;
    // console.log('after showValue', this.showValue)
    this.pickerDays = this.getPickerDays(this.showValue);
    // console.log('this.pickerDays', this.pickerDays)
  }

  stopPropagation(event: { preventDefault: () => void; returnValue: boolean; }) {
    if (event.preventDefault) {
      event.preventDefault();
    }
    if (event.returnValue) {
      event.returnValue = false;
    }
  }

  changeDay(date: DatePickerItem) {
    if (this.allBookedTents[date.timestamp] === 10) {
      return;
    }
    const month = this.month < 10 ? `${this.month}` : `${this.month}`;
    const day = Number(date.date) < 10 ? `${date.date}` : `${date.date}`;

    this.dateChanged.emit(`${this.year}/${month}/${day}`);
    // calendar顯示開關
    this.isShowPicker = false;
    this.onShowPicker.emit(this.isShowPicker);

    // TypeStart
    // if(this.isTypeStart) {
    //   // this.dateChanged.emit(`${this.year}-${month}-${day}`);
    //   this.router.navigate([`/start-date/${this.route.snapshot.paramMap.get('id')}/order`], {
    //     queryParams: {
    //       startDate: `${this.year}-${month}-${day}`
    //     }
    //   })
    // } else {
    //   this.hideDatePicker();
    //   this.dateChanged.emit(`${this.year}-${month}-${day}`);
    // }
  }

  getPickerDays(date: string): DatePickerItem[] {
    const days: DatePickerItem[] = [];
    const lastDay = this.getLastDateInThisMonth(date);
    this.fillEmptyDays(lastDay.getDate(), lastDay.getDay(), days);
    this.setDaysFromThisMonth(lastDay.getDate(), days);
    return days;
  }

  isDisableMonth(count: number) {
    const d = new Date(this.showValue);
    const currentMonth = d.getMonth();
    d.setMonth(currentMonth + count);
    let minYear = Number(this.minDate.split('/')[0]);
    // const maxYear = Number(this.maxDate.split('/')[0]);
    let minMonth = Number(this.minDate.split('/')[1]);
    // Lynn -> 往左點擊可顯示的日曆最小月份
    // minMonth = 9

    let maxMonth = Number(this.maxDate.split('/')[1]);
    const maxYear = minMonth < 6 || minYear === Number(this.maxDate.split('/')[0]) ? minYear : minYear + 1;


    if (count < 0) {
      if (this.minDate) {
        if (this.clickedMonth.year < minYear) {
          return true;
        }
        if (this.clickedMonth.year === minYear && this.clickedMonth.month <= minMonth) {
          return true;
        }
      }
    }

    if (count > 0) {
      if (this.maxDate) {
        if (this.clickedMonth.year > maxYear) {
          return true;
        }
        if (this.clickedMonth.year === maxYear && this.clickedMonth.month > maxMonth) {
          return true;
        }
      }
    }
    return false;
  }

  isActive(date: string): boolean {
    if (date === this.day.toString() && this.isCurrentMonth() && this.isCurrentYear()) {
      return true;
    }
    return false;
  }

  // checkFull(dateTS: number, isOrder: boolean) {
  //   if(isOrder) {
  //     const minBookedDate = Math.min(...this.bookedDate);
  //     if(dateTS > minBookedDate) {
  //       return true
  //     }
  //   }

  //   return false;
  // }

  // 檢查國外時區是否為夏令時間(夏至會快ㄧ小時)
  private isDaylightSavingTime(date: any, timeZone: any) {
    return moment.tz(date, timeZone).isDST();
  }

  private timezoneOffsetAndDayLightTime(localTimestamp: number) {
    const localOffset = new Date().getTimezoneOffset() / 60;

    const diff = (-localOffset * 60 * 60 * 1000) - (8 * 60 * 60 * 1000);

    // 歐洲有夏令時間，夏天會撥快1小時
    const currentTimezone = moment.tz.guess();
    const dayLightSavingForOneHour = this.isDaylightSavingTime(new Date(localTimestamp), currentTimezone) ? 3600000 : 0

    return Number(diff + dayLightSavingForOneHour)
  }

  // TODO: 定期更新
  public isOfficialHolidays(timestamp: number) {
    const holidaysDatePool_2024 = flatten(getOfficialHolidays().year_2024).map(date => new Date(date).getTime() + this.timezoneOffsetAndDayLightTime(timestamp))
    const holidaysDatePool_2025 = flatten(getOfficialHolidays().year_2025).map(date => new Date(date).getTime() + this.timezoneOffsetAndDayLightTime(timestamp))

    const allPool = holidaysDatePool_2024.concat(holidaysDatePool_2025)
    return allPool.includes(timestamp);
  }

  public getMinDateTS(): number {
    return new Date(this.minDate).getTime() + this.timezoneOffsetAndDayLightTime(new Date(this.minDate).getTime()) - this.dayTS;
  }

  private isCurrentMonth() {
    const currentMonth = new Date(this.value).getMonth();
    const showMonth = new Date(this.showValue).getMonth();
    return currentMonth === showMonth;
  }

  private isCurrentYear(){
    const currentYear = new Date(this.value).getFullYear();
    const showYear = new Date(this.showValue).getFullYear();
    return currentYear === showYear;
  }

  private setDaysFromThisMonth(dateCount: number, days: DatePickerItem[]) {
    for (let i = 1; i <= dateCount; i++) {
      days.push({
        date: String(i),
        disable: this.isDisableItem(i),
        timestamp: this.getTS(this.year, this.month - 1, i)
      });
    }
  }

  private getTS(year: number, month: number, day: number): string {
    return String(new Date(year, month, day).getTime() + this.timezoneOffsetAndDayLightTime(new Date(year, month, day).getTime()));
  }

  private fillEmptyDays(total: number, count: number, days: DatePickerItem[]) {
    let emptyDays = count - (total % 7) + 1;

    if (emptyDays < 0) {
      emptyDays = 7 + emptyDays;
    }

    for (let i = 0; i < emptyDays; i++) {
      days.push({
        date: '',
        disable: false,
        timestamp: ''
      });
    }
  }

  private getLastDateInThisMonth(date: string) {
    const separateDay = date.split('/');
    const newDate = `${separateDay[0]}/${separateDay[1]}/20`;
    const time = new Date(newDate);
    time.setMonth(time.getMonth() + 1);
    time.setDate(0);
    return time;
  }

  private isDisableItem(date: number) {
    if (this.isOverMax(date) || this.isOverMin(date)) {
      return true;
    }
    return false;
  }

  private isOverMin(date: number) {
    const min = new Date(this.minDate);

    if (this.minDate === '') {
      return false;
    }
    if (min.getFullYear() !== this.year) {
      return false;
    }
    if ((min.getMonth() + 1) !== this.month) {
      return false;
    }
    return date < min.getDate();
  }

  private isOverMax(date: number) {
    const min = new Date(this.maxDate);

    if (this.maxDate === '') {
      return false;
    }
    if (min.getFullYear() !== this.year) {
      return false;
    }
    if ((min.getMonth() + 1) !== this.month) {
      return false;
    }
    return date > min.getDate();
  }

  isCorrectFormat(input_data: string): boolean {
    // const reg = /^[1-9]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;
    const reg = /^[1-9]\d{3}\/([1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/;
    return new RegExp(reg).test(input_data);
  }

  isCorrectDate(input_data: string): boolean {
    const time = getString(input_data.split(' '), '0');
    const date = getNumber(time.split('/'), '2');
    return new Date(input_data).getDate() === date;
  }

  parseDateString(dateString: string): string {
    if (
      !this.isCorrectFormat(dateString) ||
      !this.isCorrectDate(dateString)) {
      const newDate = this.timeService.dateTime.toString().slice(0, 10)
      const y = newDate.split('-')[0]
      const m = parseInt(newDate.split('-')[1].toString(), 10).toString()
      const d = parseInt(newDate.split('-')[2].toString().slice(0, 10), 10).toString()
      const localeDateString = `${y}/${m}/${d}`
      return localeDateString
    }
    return dateString;
  }

  // custom function
  // public isBookedAvailable(ts: number): boolean {
  //   const isBooked = {
  //     'tent_homestay': this.bookedDate['tent_homestay']?.includes(ts),
  //     'tent_a1_homestay': this.bookedDate['tent_a1_homestay']?.includes(ts),
  //     'homestay': this.bookedDate['homestay']?.includes(ts),
  //     'tent_A': this.bookedDate['tent_A']?.includes(ts),
  //     'tent_A1': this.bookedDate['tent_A1']?.includes(ts),
  //     'tent_A2': this.bookedDate['tent_A2']?.includes(ts),
  //   }
  //   return !isBooked['tent_homestay'] &&
  //   (!isBooked['tent_a1_homestay'] || !isBooked['tent_A2']) &&
  //   (!isBooked['tent_a1_homestay'] || (!isBooked['tent_A2'] && !(new Date(ts).getDay() === 5 || new Date(ts).getDay() === 6 || new Date(ts).getDay() === 0))) &&
  //   (!isBooked['homestay'] || !isBooked['tent_A']) &&
  //   (((!isBooked['tent_A1'] || !isBooked['tent_A2']) || !isBooked['homestay'] || !isBooked['tent_a1_homestay']) && (new Date(ts).getDay() !== 5 || new Date(ts).getDay() !== 6))
  // }

  // custom 公休日星期二，未來可以新增特殊休假日(只有顯示用)
  isDayOff(ts: number) {
    return new Date(ts).getDay() === 2 || new Date(ts).getDay() === 1;
  }

  isPrivateRoom(ts: number) {
    const curTS = new Date().getTime();
    return (curTS + this.timezoneOffsetAndDayLightTime(curTS) + this.dayTS * Common.IS_PRIVATE_ROOM_LIMIT) < ts + this.timezoneOffsetAndDayLightTime(curTS);
  }

  // 手動休假日
  isHoliday(ts: number) {
    return this.holidays.includes(ts)
  }
}
