import { Component, OnInit } from '@angular/core';
import { ModalNewService } from 'src/app/service/modal-new.service';
import { ModalNewBase } from '../modal-new-base';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.sass']
})
export class QuizComponent extends ModalNewBase implements OnInit {
  inpAnswer = '';
  answer = '';
  x = 0;
  y = 0;
  z = 0;

  constructor(modalNew: ModalNewService) {
    super(modalNew)
   }

  ngOnInit(): void {
    this.doMath();
  }
  correct() {
    if(this.inpAnswer !== String(this.answer)) {
      alert('答錯了，請重新作答')
      this.inpAnswer = '';
      this.doMath();

      return
    }
    this.submit();
  }

  doMath() {
    const x = Math.floor(Math.random() * 10)
    const y = Math.floor(Math.random() * 10)
    const z = Math.floor(Math.random() * 10)

    this.x = x;
    this.y = y;
    this.z = z;

    const answer = String(x * y + z)
    this.answer = answer;
  }
}
