import { Component, Input, OnDestroy } from '@angular/core';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { OrderHomeStay } from 'src/app/enum/order-type';
import { ModalNewService } from 'src/app/service/modal-new.service';
import { ModalNewBase } from '../modal-new-base';
import { getAreaTitle } from 'src/app/helper/helper';
import { QuizComponent } from '../quiz/quiz.component';

@Component({
  selector: 'app-confirm-box',
  templateUrl: './confirm-box.component.html',
  styleUrls: ['./confirm-box.component.sass']
})
export class ConfirmBoxComponent extends ModalNewBase implements OnDestroy {
  @Input() props!: {
    title: string;
    orderType: string;
    order: any;
    paymentInfo: any;
  };
  OrderHomeStay = OrderHomeStay;
  faCheck = faCheck;
  isAgree = false;
  isSent = false;
  isTogglePrice = false;
  bookingStep = 0
  getAreaTitle = getAreaTitle;
  isQuizActive = false;
  isQuizBtnClose = false;

  constructor(modalNew: ModalNewService) {
    super(modalNew)
  }

  ngOnInit(): void {

  }
  ngOnDestroy(): void {
    this.bookingStep = 0
    this.isSent = false
  }
  agree() {
    this.isAgree = !this.isAgree;
  }

  sent() {
    this.isSent = true;
  }

  nextStep() {
    this.bookingStep += 1;
  }

  checkIfRobot() {
    this.isQuizActive = true;
    const quiz = this.modalNew.open(QuizComponent, {
      title: 'quiz'
    })

    quiz.onSubmit.subscribe(() => {
      this.isQuizBtnClose = true;
      this.isQuizActive = false;
      quiz.close();
    })
  }

}
