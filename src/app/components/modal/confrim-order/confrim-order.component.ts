import { OrderTent } from 'src/app/enum/order-type';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ModalNewService } from 'src/app/service/modal-new.service';
import { ModalNewBase } from '../modal-new-base';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-confrim-order',
  templateUrl: './confrim-order.component.html',
  styleUrls: ['./confrim-order.component.sass']
})

// 營區order
// entry.530227552: "姓名"
// entry.1856466362: "091233387"
// entry.1966906870: "lll@gmail.com"
// entry.1541435256: "A1"
// entry.37384340_year: "2022"
// entry.37384340_month: "04"
// entry.37384340_day: "26"
// entry.1109261803_year: "2022"
// entry.1109261803_month: "04"
// entry.1109261803_day: "28"
// entry.403233387: 2 // tents數量
// entry.1076644647: 2 // 過夜天數
// entry.2086188320: 0 // 加人數
// entry.286246681: "4000" // 費用
// entry.1888965328: "12355" //帳號末五碼
// entry.1919131714: "否" //是否付款

// 宿屋order
// entry.1272831780: "姓名"
// entry.191251677: "電話"
// entry.971666548: "email@gmail.com"
// entry.564936396_year: "2022"
// entry.564936396_month: "04"
// entry.564936396_day: "29"
// entry.1907359099_year: "2022"
// entry.1907359099_month: "04"
// entry.1907359099_day: "30"
// entry.1115877202: "2" // 大人
// entry.2107130537: "1" // 小孩
// entry.1798950547: "12345"
// entry.1120019154: "1" // 過夜天數
// entry.1023425628: "否"
// entry.1799577947: "否"
export class ConfrimOrderComponent extends ModalNewBase implements OnDestroy {
  @Input() props!: {
    title: string;
    orderType: string;
    order: any;
    paymentInfo: any;
  };
  OrderTent = OrderTent;
  faCheck = faCheck;
  isAgree = false;
  isSent = false;
  isTogglePrice = false;
  bookingStep = 0

  constructor(
    modalNew: ModalNewService,
  ) {
    super(modalNew)
  }

  ngOnDestroy(): void {
    this.bookingStep = 0
    this.isSent = false
  }

  sent() {
    this.isSent = true;
  }

  agree() {
    this.isAgree = !this.isAgree;
  }

  nextStep() {
    this.bookingStep += 1;
  }
}
