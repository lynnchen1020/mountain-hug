import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmBoxInnComponent } from './confirm-box-inn.component';

describe('ConfirmBoxInnComponent', () => {
  let component: ConfirmBoxInnComponent;
  let fixture: ComponentFixture<ConfirmBoxInnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmBoxInnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmBoxInnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
