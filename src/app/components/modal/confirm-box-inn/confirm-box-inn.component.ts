import { Component } from '@angular/core';
import { ModalNewService } from 'src/app/service/modal-new.service';
import { ConfirmBoxComponent } from '../confirm-box/confirm-box.component';

@Component({
  selector: 'app-confirm-box-inn',
  templateUrl: './confirm-box-inn.component.html',
  styleUrls: ['./confirm-box-inn.component.sass']
})
export class ConfirmBoxInnComponent extends ConfirmBoxComponent {

  constructor(modalNew: ModalNewService,) {
    super(modalNew);
  }


}
