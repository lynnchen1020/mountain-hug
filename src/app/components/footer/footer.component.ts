import { Component, OnInit } from '@angular/core';
import { faPhoneAlt, faEnvelope, faMobileAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {
  faPhoneAlt = faPhoneAlt;
  faMobileAlt = faMobileAlt;
  faEnvelope = faEnvelope;

  constructor() { }

  ngOnInit(): void {
  }

}
