import { Component, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
// import { ReCaptchaV3Service } from 'ng-recaptcha';
import { Subscription } from 'rxjs';
import { OrderType } from 'src/app/enum/order-type';
import { IDate } from 'src/app/models/IDateTime';
import { ApiService } from 'src/app/service/api.service';
import { ModalNewService } from 'src/app/service/modal-new.service';
import { TimeService } from 'src/app/service/time.service';

@Component({
  template: '',
})

export abstract class BaseOrderComponent {
  order: any;
  orderList: any[] = [];

  dayTS: number;
  dateLeave: string;
  dateMin: string;
  dateMax: string;
  dateStart: string | undefined;
  dateEnd: string | undefined;

  bookedDate: number[];
  status: string;

  formControls: ElementRef[] = [];
  form: FormGroup | undefined;

  message: { [key: string]: string } = {};
  validationMessages!: {
    [key: string]: { [key: string]: string | { [key: string]: string; }; };
  };
  OrderType = OrderType;

  subList?: Subscription[] = [];

  constructor(
    public router: Router,
    public apiService: ApiService,
    public time: TimeService,
    public route: ActivatedRoute,
    public modService: ModalNewService,
    // public recaptchaV3Service: ReCaptchaV3Service
  ) {
    this.dayTS = 24 * 60 * 60 * 1000;
    this.dateLeave = '';
    this.dateMin = '';
    this.dateMax = '';
    this.dateEnd = '';
    this.dateStart = this.dateMin;
    this.dateEnd = this.dateMax;

    this.bookedDate = [];
    this.status = '';
  }

  // ngOnInit(): void {
  //   throw new Error('Method not implemented.');
  // }

  // abstract initForm(): void
  abstract getYMD(ymd: string, index: number): string;
  abstract getTS(bookedDate: string): IDate;
  abstract getNights(): void;
  // abstract getReservationData(): void;
  abstract submit(): void;


}
