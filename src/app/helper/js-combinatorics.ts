import {
  C as c_C,
  bigCombination as c_bigCombination,
  combination as c_combination,
} from 'js-combinatorics';
import { get, uniq } from './lodash';

// 取得排列組合後，做排序
export function getSuits(list: any, length: number): any {
  const suits: any = c_bigCombination(list, length).toArray();

  suits.sort((a, b) => {
    const index = getIndex(a, b);

    return a[index] - b[index];
  });
  return suits;
}

export function C(m: number, n: number): number {
  return c_C(m, n);
}

export function combination<T>(list: Array<T>, length?: number) {
  return c_combination(list, length);
}

function getIndex(a: any, b: number): number {
  for (let index = 0; index < a.length; index += 1) {
    if (a[index] !== b[index]) {
      return index;
    }
  }
  return 0;
}

/**
 * 計算兩組 array 不重複的值的排列組合
 */
export function twoArrCombination(
  lists: string[][],
  hasLocate: boolean
): string[][] {
  const arr1: number[] = get(lists, '0', []) || [];
  const arr2: number[] = get(lists, '1', []) || [];
  const suits: string[][] = [];

  arr1.forEach((choose1) => {
    arr2.forEach((choose2) => {
      let list = [Number(choose1), Number(choose2)];

      if (!hasLocate) {
        list = list.sort();
      }

      suits.push(list.map((num) => String(num)));
    });
  });

  return uniq(suits.sort().map((list) => list.join('.'))).map((str: string) =>
    str.split('.')
  );
}

/**
 * 計算三組 array 的值的排列組合
 *
 */
export function threeArrCombination(
  lists: string[][],
  hasLocate: boolean
): string[][] {
  const arr1: number[] = get(lists, '0', []);
  const arr2: number[] = get(lists, '1', []);
  const arr3: number[] = get(lists, '2', []);
  const suits: string[][] = [];

  arr1.forEach((choose1) => {
    arr2.forEach((choose2) => {
      arr3.forEach((choose3) => {
        let list = [Number(choose1), Number(choose2), Number(choose3)];

        if (!hasLocate) {
          list = list.sort();
        }

        suits.push(list.map((num) => String(num)));
      });
    });
  });

  return uniq(suits.sort().map((list) => list.join('.'))).map((str: string) =>
    str.split('.')
  );
}

/**
 * 計算四組 array 的值的排列組合
 *
 */
export function fourArrCombination(
  lists: string[][],
  hasLocate: boolean
): string[][] {
  const arr1: number[] = get(lists, '0', []);
  const arr2: number[] = get(lists, '1', []);
  const arr3: number[] = get(lists, '2', []);
  const arr4: number[] = get(lists, '3', []);
  const suits: string[][] = [];

  arr1.forEach((choose1) => {
    arr2.forEach((choose2) => {
      arr3.forEach((choose3) => {
        arr4.forEach((choose4) => {
          let list = [
            Number(choose1),
            Number(choose2),
            Number(choose3),
            Number(choose4),
          ];

          if (!hasLocate) {
            list = list.sort();
          }

          suits.push(list.map((num) => String(num)));
        });
      });
    });
  });

  return uniq(suits.sort().map((list) => list.join('.'))).map((str: string) =>
    str.split('.')
  );
}
