import { OrderInfo } from '../models/api/order-history-response';
import { get } from '../helper/lodash';

const MAPPING = ['BBAD', 'BBGE', 'BBRB'];

// NOTE: 沒有包含 ROUND 的賠率名稱
const ODDS_MAP = [
 'WIN:HOME',
 'WIN:AWAY',
 'WIN:TIE'
];

/**
 * 判斷是不是有走地的賠率名稱
 */
export function hasRoundOdds(order: OrderInfo): boolean {
  const isRoundGameType = MAPPING.indexOf(order.gameType) !== -1;

  if (isRoundGameType && order.items.length > 0) {
    const oddsKey = get(order.items, '0.orders.0.choose', '');
    const isRound = oddsKey.indexOf('ROUND') !== -1 || ODDS_MAP.indexOf(oddsKey) !== -1;

    if (isRound) {
      return true;
    }
  }

  return false;
}
