import { PLATEFORM_CODE } from 'enum/Plateform-code';
import { Device } from 'classes/Device';

const IPAD = 'IPAD';
const IPHONE = 'IPHONE';

export function getPlatformCode(): PLATEFORM_CODE {
  const device = new Device();

  if (device.isPC()) {
    return PLATEFORM_CODE.PC;
  }

  if (device.isIOS()) {
    const product = device.product().toUpperCase();

    if (product === IPAD) {
      return PLATEFORM_CODE.IPAD;
    }

    if (product === IPHONE) {
      return PLATEFORM_CODE.IPHONE;
    }
  }

  if (device.isAndroid()) {
    return device.isMobile() ? PLATEFORM_CODE.ANDROID : PLATEFORM_CODE.TABLET;
  }

  return PLATEFORM_CODE.MOBILE;
}
