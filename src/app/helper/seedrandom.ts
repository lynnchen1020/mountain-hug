export function seedrandom(str: string): number {
  if (str === '') {
    return 0;
  }
  if (str.length === 1) {
    return random(str.charCodeAt(0));
  }

  const seed = str.split('')
                  .map(item => `${item}`.charCodeAt(0))
                  .reduce((a, b) => a + b);
  return random(seed);

}

function random(seed) {
  const x = Math.sin(seed) * 10000;
  return x - Math.floor(x);
}
