/* eslint max-depth: ["error", 5]*/
/* eslint consistent-return: */

function sliceData(data, outputLength) {
  const startIndex = (data.length - outputLength) > 0 ? data.length - outputLength : 0;
  return data.slice(startIndex, data.length);
}

function create2DArray(y, x) {
  return Array.from(Array(y), () => Array.from(Array(x), () => ''));
}


export default function parse(data = [], y = 6, x = 36) {
  const dataSet = create2DArray(x, y);

  const outputLength = y * x;

  const topData = sliceData(data, outputLength);

  dataSet.forEach((_ArrayX, indexX) => {
    _ArrayX.forEach((_ArrayY, indexY) => {
      dataSet[indexX][indexY] = topData[indexY + indexX * y];
    });
  });
  return dataSet;
}
