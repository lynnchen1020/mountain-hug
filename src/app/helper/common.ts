import { PLATEFORM_CODE } from 'enum/Plateform-code';
import { map } from 'helper/lodash';
import { getPlatformCode } from './platform';
/**
 * 視窗功能
 *
 * @param url 連結位置
 * @param name 視窗名稱
 *
 * @returns window | null
 */
export function windowOpen(url: string, name?: string, attr?: any): Window | null {
  if(name && getPlatformCode() !== PLATEFORM_CODE.PC){
    // 有設定 strWindowName, 卻是手機, 為避免跳轉頁面bug , 直接採用導頁處理
    window.location.href = url;
    return null
  }

  return window.open(url, name, attr);
}

/**
 * 視窗功能
 *
 * @param url 連結位置
 * @param name 視窗名稱
 * @param attr 參數設定
 *
 * @returns window | null
 */
export function xopen(url: string, name?: string, attr?: any): any {
  // 預設參數
  const _attr = {
    width: 1024,
    height: 800,
    resizable: 'yes',
    scrollbars: 'yes',
  };

  const options = map(
    Object.assign({}, _attr, attr),
    (v: string, k: string | number) => [k, v].join('=')
  ).join(',');

  return window.open(url, name, options);
}

function transformInt(
  num1: string | number,
  num2: string | number,
  padZeno: boolean,
  compute
) {
  num1 = '' + num1;
  num2 = '' + num2;

  let p1 = 0;
  let p2 = 0;

  try {
    p1 = num1.split('.')[1].length;
  } catch (e) {}
  try {
    p2 = num2.split('.')[1].length;
  } catch (e) {}

  if (padZeno) {
    while (p1 < p2) {
      p1++;
      num1 += '0';
    }
    while (p2 < p1) {
      p2++;
      num2 += '0';
    }
  }

  const int1 = parseInt(num1.replace('.', ''), 10);
  const int2 = parseInt(num2.replace('.', ''), 10);

  return compute(int1, int2, p1, p2);
}

/* 浮點數相加 */
export function floatAdd(num1: string | number, num2: string | number) {
  return transformInt(num1, num2, true, (int1, int2, p1, p2) => {
    return (int1 + int2) / Math.pow(10, p1);
  });
}

/* 浮點數相減 */
export function floatSub(num1: string | number, num2: string | number) {
  return transformInt(num1, num2, true, (int1, int2, p1, p2) => {
    return (int1 - int2) / Math.pow(10, p1);
  });
}

/* 浮點數相乘 */
export function floatMul(num1: string | number, num2: string | number) {
  return transformInt(num1, num2, false, (int1, int2, p1, p2) => {
    return (int1 * int2) / Math.pow(10, p1 + p2);
  });
}

/* 浮點數相除 */
export function floatDiv(num1: string | number, num2: string | number) {
  return transformInt(num1, num2, false, (int1, int2, p1, p2) => {
    return int1 / int2 / Math.pow(10, p1 - p2);
  });
}
