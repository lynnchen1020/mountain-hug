import { OrderTypeName, OrderTypeTentsMax } from '../enum/order-type';

export function padLeft(str: string | number, length: number): string {
  const s = '' + str;
  return s.length >= length ? s : padLeft('0' + str, length);
}

export function addZero(n: string): string {
  const result = Number(n);
  if (result < 10) {
    return `0${n}`;
  }

  return `${n}`;
}

interface IDayMapping {
  [key: string]: string;
}

export function getCurrentDay(dateTime: string): string {
  const dayMapping: IDayMapping = {
    '0': '日',
    '1': '一',
    '2': '二',
    '3': '三',
    '4': '四',
    '5': '五',
    '6': '六',
  };

  return dayMapping[String(new Date(dateTime).getDay())];
}

export function getOfficialHolidays() {
  const official_holidays = {
    year_2023: [
      ['2023/1/1', '2023/1/2', '2023/1/3'],
      ['2023/2/26', '2023/2/27', '2023/2/28'],
      ['2023/4/1', '2023/4/2', '2023/4/3', '2023/4/4', '2023/4/5'],
      ['2023/6/22', '2023/6/23', '2023/6/24', '2023/6/25'],
      ['2023/9/29', '2023/9/30', '2023/10/1'],
      ['2023/10/7', '2023/10/8', '2023/10/9', '2023/10/10'],
      ['2023/12/30', '2023/12/31', '2024/1/1'],
    ],
    year_2024: [
      [
        '2024/2/8',
        '2024/2/9',
        '2024/2/10',
        '2024/2/11',
        '2024/2/12',
        '2024/2/13',
        '2024/2/14',
      ],
      ['2024/4/4', '2024/4/5', '2024/4/6', '2024/4/7'],
      ['2024/6/8', '2024/6/9', '2024/6/10'],
      ['2024/9/17'],
      ['2024/10/10'],
    ],
    year_2025: [
      [
        '2025/1/1',
      ],
      [
        '2025/1/27',
        '2025/1/28',
        '2025/1/29',
        '2025/1/30',
        '2025/1/31',
      ],
      ['2025/2/28'],
      ['2025/4/4', '2025/4/5', '2025/4/6', '2025//4/7'],
      ['2025/5/30', '2025/5/31', '2025/6/1'],
      ['2025/10/4', '2025/10/5', '2025/10/6'],
      ['2025/10/10'],
    ],
  };

  return official_holidays;
}

// 露營區專用 - 取得各營區名稱
export function getAreaTitle(area: string) {
  switch (area) {
    case 'camp_a':
      return OrderTypeName.CAMP_A;
    case 'camp_b':
      return OrderTypeName.CAMP_B;
    case 'camp_c':
      return OrderTypeName.CAMP_C;
    case 'camp_d':
      return OrderTypeName.CAMP_D;
    case 'inn':
      return OrderTypeName.INN;
    default:
      return '';
  }
}
// 露營區專用 - 取得各營區帳數上限值
export function getAreaTentsMax(area: string) {
  switch (area) {
    case 'camp_a':
      return OrderTypeTentsMax.CAMP_A;
    case 'camp_b':
      return OrderTypeTentsMax.CAMP_B;
    case 'camp_c':
      return OrderTypeTentsMax.CAMP_C;
    case 'camp_d':
      return OrderTypeTentsMax.CAMP_D;
    case 'inn':
      return OrderTypeTentsMax.INN;
    default:
      return '';
  }
}
