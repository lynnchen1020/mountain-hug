import { pick as array_pick } from 'shuffle-array';

interface PickOption {
  'picks': number;
}
export function pickToArray(list: any, option?: PickOption): any {
  if (option && option.picks < 2) {
    return [array_pick(list, option)];
  }
  return array_pick(list, option);
}

export function pick(list: any, option?: PickOption): any {
  return array_pick(list, option);
}
