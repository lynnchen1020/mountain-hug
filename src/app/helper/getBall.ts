export const getOddBalls = (balls: string[]): string[] =>{
  return balls.filter(ball=> Number(ball) % 2 === 1)
}

export const getEvenBalls = (balls: string[]): string[] =>{
  return balls.filter(ball=> Number(ball) % 2 === 0)
}

export const getOverBalls = (balls: string[]): string[] =>{
  return balls.filter(ball=> Number(ball) > Number(balls[balls.length-1])/2)
}

export const getUnderBalls = (balls: string[]): string[] =>{
  return balls.filter(ball=> Number(ball) <= Number(balls[balls.length-1])/2)
}

export const getLastOverBalls = (balls: string[]): string[] =>{
  return balls.filter(ball=> Number(ball) > Number(balls[balls.length-1])/2)
}

export const getLastUnderBalls = (balls: string[]): string[] =>{
  return balls.filter(ball=> Number(ball) <= Number(balls[balls.length-1])/2)
}
