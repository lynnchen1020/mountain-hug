export interface IVersionInfo {
  tag: string;
  commitHash: string;
  buildTime: string;
}

export function getVersion(): IVersionInfo {
  const info = {
    tag: '',
    commitHash: '',
    buildTime: ''
  };

  for (let i = 0; i < document.body.childNodes.length; i++) {
    const node = document.body.childNodes[i];

    if (node.nodeType === 8 && node.textContent.includes('tag')) {
      info['tag'] = node.textContent.replace('Last tag:', '');
    }

    if (node.nodeType === 8 && node.textContent.includes('build time')) {
      info['buildTime'] = node.textContent.replace('Last build time:', '');
    }
  }

  for (let i = 0; i < document.childNodes.length; i++) {
    const node = document.childNodes[i];

    if (node.nodeType === 8 && node.textContent.includes('commit hash')) {
      info['commitHash'] = node.textContent.replace('Last commit hash:', '');
    }
  }

  return info;
}
