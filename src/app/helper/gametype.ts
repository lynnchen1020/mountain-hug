import { IxbbGames } from "services/init-config.service";

export function getGameByGameType(xbbGames: IxbbGames[], gameType: string): IxbbGames {
  return xbbGames.find(game => game.gameType === gameType);
}

export function getGameIdByGameType(xbbGames: IxbbGames[], gameType: string): string {
  return !!getGameByGameType(xbbGames, gameType) ? getGameByGameType(xbbGames, gameType).gameId : gameType;
}

export function getGameByGameId(xbbGames: IxbbGames[], gameId: string): IxbbGames {
  return xbbGames.find(game => game.gameId === gameId);
}

export function getGameTypeByGameId(xbbGames: IxbbGames[], gameId: string): string {
  return !!getGameByGameId(xbbGames, gameId) ? getGameByGameId(xbbGames, gameId).gameType : gameId;
}
