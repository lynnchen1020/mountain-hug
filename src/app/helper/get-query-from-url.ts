export const getQueryFromUrl = (url: string) : Map<string, string>=> {
  const urlMap = new Map<string, string>();
  const urlList = url.split('?');
  if(urlList.length < 2){
    return urlMap
  }

  const queryList = urlList[1].split('&')
  if(queryList.length === 0 ){
    return urlMap
  }

  queryList.forEach(keyVal=>{
    const data = keyVal.split('=')
    if(data.length === 2){
      urlMap.set(String(data[0]), String(data[1]))
    }
  })

  return urlMap

}
