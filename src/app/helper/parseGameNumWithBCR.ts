import { IOrderData } from 'models/IOrderData';

export function parseBCROrderNum(orderList: IOrderData[][]) :IOrderData[][]{
  orderList.forEach(order=>{
    order.forEach(orderItem=>{
      // parse BCR num 15-4 123456 => 123456
      if(orderItem.gameType.includes('BCR')){
        orderItem.num = orderItem.num.split(' ').length > 1 ? orderItem.num.split(' ')[1] : orderItem.num;
      }
    });
  });

  return orderList;
}
