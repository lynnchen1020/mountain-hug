export function getThemeFromCookie(isProd: boolean, inGamePage: boolean): string {
  let mode: string[] = [];

  if(isProd && inGamePage){
    // 正式站遊戲內頁 直接呈現舊版樣式
    return 'is-oldType'
  }

  if(localStorage.getItem('is-fashionLobby-light') === 'true'){
    mode.push('is-light');
  } else {
    mode.push('is-dark');
  }

  if(inGamePage && localStorage.getItem('is-oldType-game-page') === 'true'){
    mode = ['is-oldType'];
  }

  return mode.join(' ');
}
