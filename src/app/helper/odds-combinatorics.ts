import { min } from 'helper/lodash';

export function getGroupCombinatoricsOdds(odds_sum, count ): number {
  // 特殊賠率算法 加總後除以賠率數量 兩次取得最後賠率,賠率小數點兩位後不四捨五入去除
  const string = String((odds_sum / count / count).toFixed(4)).split('.');

  if (string.length > 1) {
    string[1] = string[1].substring(0, 2);
    return  Number(`${string[0]}.${string[1]}`);
  }

  return Number(`${string[0]}`);

}

// 小數點四捨五入
function ff(num: number, pos: number) {
  const size = 10 ** pos;

  return Math.round(num * size) / size;
}

export function calBySumOfPowChoose(odds = []) {
  const l = odds.length;

  if (l === 0) {
    return 0;
  }

  const sum = odds.reduce((s, o) => s + o, 0);

  // 精準度 = ="
  // 舊制先取小數後四位四捨五入後，再取小數後二數四捨五入
  // 請參考六合彩
  const l2 = l ** 2;

  return ff(ff(sum / l2, 4), 2);
}

export function calByMinOfChoose(odds = []) {
  return min(odds);
}

// 轉換數字 防止發生js計算精準度問題
// 浮點數相除
export function divFloat(num1, num2) {
  // eslint-disable-next-line one-var
  let r1, r2, t1 = 0, t2 = 0;
  try {
    t1 = num1.toString().split('.')[1].length;
  } catch (e) { }
  try {
    t2 = num2.toString().split('.')[1].length;
  } catch (e) { }

  r1 = Number(num1.toString().replace('.', ''));
  r2 = Number(num2.toString().replace('.', ''));
  return (r1 / r2) * Math.pow(10, t2 - t1);
}

// 浮點數相加
export function addFloat(num1, num2) {
  // eslint-disable-next-line one-var
  let r1, r2, m;
  try {
    r1 = num1.toString().split('.')[1].length;
  } catch (e) {
    r1 = 0;
  }
  try {
    r2 = num2.toString().split('.')[1].length;
  } catch (e) {
    r2 = 0;
  }
  m = Math.pow(10, Math.max(r1, r2));
  return (mulFloat(num1, m) + mulFloat(num2, m)) / m;
}

// 浮點數相乘
export function mulFloat(num1, num2) {
  let m = 0;
  const s1 = num1.toString();
  const s2 = num2.toString();
  try {
    m += s1.split('.')[1].length;
  } catch (e) { }
  try {
    m += s2.toString().split('.')[1].length;
  } catch (e) { }

  return Number(s1.replace('.', '')) * Number(s2.replace('.', '')) / Math.pow(10, m);
}
