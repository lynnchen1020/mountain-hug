import { Type, ViewRef } from '@angular/core';
import { Observable } from 'rxjs';

export interface IModalNewService {
  modalList: Array<any>;

  onComponentCreate: Observable<any>;
  onComponentCloseLast: Observable<any>;
  onComponentCloseByHostView: Observable<ViewRef>;
  onComponentClearAll: Observable<any>;

  open(component: Type<any>, props: any): Type<any>;
  closeLast(): void;
  clearAll(): void;
  closeByHostView(hostView: ViewRef): void;
}
