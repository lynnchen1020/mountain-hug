// import { DateTime } from "classes/dateTime";
import { Subject, Subscription } from "rxjs";
import { DateTime } from "../classes/dateTime";
// import { IBootResolve } from "./IBootResolve";

export interface ItimeService {
  dateTime: DateTime;
  tick$: Subject<number>;
  subscription: Subscription;
  oldUpdateTime: number;
}
