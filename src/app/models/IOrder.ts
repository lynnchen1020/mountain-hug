export interface IOrder {
  name: string;
  campArea: string,
  mobile: string;
  email: string;
  startDate: string;
  endDate: string;
  nights: number;
  tents: number;
  people: number;
  fee: number;
  paid?: boolean;
}
