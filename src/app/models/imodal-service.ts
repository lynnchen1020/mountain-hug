import { Observable, Subject } from 'rxjs';

export interface CurrentModalEvent {
  onClose: Subject<any>;
  onCancel: Subject<any>;
  onSubmit: Subject<any>;
}

export interface IModalService {
  backdrop: boolean | 'static';
  isActive: boolean;
  isMulti: boolean;
  currentModalEvent: CurrentModalEvent;

  onComponentChange: Observable<any>;
  onClose: Observable<any>;
  onCancel: Observable<any>;
  onSubmit: Observable<any>;

  afterClosed(): Observable<any>;
  afterCanceled(): Observable<any>;
  afterSubmitted(): Observable<any>;
  open(component: any, props?: any): void;
  close(): void;
}
