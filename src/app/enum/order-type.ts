export enum OrderType {
  TENT = 'tent',
  HOMESTAY = 'homestay',
  TENT_HOMESTAY = 'tent_homestay',
  TENT_A1_HOMESTAY = 'tent_a1_homestay',
  TENT_A = 'tent_A',
  TENT_A1 = 'tent_A1',
  TENT_A2 = 'tent_A2',
  CAMP_A = 'camp_a', // 新的 ordertype
  CAMP_B = 'camp_b', // 新的 ordertype
  CAMP_C = 'camp_c', // 新的 ordertype
  CAMP_D = 'camp_d', // 新的 ordertype
  INN = 'inn', // 新的 ordertype
}

export enum OrderTypeName {
  CAMP_A = '五葉松 A區',
  CAMP_B = '落羽松 L區',
  CAMP_C = 'C雨棚區',
  CAMP_D = 'DXX區',
  INN = '山中宿屋',
}

export enum OrderTypeTentsMax {
  CAMP_A = 4,
  CAMP_B = 5,
  CAMP_C = 0,
  CAMP_D = 0,
  INN = 0,
}

export enum OrderPreRatio {
  TENT = 0.5,
  INN = 0.5,
  HOMESTAY = 0.3,
  TENT_HOMESTAY = 0.5,
  TENT_A1_HOMESTAY = 0.5
}

export enum FeeType {
  NORMAL = 1200,
  WEEKEND = 1200,
}

export enum OrderTent {
  ORDER_ID = 'entry.708583205',
  NAME = 'entry.530227552',
  PHONE = 'entry.1856466362',
  EMAIL = 'entry.1966906870',
  TENT_AREA = 'entry.1541435256',
  START_YEAR = 'entry.211652168_year',
  START_MONTH = 'entry.211652168_month',
  START_DAY = 'entry.211652168_day',
  END_YEAR = 'entry.1109261803_year',
  END_MONTH = 'entry.1109261803_month',
  END_DAY = 'entry.1109261803_day',
  STAY_DATE_RANGE = 'entry.1743704001',
  TENTS = 'entry.403233387',
  NIGHTS = 'entry.1076644647',
  // PEOPLE = 'entry.2086188320',
  PREPAID_FEE = 'entry.1731913265',
  REST_FEE = 'entry.709997284',
  TOTAL_FEE = 'entry.286246681',
  // FIVE_DIGITS = 'entry.1888965328',
  IS_PAID_REST_FEE = 'entry.1919131714',
  IS_PAID_PRE_FEE = 'entry.1889882666',
  ORDER_SOURCE = 'entry.65728771',
  IS_CANCEL_ORDER = 'entry.623935948'
}

export enum OrderHomeStay {
  ORDER_ID = 'entry.1306018080',
  NAME = 'entry.1272831780',
  PHONE = 'entry.191251677',
  EMAIL = 'entry.971666548',
  TENT_AREA = 'entry.1541435256',
  START_YEAR = 'entry.564936396_year',
  START_MONTH = 'entry.564936396_month',
  START_DAY = 'entry.564936396_day',
  END_YEAR = 'entry.1907359099_year',
  END_MONTH = 'entry.1907359099_month',
  END_DAY = 'entry.1907359099_day',
  STAY_DATE_RANGE = 'entry.964053448',
  ADULTS = 'entry.2085616214',
  CHILDREN = 'entry.1412794741',
  ADD_BED = 'entry.1513204739',
  ADD_BREAKFAST = 'entry.2137885090',
  NIGHTS = 'entry.1120019154',
  PEOPLE = 'entry.1115877202',
  PREPAID_FEE = 'entry.433555344',
  REST_FEE = 'entry.1384923397',
  TOTAL_FEE = 'entry.1838855808',
  // FIVE_DIGITS = 'entry.1798950547',
  IS_PAID_REST_FEE = 'entry.1799577947',
  IS_PAID_PRE_FEE = 'entry.1023425628',
  REQUEST = 'entry.348750517',
  ORDER_SOURCE = 'entry.1318471277',
  IS_CANCEL_ORDER = 'entry.376580944'
}

export enum OrderTentHomeStay {
  ORDER_ID = 'entry.740830521',
  NAME = 'entry.1000317480',
  PHONE = 'entry.2102893954',
  EMAIL = 'entry.964899361',
  START_YEAR = 'entry.419916791_year',
  START_MONTH = 'entry.419916791_month',
  START_DAY = 'entry.419916791_day',
  END_YEAR = 'entry.1488461577_year',
  END_MONTH = 'entry.1488461577_month',
  END_DAY = 'entry.1488461577_day',
  STAY_DATE_RANGE = 'entry.56286441',
  TENTS = 'entry.403233387',
  NIGHTS = 'entry.408957610',
  ADD_BED = 'entry.2096038797',
  PREPAID_FEE = 'entry.576677966',
  REST_FEE = 'entry.1072849630',
  TOTAL_FEE = 'entry.199477045',
  // FIVE_DIGITS = 'entry.74535983',
  IS_PAID_REST_FEE = 'entry.1074078368',
  IS_PAID_PRE_FEE = 'entry.708741352',
  ORDER_SOURCE = 'entry.1558250427',
  IS_CANCEL_ORDER = 'entry.238547480'
}


export enum OrderTentA1HomeStay {
  ORDER_ID = 'entry.1089301511',
  NAME = 'entry.2004593210',
  PHONE = 'entry.1151046652',
  EMAIL = 'entry.498053930',
  START_YEAR = 'entry.185060475_year',
  START_MONTH = 'entry.185060475_month',
  START_DAY = 'entry.185060475_day',
  END_YEAR = 'entry.1752292606_year',
  END_MONTH = 'entry.1752292606_month',
  END_DAY = 'entry.1752292606_day',
  STAY_DATE_RANGE = 'entry.288409591',
  NIGHTS = 'entry.77437324',
  TENTS = 'entry.2051345920',
  ADD_BED = 'entry.722302437',
  PREPAID_FEE = 'entry.1238905032',
  REST_FEE = 'entry.1105153424',
  TOTAL_FEE = 'entry.421480677',
  // FIVE_DIGITS = 'entry.',
  IS_PAID_REST_FEE = 'entry.168943451',
  IS_PAID_PRE_FEE = 'entry.1066634500',
  ORDER_SOURCE = 'entry.1621304146',
  IS_CANCEL_ORDER = 'entry.240736413',
}
