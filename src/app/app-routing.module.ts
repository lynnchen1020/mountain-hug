import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { environment } from 'src/environments/environment';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./page/home/home.module').then(m => m.HomeModule),
    data: {
      seo: {
        title: '南投國姓露營區',
        metaTags: [
          { name: 'description', content: '山的抱抱座落於海拔600公尺南投國姓鄉的淺山上，以C字型的山脈懷抱著山的抱抱，清晨時欣賞著北港溪變幻的山嵐雲海風景，露營區可包場、小包區，擁有自己的專屬空間，快來山的抱抱享受美好！' },
          { name: 'keywords', content: '國姓露營, 國姓景點, 國姓旅遊, 國姓旅行, 南投國姓露營區, 國姓住宿, 國姓小木屋, 南投露營, 南投住宿, 南投景點, 南投旅行, 南投小木屋' },
          { name: 'description', content: '山的抱抱座落於海拔600公尺南投國姓鄉的淺山上，以C字型的山脈懷抱著山的抱抱，清晨時欣賞著北港溪變幻的山嵐雲海風景，露營區可包場、小包區，擁有自己的專屬空間，快來山的抱抱享受美好！' },
          { property: 'og:title', content: '山的抱抱 - Home' },
          { proprety: 'og:description', content: '山的抱抱座落於海拔600公尺南投國姓鄉的淺山上，以C字型的山脈懷抱著山的抱抱，清晨時欣賞著北港溪變幻的山嵐雲海風景，露營區可包場、小包區，擁有自己的專屬空間，快來山的抱抱享受美好！' },
          { property: 'og:image', content: environment.appUrl + 'assets/img/img_h1.jpg' },
          { property: 'og:url', content: environment.appUrl },
          // { name: "twitter:card", content: "summary_large_image" },
        ]
      }
    }
  },
  {
    path: 'pick-tent',
    loadChildren: () => import('./page/calendar/calendar.module').then(m => m.CalendarModule),
    data: {
      seo: {
        title: '立即預訂',
        metaTags: [
          { name: 'description', content: '山的抱抱座落於海拔600公尺南投國姓鄉的淺山上，以C字型的山脈懷抱著山的抱抱，清晨時欣賞著北港溪變幻的山嵐雲海風景，露營區可包場、小包區，擁有自己的專屬空間，快來山的抱抱享受美好！' },
          { name: 'keywords', content: '國姓露營, 國姓景點, 國姓旅遊, 國姓旅行, 南投國姓露營區, 國姓住宿, 國姓小木屋, 南投露營, 南投住宿, 南投景點, 南投旅行, 南投小木屋' },
          { name: 'description', content: '山的抱抱座落於海拔600公尺南投國姓鄉的淺山上，以C字型的山脈懷抱著山的抱抱，清晨時欣賞著北港溪變幻的山嵐雲海風景，快來山的抱抱享受美好！' },
          { property: 'og:title', content: '山的抱抱 - 立即預訂' },
          { proprety: 'og:description', content: '山的抱抱座落於海拔600公尺南投國姓鄉的淺山上，以C字型的山脈懷抱著山的抱抱，清晨時欣賞著北港溪變幻的山嵐雲海風景，快來山的抱抱享受美好！' },
          { property: 'og:image', content: environment.appUrl + 'assets/img/img_h1.jpg' },
          { property: 'og:url', content: environment.appUrl },
          // { name: "twitter:card", content: "summary_large_image" },
        ]
      }
    }
  },
  {
    path: 'note',
    loadChildren: () => import('./page/note/note.module').then(m => m.NoteModule),
    data: {
      seo: {
        title: '入住須知',
        metaTags: [
          { name: 'description', content: '山的抱抱座落於海拔600公尺南投國姓鄉的淺山上，以C字型的山脈懷抱著山的抱抱，清晨時欣賞著北港溪變幻的山嵐雲海風景，露營區可包場、小包區，擁有自己的專屬空間，快來山的抱抱享受美好！' },
          { name: 'keywords', content: '國姓露營, 國姓景點, 國姓旅遊, 國姓旅行, 南投國姓露營區, 國姓住宿, 國姓小木屋, 南投露營, 南投住宿, 南投景點, 南投旅行, 南投小木屋' },
          { name: 'description', content: '山的抱抱座落於海拔600公尺南投國姓鄉的淺山上，以C字型的山脈懷抱著山的抱抱，清晨時欣賞著北港溪變幻的山嵐雲海風景，快來山的抱抱享受美好！' },
          { property: 'og:title', content: '山的抱抱 - 入住須知' },
          { proprety: 'og:description', content: '山的抱抱座落於海拔600公尺南投國姓鄉的淺山上，以C字型的山脈懷抱著山的抱抱，清晨時欣賞著北港溪變幻的山嵐雲海風景，快來山的抱抱享受美好！' },
          { property: 'og:image', content: environment.appUrl + 'assets/img/img_h1.jpg' },
          { property: 'og:url', content: environment.appUrl },
          // { name: "twitter:card", content: "summary_large_image" },
        ]
      }
    }
  },
  {
    path: 'thanks',
    loadChildren: () => import('./page/thanks/thanks.module').then(m => m.ThanksModule),
    data: {
      seo: {
        title: '謝謝預訂',
        metaTags: [
          { name: 'description', content: '山的抱抱座落於海拔600公尺南投國姓鄉的淺山上，以C字型的山脈懷抱著山的抱抱，清晨時欣賞著北港溪變幻的山嵐雲海風景，露營區可包場、小包區，擁有自己的專屬空間，快來山的抱抱享受美好！' },
          { name: 'keywords', content: '國姓露營, 國姓景點, 國姓旅遊, 國姓旅行, 南投國姓露營區, 國姓住宿, 國姓小木屋, 南投露營, 南投住宿, 南投景點, 南投旅行, 南投小木屋' },
          { name: 'description', content: '山的抱抱座落於海拔600公尺南投國姓鄉的淺山上，以C字型的山脈懷抱著山的抱抱，清晨時欣賞著北港溪變幻的山嵐雲海風景，露營區可包場、小包區，擁有自己的專屬空間，快來山的抱抱享受美好！' },
          { property: 'og:title', content: '山的抱抱 - 謝謝預訂' },
          { proprety: 'og:description', content: '山的抱抱座落於海拔600公尺南投國姓鄉的淺山上，以C字型的山脈懷抱著山的抱抱，清晨時欣賞著北港溪變幻的山嵐雲海風景，露營區可包場、小包區，擁有自己的專屬空間，快來山的抱抱享受美好！' },
          // { property: 'og:image', content: environment.appUrl + 'assets/img/img_h1.jpg' },
          // { property: 'og:url', content: environment.appUrl },
          // { name: "twitter:card", content: "summary_large_image" },
        ]
      }
    }
  },
  {
    path: 'start-date/camp_a',
    loadChildren: () => import('./page/order-camp/order-camp.module').then(m => m.OrderCampModule),
    data: {
      seo: {
        title: '預訂五葉松A區',
        metaTags: [
          { name: 'description', content: '五葉松A區，4帳可包場，週末與三五好友相約露營，享受自然清幽不被打擾的環境！' },
          { name: 'keywords', content: '國姓露營, 國姓景點, 國姓旅遊, 國姓旅行, 南投國姓露營區, 國姓住宿, 國姓小木屋, 南投露營, 南投住宿, 南投景點, 南投旅行, 南投小木屋' },
          { name: 'description', content: '五葉松A區，4帳可包場，週末與三五好友相約露營，享受自然清幽不被打擾的環境！' },
          { property: 'og:title', content: '山的抱抱 - 預訂五葉松A區' },
          { proprety: 'og:description', content: '五葉松A區，4帳可包場，週末與三五好友相約露營，享受自然清幽不被打擾的環境！' },
          // { property: 'og:image', content: environment.appUrl + 'assets/img/img_h1.jpg' },
          // { property: 'og:url', content: environment.appUrl + '/start-date' },
          // { name: "twitter:card", content: "summary_large_image" },
        ]
      }
    }
  },
  {
    path: 'start-date/camp_b',
    loadChildren: () => import('./page/order-camp-b/order-camp-b.module').then(m => m.OrderCampBModule),
    data: {
      seo: {
        title: '預訂落羽松L區',
        metaTags: [
          { name: 'description', content: '落羽松L區，5帳可包場，週末與三五好友相約露營，享受自然清幽不被打擾的環境！' },
          { name: 'keywords', content: '國姓露營, 國姓景點, 國姓旅遊, 國姓旅行, 南投國姓露營區, 國姓住宿, 國姓小木屋, 南投露營, 南投住宿, 南投景點, 南投旅行, 南投小木屋' },
          { name: 'description', content: '落羽松L區，5帳可包場，週末與三五好友相約露營，享受自然清幽不被打擾的環境！' },
          { property: 'og:title', content: '山的抱抱 - 預訂落羽松L區' },
          { proprety: 'og:description', content: '落羽松L區，5帳可包場，週末與三五好友相約露營，享受自然清幽不被打擾的環境！' },
          // { property: 'og:image', content: environment.appUrl + 'assets/img/img_h1.jpg' },
          // { property: 'og:url', content: environment.appUrl + '/start-date' },
          // { name: "twitter:card", content: "summary_large_image" },
        ]
      }
    }
  },
  {
    path: 'start-date/inn',
    loadChildren: () => import('./page/order-inn/order-inn.module').then(m => m.OrderInnModule),
    data: {
      seo: {
        title: '預訂山中宿屋',
        metaTags: [
          { name: 'description', content: '山中宿屋，2間雙人房1大客廳，適合一家人來場溫馨小旅行，超美山谷雲海景觀盡收眼底。' },
          { name: 'keywords', content: '國姓露營, 國姓景點, 國姓旅遊, 國姓旅行, 南投國姓露營區, 國姓住宿, 國姓小木屋, 南投露營, 南投住宿, 南投景點, 南投旅行, 南投小木屋' },
          { name: 'description', content: '山中宿屋，2間雙人房1大客廳，適合一家人來場溫馨小旅行，超美山谷雲海景觀盡收眼底。' },
          { property: 'og:title', content: '山的抱抱 - 預訂山中宿屋' },
          { proprety: 'og:description', content: '山中宿屋，2間雙人房1大客廳，適合一家人來場溫馨小旅行，超美山谷雲海景觀盡收眼底。' },
          // { property: 'og:image', content: environment.appUrl + 'assets/img/img_h1.jpg' },
          // { property: 'og:url', content: environment.appUrl + '/start-date' },
          // { name: "twitter:card", content: "summary_large_image" },
        ]
      }
    }
  },
  // sorry為網站建置中頁面
  // {
  //   path: '',
  //   loadChildren: () => import('./page/sorry/sorry.module').then(m => m.SorryModule),
  //   data: {
  //     seo: {
  //       title: '露營 | 宿屋',
  //       metaTags: [
  //         { name: 'description', content: '山的抱抱座落於海拔600公尺南投國姓鄉的淺山上，以C字型的山脈懷抱著山的抱抱，清晨時欣賞著北港溪變幻的山嵐雲海風景，露營區可包場、小包區，擁有自己的專屬空間，快來山的抱抱享受美好！' },
  //         { name: 'keywords', content: '國姓露營, 國姓景點, 國姓旅遊, 國姓旅行, 南投國姓露營區, 國姓住宿, 國姓小木屋, 南投露營, 南投住宿, 南投景點, 南投旅行, 南投小木屋' },
  //         { name: 'description', content: '山的抱抱座落於海拔600公尺南投國姓鄉的淺山上，以C字型的山脈懷抱著山的抱抱，清晨時欣賞著北港溪變幻的山嵐雲海風景，露營區可包場、小包區，擁有自己的專屬空間，快來山的抱抱享受美好！' },
  //         { property: 'og:title', content: '山的抱抱 - Home' },
  //         { proprety: 'og:description', content: '山的抱抱座落於海拔600公尺南投國姓鄉的淺山上，以C字型的山脈懷抱著山的抱抱，清晨時欣賞著北港溪變幻的山嵐雲海風景，露營區可包場、小包區，擁有自己的專屬空間，快來山的抱抱享受美好！' },
  //         { property: 'og:image', content: environment.appUrl + 'assets/img/img_h1.jpg' },
  //         { property: 'og:url', content: environment.appUrl },
  //         // { name: "twitter:card", content: "summary_large_image" },
  //       ]
  //     }
  //   }
  // },
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    scrollPositionRestoration: 'top'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
