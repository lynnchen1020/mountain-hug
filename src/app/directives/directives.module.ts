import { LazyloadDirective } from './lazyload.directive';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const directives = [
  LazyloadDirective
]

@NgModule({
  declarations: [
    ...directives
  ],
  exports: [
    ...directives
  ],
  imports: [
    CommonModule
  ]
})
export class DirectivesModule { }
