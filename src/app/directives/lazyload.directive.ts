import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Directive, ElementRef, Inject, Input, OnDestroy, PLATFORM_ID } from '@angular/core';

@Directive({
  selector: '[appLazyload]'
})
export class LazyloadDirective implements OnDestroy {
  observer?: IntersectionObserver;
  @Input() appLazyload?: () => void | undefined;

  constructor(
    @Inject(DOCUMENT) private readonly document: Document,
    @Inject(PLATFORM_ID) private readonly platformId: Object,
    private readonly elementRef: ElementRef
  ) {
    if(isPlatformBrowser(this.platformId)) {
      const opt = {
        root: this.document,
        rootMargin: "50px",
      }

      const intersectionCallback = (entries: IntersectionObserverEntry[]) => {
        entries.forEach((entry) => {
          if(entry.isIntersecting) {
            this.elementRef?.nativeElement.classList.add('show')
          }
        })
      }

      this.observer = new IntersectionObserver(intersectionCallback, opt);

      const target = this.elementRef?.nativeElement
      target && this.observer?.observe(target)

    }

  }

  ngOnDestroy(): void {
    this.observer?.disconnect();
  }

}
