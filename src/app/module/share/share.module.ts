import { ConfrimOrderComponent } from './../../components/modal/confrim-order/confrim-order.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalNewComponent } from 'src/app/components/modal/modal-new.component';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { RouterModule } from '@angular/router';
import { FooterComponent } from 'src/app/components/footer/footer.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CalendarDatePickerComponent } from 'src/app/components/common/calendar-date-picker/calendar-date-picker.component';
import { ConfirmBoxComponent } from 'src/app/components/modal/confirm-box/confirm-box.component';
import { ConfirmBoxInnComponent } from 'src/app/components/modal/confirm-box-inn/confirm-box-inn.component';
import { QuizComponent } from 'src/app/components/modal/quiz/quiz.component';
import { FormsModule } from '@angular/forms';

const components = [
  CalendarDatePickerComponent,
  ModalNewComponent,
  ConfrimOrderComponent,
  ConfirmBoxComponent,
  ConfirmBoxInnComponent,
  QuizComponent,
  HeaderComponent,
  FooterComponent
]


@NgModule({
  declarations: [...components],
  imports: [
    FormsModule,
    FontAwesomeModule,
    RouterModule,
    CommonModule
  ],
  exports: [...components, FontAwesomeModule]
})
export class ShareModule { }
