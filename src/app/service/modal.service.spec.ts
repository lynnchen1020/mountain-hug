import { TestBed } from '@angular/core/testing';

import { ModalService } from './modal.service';
import { Component } from '@angular/core';
import { ModalComponent } from '../components/modal/modal.component';

@Component({ selector: 'app-test', template: '<div></div>' })
class TestComponent { }

describe('ModalService', () => {
  let component: ModalComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ModalComponent]
    });
    component = TestBed.createComponent(ModalComponent).componentInstance;
  });

  it('should be created', () => {
    const service: ModalService = TestBed.inject(ModalService);
    expect(service).toBeTruthy();
  });

  it('執行 open() 時有傳值元件與設定 modalService.isActive 為 true', () => {
    // Arrange
    const service: ModalService = TestBed.inject(ModalService);
    const comp: Component = new TestComponent();

    let retrunComp = null;
    service.onComponentChange.subscribe(newComp => {
      retrunComp = newComp.component;
    });

    // ACT
    service.open(comp);

    // Arrange
    expect(retrunComp).toEqual(comp);
    expect(service.isActive).toBe(true);
  });

  it('執行 close() 時有設定 modalService.isActive 為 false', () => {
    const service: ModalService = TestBed.inject(ModalService);
    service.close();

    expect(service.isActive).toBe(false);
  });
});
