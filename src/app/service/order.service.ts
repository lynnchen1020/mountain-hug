import { Injectable } from '@angular/core';
import { concat, flatten } from 'lodash';
import { Observable, Subject, Subscription, tap, zip } from 'rxjs';
import { OrderType } from '../enum/order-type';
import { getCurrentDay } from '../helper/helper';
import { countBy, get, intersection, map, uniq } from '../helper/lodash';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  bookedAvailableSub$ = new Subject();
  isInitData$ = new Subject();
  orderListTH: any[] = [];
  orderListH: any[] = [];
  orderListTent: any[] = [];
  orderListTA1H: any[] = [];
  subList: Subscription[] = [];
  isEmpty = false;
  isLoading = false;
  isWeekend = false;
  hasTue = false;
  isSatOrSunOrMonButTuesdayLeave = false
  resData: any;
  bookedNightsData: {
    [key: string]: string[]
  } = {}
  isBookedAvailable: {
    [key: string]: boolean
  } = {}
  bookedNightsTS: {
    [key: string]: number[]
  } = {}

  bookedStatus: {
    [key: string]: number[]
  } = {}


  dateStart = '';
  dateLeave = '';
  dayStart = '';
  dayLeave = '';
  bookedNights = 1;
  dayTS = 86400000;
  stayDateRange: string[] = [];

  // New Variables Start
  currentArea: string = '';
  eachDateTents: {
    [key: string]: number[]
  } = {}

  maxTents: {
    [key: string]: number
  } = {
    'camp_a': 4,
    'camp_b': 5,
    'camp_c': 4,
    'camp_d': 4,
    'inn': 1,
  }

  maxTentsAmount: number = 1;
  checkIfAvailableSub$ = new Subject();
  allBookedTents: {
    [key: string]: number
  } = {}
  // New Variables End

  constructor(private apiService: ApiService) { }

  resetDate() {
    this.dateStart = '';
    this.dateLeave = '';
  }

  getBookedNights() {
    if(this.dateStart === '' || this.dateLeave === '') {
      return;
    }
    const startTS = new Date(this.dateStart).getTime();
    const endTS = new Date(this.dateLeave).getTime();
    // this.dayCheckOut = getCurrentDay(this.dateLeave);

    this.bookedNights = Math.floor((endTS - startTS) / this.dayTS);
    this.dayStart = getCurrentDay(this.dateStart)
    this.dayLeave = getCurrentDay(this.dateLeave)
    return Math.floor((endTS - startTS) / this.dayTS);
  }

  // New Start
  public getUniqBookedDate(isInit: boolean) {
    const sub = zip(
      this.getCampOrders(OrderType.CAMP_A),
      this.getCampOrders(OrderType.CAMP_B),
      this.getCampOrders(OrderType.CAMP_C),
      this.getCampOrders(OrderType.CAMP_D),
      // this.getInnOrders(),
    ).subscribe(res => {
      const data: {[key: string]: number[]} = {
        'camp_a': flatten(get(res, '0', '').map((item: any) => item.tents_per_day)),
        'camp_b': flatten(get(res, '1', '').map((item: any) => item.tents_per_day)),
        'camp_c': flatten(get(res, '2', '').map((item: any) => item.tents_per_day)),
        'camp_d': flatten(get(res, '3', '').map((item: any) => item.tents_per_day)),
        // 'inn': flatten(get(res, '4', '').map((item: any) => item.tents_per_day)),
      }

      this.eachDateTents = data;


      // 算出各區已被預訂的帳數
      const eachDayLeftTents = {
        'uniqBookedDateA': uniq(flatten(get(res, '0', '').map((item: any) => item.tents_per_day))),
        'uniqBookedDateB': uniq(flatten(get(res, '1', '').map((item: any) => item.tents_per_day))),
        'uniqBookedDateC': uniq(flatten(get(res, '2', '').map((item: any) => item.tents_per_day))),
        'uniqBookedDateD': uniq(flatten(get(res, '3', '').map((item: any) => item.tents_per_day))),
        // 'uniqBookedDateInn': uniq(flatten(get(res, '4', '').map((item: any) => item.tents_per_day))),
      }
      let a_config: any = {}
      eachDayLeftTents['uniqBookedDateA'].forEach(uniqTS => {
        a_config[String(uniqTS)] = countBy(flatten(data[OrderType.CAMP_A]), uniqTS)
      })
      let b_config: any = {}
      eachDayLeftTents['uniqBookedDateB'].forEach(uniqTS => {
        b_config[String(uniqTS)] = countBy(flatten(data[OrderType.CAMP_B]), uniqTS)
      })
      let c_config: any = {}
      eachDayLeftTents['uniqBookedDateC'].forEach(uniqTS => {
        c_config[String(uniqTS)] = countBy(flatten(data[OrderType.CAMP_C]), uniqTS)
      })
      let d_config: any = {}
      eachDayLeftTents['uniqBookedDateD'].forEach(uniqTS => {
        d_config[String(uniqTS)] = countBy(flatten(data[OrderType.CAMP_D]), uniqTS)
      })
      // let inn_config: any = {}
      // eachDayLeftTents['uniqBookedDateInn'].forEach(uniqTS => {
      //   inn_config[String(uniqTS)] = countBy(flatten(data[OrderType.INN]), uniqTS)
      // })
      const eachDayLeftTentsAllObj = Object.assign({
        camp_a: a_config,
        camp_b: b_config,
        camp_c: c_config,
        camp_d: d_config,
        // inn: inn_config,
      })


      // 算出每天總剩餘帳數
      const uniqBookedTSKeys = uniq(
        concat(
          flatten(data[OrderType.CAMP_A]),
          flatten(data[OrderType.CAMP_B]),
          flatten(data[OrderType.CAMP_C]),
          flatten(data[OrderType.CAMP_D]),
          // flatten(data[OrderType.INN])
        )
      )
      const countTentsList = concat(
        flatten(data[OrderType.CAMP_A]),
        flatten(data[OrderType.CAMP_B]),
        flatten(data[OrderType.CAMP_C]),
        flatten(data[OrderType.CAMP_D]),
        // flatten(data[OrderType.INN])
      )
      const obj: any = {}
      uniqBookedTSKeys.forEach(uniqTS => {
        obj[String(uniqTS)] = countBy(countTentsList, uniqTS)
      })

      // 顯示在日曆上每天剩餘的帳數
      this.allBookedTents = obj;

      // 如果不是一次取得 orders, 就送出新資料給是否重複 booking
      if(!isInit) {
        this.checkIfAvailableSub$.next(eachDayLeftTentsAllObj)
      }

      if(sub) {
        sub.unsubscribe();
      }
    })
  }
  public getCampOrders(area: string) {
    return new Observable(observer => {
      this.apiService.getOrders(area).subscribe(res => {
        observer.next(res)
        observer.complete()
      })

    })
  }

  public getInnOrders() {
    return new Observable(observer => {
      this.apiService.getInnOrders().subscribe(res => {
        observer.next(res)
        observer.complete()
      })

    })
  }

  // New End

  // Result
  // getTentAndTentsHomestayBookedReservation(isInit: boolean) {
  //   this.checkIfDayOffOrWeekend();

  //   this.isLoading = true;
  //   this.resData = '';
  //   const sub = zip(
  //     this.getTentsHomestayReservationData(),
  //     this.getTentsA1HomestayReservationData(),
  //     this.getHomestayReservationData(),
  //     this.getTentReservationData('A'),
  //     this.getTentReservationData('A1'),
  //     this.getTentReservationData('A2')
  //     )
  //     .pipe(
  //       tap(x => x.concat(x))
  //     ).subscribe((data: any) => {

  //       if(data && isInit) {
  //         // this.resData = data
  //         this.isLoading = false;
  //         this.bookedNightsData = {
  //           "tent_homestay": data[0],
  //           "tent_a1_homestay": data[1],
  //           "homestay": data[2],
  //           "tent_A": data[3],
  //           "tent_A1": data[4],
  //           "tent_A2": data[5],
  //         }

  //         // console.log('所有入住日期', this.bookedNightsData)

  //         this.bookedNightsTS = {
  //           "tent_homestay": this.dateToTSArr(this.bookedNightsData['tent_homestay']),
  //           "tent_a1_homestay": this.dateToTSArr(this.bookedNightsData['tent_a1_homestay']),
  //           "homestay": this.dateToTSArr(this.bookedNightsData['homestay']),
  //           "tent_A": this.dateToTSArr(this.bookedNightsData['tent_A']),
  //           "tent_A1": this.dateToTSArr(this.bookedNightsData['tent_A1']),
  //           "tent_A2": this.dateToTSArr(this.bookedNightsData['tent_A2'])
  //         }

  //         this.isInitData$.next(data)

  //         // console.log('bookedNightsTS', this.bookedNightsTS)
  //         return
  //       }

  //       if(data) {
  //         this.resData = data;
  //         this.isLoading = false;
  //         this.bookedNightsData = {
  //           "tent_homestay": data[0],
  //           "tent_a1_homestay": data[1],
  //           "homestay": data[2],
  //           "tent_A": data[3],
  //           "tent_A1": data[4],
  //           "tent_A2": data[5],
  //         }

  //         this.bookedStatus = {
  //           'tent_homestay': this.isBookedDate(this.bookedNightsData['tent_homestay']),
  //           'tent_a1_homestay': this.isBookedDate(this.bookedNightsData['tent_a1_homestay']),
  //           'homestay': this.isBookedDate(this.bookedNightsData['homestay']),
  //           'tent_A': this.isBookedDate(this.bookedNightsData['tent_A']),
  //           'tent_A1': this.isBookedDate(this.bookedNightsData['tent_A1']),
  //           'tent_A2': this.isBookedDate(this.bookedNightsData['tent_A2']),
  //         }

  //         this.isBookedAvailable = {
  //           'tent_homestay': !this.bookedStatus['tent_homestay'] && !this.bookedStatus['tent_a1_homestay'] && (!this.bookedStatus['homestay'] && !this.bookedStatus['tent_A']) && (!this.bookedStatus['tent_A1'] && !this.bookedStatus['tent_A2']),
  //           'tent_a1_homestay': !this.bookedStatus['tent_a1_homestay'] && !this.bookedStatus['tent_homestay'] && (!this.bookedStatus['homestay'] && !this.bookedStatus['tent_A']) && !this.bookedStatus['tent_A1'],
  //           'homestay': !this.bookedStatus['homestay'] && !this.bookedStatus['tent_homestay'] && !this.bookedStatus['tent_a1_homestay'],
  //           'tent_A': !this.bookedStatus['tent_A'] && !this.bookedStatus['tent_homestay'] && !this.bookedStatus['tent_a1_homestay'] && (!this.bookedStatus['tent_A1'] && !this.bookedStatus['tent_A2']),
  //           'tent_A1': !this.bookedStatus['tent_A1'] && !this.bookedStatus['tent_homestay'] && !this.bookedStatus['tent_a1_homestay'] && !this.bookedStatus['tent_A'] && !this.isWeekend,
  //           'tent_A2': !this.bookedStatus['tent_A2'] && !this.bookedStatus['tent_homestay'] && !this.bookedStatus['tent_A'] && !this.isWeekend,
  //           // 'tent_A1': !this.bookedStatus['tent_A1'] && !this.bookedStatus['tent_homestay'] && !this.bookedStatus['tent_A'],
  //           // 'tent_A2': !this.bookedStatus['tent_A2'] && !this.bookedStatus['tent_homestay'] && !this.bookedStatus['tent_A'],
  //         }

  //         // console.log('已預約狀況', this.bookedStatus)
  //         this.bookedAvailableSub$.next(this.bookedStatus)

  //         this.isEmpty = !(this.isBookedAvailable['tent_homestay'] && !this.hasTuesday()) &&
  //         !(this.isBookedAvailable['tent_a1_homestay'] && !this.hasTuesday()) &&
  //         !(this.isBookedAvailable['homestay'] && !this.hasTuesday()) &&
  //         !(this.isBookedAvailable['tent_A'] && !this.hasTuesday()) &&
  //         !(this.isBookedAvailable['tent_A1'] && !this.hasTuesday()) &&
  //         !(this.isBookedAvailable['tent_A2'] && !this.hasTuesday())
  //       }

  //       // return true

  //       // if(sub) {
  //       //   sub.unsubscribe()
  //       // }
  //     })
  // }

  // get sheet data 大包場
  // getTentsHomestayReservationData() {
  //   return new Observable(observer => {
  //     const orderType = OrderType.TENT_HOMESTAY;
  //     const sub = this.apiService.getCookerFilter(orderType).subscribe(res => {

  //       const orderKey: string[] = [
  //         'timestampUID',
  //         'order_id',
  //         'name',
  //         'prepaid_fee',
  //         'is_paid_pre_fee',
  //         'check_prepaid_timestamp',
  //         'phone',
  //         'email',
  //         'startDate',
  //         'endDate',
  //         'stayDateRange',
  //         'nights',
  //         'add_bed',
  //         'rest_fee',
  //         'is_paid_rest_fee',
  //         'total_fee',
  //         'order_source',
  //         'is_cancel_order'
  //       ]

  //       if(res) {
  //         this.orderListTH.length = 0
  //         res.rows.forEach((order: string[], i: number) => {
  //           this.orderListTH.push({});

  //           order.forEach((val: string, j) => {
  //             this.orderListTH[i][orderKey[j]] = val;
  //           });
  //         });
  //         this.orderListTH = this.orderListTH.filter(order => {
  //           // 不是取消狀態 也不是手動key單的空資料狀態
  //           return order.is_cancel_order === 'FALSE' || order.is_cancel_order === "";
  //         })
  //         let bookedArr: any[] = []
  //         this.orderListTH.forEach((order: any) => {

  //           bookedArr.push(order.stayDateRange);
  //         });

  //         observer.next(bookedArr)
  //         observer.complete()
  //       }
  //     });

  //     this.subList?.push(sub);
  //   })
  // }

  // get sheet data 小包區
  // getTentsA1HomestayReservationData() {
  //   return new Observable(observer => {
  //     const orderType = OrderType.TENT_A1_HOMESTAY;
  //     const sub = this.apiService.getCookerFilter(orderType).subscribe(res => {

  //       const orderKey: string[] = [
  //         'timestampUID',
  //         'order_id',
  //         'name',
  //         'prepaid_fee',
  //         'is_paid_pre_fee',
  //         'check_prepaid_timestamp',
  //         'phone',
  //         'email',
  //         'startDate',
  //         'endDate',
  //         'stayDateRange',
  //         'nights',
  //         'add_bed',
  //         'rest_fee',
  //         'total_fee',
  //         'is_paid_rest_fee',
  //         'order_source',
  //         'is_cancel_order'
  //       ]

  //       if(res) {
  //         this.orderListTA1H.length = 0
  //         res.rows.forEach((order: string[], i: number) => {
  //           this.orderListTA1H.push({});

  //           order.forEach((val: string, j) => {
  //             this.orderListTA1H[i][orderKey[j]] = val;
  //           });
  //         });
  //         this.orderListTA1H = this.orderListTA1H.filter(order => {
  //           // 不是取消狀態 也不是手動key單的空資料狀態
  //           return order.is_cancel_order === 'FALSE' || order.is_cancel_order === "";
  //         })
  //         let bookedArr: any[] = []
  //         this.orderListTA1H.forEach((order: any) => {

  //           bookedArr.push(order.stayDateRange);
  //         });

  //         observer.next(bookedArr)
  //         observer.complete()
  //       }
  //     });

  //     this.subList?.push(sub);
  //   })
  // }

  // get sheet data 宿屋
  // getHomestayReservationData() {
  //   return new Observable(observer => {
  //     const orderType = OrderType.HOMESTAY;
  //     const sub = this.apiService.getCookerFilter(orderType).subscribe(res => {

  //       const orderKey: string[] = [
  //         'timestampUID',
  //         'order_id',
  //         'name',
  //         'prepaid_fee',
  //         'is_paid_pre_fee',
  //         'check_prepaid_timestamp',
  //         'phone',
  //         'email',
  //         'startDate',
  //         'endDate',
  //         'stayDateRange',
  //         'nights',
  //         'adults',
  //         'children',
  //         'add_bed',
  //         'add_breakfast',
  //         'request',
  //         'rest_fee',
  //         'is_paid_rest_fee',
  //         'total_fee',
  //         'order_source',
  //         'is_cancel_order'
  //       ]

  //       if(res) {
  //         this.orderListH.length = 0
  //         res.rows.forEach((order: string[], i: number) => {
  //           this.orderListH.push({});

  //           order.forEach((val: string, j) => {
  //             this.orderListH[i][orderKey[j]] = val;
  //           });
  //         });

  //         this.orderListH = this.orderListH.filter(order => {
  //           // 不是取消狀態 也不是手動key單的空資料狀態
  //           return order.is_cancel_order === 'FALSE' || order.is_cancel_order === "";
  //         })
  //         let bookedArr: any[] = []
  //         this.orderListH?.forEach((order: any) => {
  //           bookedArr.push(order.stayDateRange);
  //         });
  //         observer.next(bookedArr)
  //         observer.complete()
  //       }
  //     });

  //     this.subList?.push(sub);
  //   })
  // }

  // get sheet data 露營區
  // getTentReservationData(campArea: string) {
  //   return new Observable(observer => {
  //     const orderType = OrderType.TENT
  //     const sub = this.apiService.getCookerFilter(orderType).subscribe(res => {

  //       const orderKey: string[] = [
  //         'timestampUID',
  //         'order_id',
  //         'name',
  //         'campArea',
  //         'prepaid_fee',
  //         'is_paid_pre_fee',
  //         'check_prepaid_timestamp',
  //         'mobile',
  //         'email',
  //         'startDate',
  //         'endDate',
  //         'stayDateRange',
  //         'nights',
  //         'tents',
  //         'rest_fee',
  //         'is_paid_rest_fee',
  //         'total_fee',
  //         'order_source',
  //         'is_cancel_order'
  //       ]

  //       if(res) {
  //         this.orderListTent.length = 0
  //         res.rows.forEach((order: string[], i: number) => {
  //           this.orderListTent.push({});

  //           order.forEach((val: string, j) => {
  //             this.orderListTent[i][orderKey[j]] = val;
  //           });
  //         });
  //         this.orderListTent = this.orderListTent.filter(order => {
  //           // 不是取消狀態 也不是手動key單的空資料狀態
  //           return order.is_cancel_order === 'FALSE' || order.is_cancel_order === "";
  //         })

  //         let filteredList: any[] = [];
  //         let bookedArr: any[] = []
  //         filteredList = this.orderListTent.filter(order => {
  //           return campArea === order.campArea;
  //         })

  //         filteredList.forEach((order: any) => {

  //           bookedArr.push(order.stayDateRange);
  //         });
  //         observer.next(bookedArr)
  //         observer.complete()
  //       }
  //     });

  //     this.subList?.push(sub);
  //   })
  // }

  // private isBookedDate(bookedNightsData: any) {
  //   return (map(bookedNightsData, this.dateStrIntoArray).map((stayRange: any) => {
  //     return intersection(this.getStayNightRange(), stayRange).length > 0;
  //   })).includes(true)
  // }

  // private dateStrIntoArray(dateStr: string) {
  //   return dateStr.split(',');
  // }

  // private getStayNightRange() {
  //   const startTS = new Date(this.changeDateToSlashFormat(this.dateStart)).getTime();
  //   const leaveTS = startTS + this.bookedNights * this.dayTS
  //   this.dateLeave = new Date(leaveTS).toLocaleDateString('zh-TW');

  //   if(leaveTS <= startTS) {
  //     // console.log('離開日期不得小於入住日期')
  //     return
  //   }

  //   // console.log('送出 start:', this.dateStart, 'end:',  this.dateLeave)

  //   let stayDateRange: any[] = []
  //   // const nights = (leaveTS - startTS) / dayTS
  //   let addedNightTS = 0
  //   for(let i = 0; i < this.bookedNights; i++) {
  //     addedNightTS = startTS + this.dayTS * i
  //     stayDateRange.push(addedNightTS)
  //   }

  //   this.stayDateRange = stayDateRange.map(date => new Date(date).toLocaleDateString('zh-TW'))

  //   return stayDateRange.map(date => new Date(date).toLocaleDateString('zh-TW'))
  // }

  // private changeDateToSlashFormat(dateStr: string) {
  //   return dateStr.split("-").join('/');
  // }

  // private dateToTSArr(dateStrArr: string[]): number[] {
  //   const splitArr = dateStrArr.map(str => {
  //     return str.split(',')
  //   })

  //   const dateTSrArr = flatten(splitArr).map((item: any) => {
  //     return new Date(item).getTime()
  //   })

  //   return dateTSrArr
  // }

  // private hasTuesday(): boolean {
  //   const hasTue = this.stayDateRange.map(date => {
  //     return new Date(date).getDay() === 2 || new Date(date).getDay() === 1;
  //   }).includes(true)
  //   const isTue = new Date(this.dateLeave).getDay() === 2 || new Date(this.dateLeave).getDay() === 1;

  //   return isTue || hasTue;
  // }

  // private checkIfDayOffOrWeekend() {
  //   this.getBookedNights();

  //   const startTS = new Date(this.changeDateToSlashFormat(this.dateStart)).getTime();
  //   const leaveTS = startTS + this.bookedNights * this.dayTS
  //   this.dateLeave = new Date(leaveTS).toLocaleDateString('zh-TW');

  //   let stayDateRange: any[] = []
  //   let addedNightTS = 0
  //   for(let i = 0; i < this.bookedNights; i++) {
  //     addedNightTS = startTS + this.dayTS * i
  //     stayDateRange.push(addedNightTS)
  //   }

  //   // 判斷客人是否有選取到星期五or六
  //   this.stayDateRange = stayDateRange.map(date => new Date(date).toLocaleDateString('zh-TW'))
  //   const isWeekend = stayDateRange.map(ts => {
  //     return new Date(ts).getDay() === 5 || new Date(ts).getDay() === 6 || new Date(ts).getDay() === 0 || new Date(ts).getDay() === 1;
  //   })
  //   // isWeekend -> 目前使用在不開放A1,A2散客使用的判斷
  //   this.isWeekend = isWeekend.includes(true);

  //   // 判斷客人是否有選取到星期二公休日
  //   const hasTue = stayDateRange.map(date => {
  //     return new Date(date).getDay() === 2;
  //   })
  //   const isTue = new Date(this.dateLeave).getDay() === 2;

  //   this.hasTue = hasTue.includes(true) || isTue;;

  //   // (暫時關閉)開放離開日可以是星期二
  //   this.isSatOrSunOrMonButTuesdayLeave = (new Date(startTS).getDay() === 5 && isTue) ||
  //   (new Date(startTS).getDay() === 6 && isTue) ||
  //   (new Date(startTS).getDay() === 0 && isTue) ||
  //   (new Date(startTS).getDay() === 1 && isTue)
  // }
}
