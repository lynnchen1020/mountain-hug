import { TestBed } from '@angular/core/testing';

import { ModalNewService } from './modal-new.service';

describe('ModalNewService', () => {
  let service: ModalNewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModalNewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
