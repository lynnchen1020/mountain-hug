import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ElementService {
  isNotScrollable: boolean;

  constructor() {
    this.isNotScrollable = false;
  }
}
