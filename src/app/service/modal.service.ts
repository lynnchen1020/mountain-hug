import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { CurrentModalEvent, IModalService } from '../models/imodal-service';

@Injectable({
  providedIn: 'root',
})
export class ModalService implements IModalService {
  backdrop: boolean | 'static' = true;
  isMulti = false;
  isActive = false;

  onComponentChange: Subject<any> = new Subject<any>();
  onClose: Subject<any> = new Subject<any>();
  onCancel: Subject<any> = new Subject<any>();
  onSubmit: Subject<any> = new Subject<any>();

  public currentModalEvent!: CurrentModalEvent;

  constructor() {}

  afterClosed(): Observable<any> {
    return new Observable((subscribe) => {
      this.onClose.next(true);
      subscribe.next(true);
      subscribe.complete();
    });
  }

  afterCanceled(): Observable<any> {
    return new Observable((subscribe) => {
      this.onCancel.next(true);
      subscribe.next(true);
      subscribe.complete();
    });
  }

  afterSubmitted(): Observable<any> {
    return new Observable((subscribe) => {
      this.onSubmit.next(true);
      subscribe.next(true);
      subscribe.complete();
    });
  }

  open(component: any, props?: any) {
    const currentModalEvent = {
      onClose: new Subject(),
      onCancel: new Subject(),
      onSubmit: new Subject(),
    };

    this.onComponentChange.next({
      component,
      props,
      currentModalEvent,
    });

    this.isActive = true;
    this.currentModalEvent = currentModalEvent;

    return this;
  }

  close() {
    this.onComponentChange.next(null);
  }
}
