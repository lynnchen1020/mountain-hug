import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { environment } from 'src/environments/environment';

declare global {
  interface Window {
    dataLayer: any[];
  }
}

@Injectable({
  providedIn: 'root'
})
export class GtmService {
  window: Window | any;
  gtmTag: string;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    @Inject(PLATFORM_ID) private readonly platformId: Object) {

    if (isPlatformBrowser(this.platformId)) {
      this.window = this.document.defaultView;
      if (!this.window.dataLayer) {
        this.window.dataLayer = [];
      }
    }

    this.gtmTag = 'GTM-KNLV6F9'
  }

  public init() {
    if(!environment.production) {
      return;
    }
    // GA 開關
    this.mountGtmScript();
  }

  private mountGtmScript() {
    if (isPlatformBrowser(this.platformId)) {
      const gtmFirstScript = document.createElement('script');
      const gtmSecondScript = document.createElement('noscript');

      gtmFirstScript.type = 'text/javascript';

      gtmFirstScript.innerHTML = `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer', '${this.gtmTag}');`


      gtmSecondScript.innerHTML = `<iframe src="https://www.googletagmanager.com/ns.html?id=${this.gtmTag}" height="0" width="0" style="display:none;visibility:hidden"></iframe>`;

      this.document.head.appendChild(gtmFirstScript);
      this.document.body.appendChild(gtmSecondScript);
    }
  }
}
