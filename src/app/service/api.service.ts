import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

interface IRecaptchaToken {
  token: string;
}

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  constructor(private http: HttpClient) { }

  public sendRecToken(token: IRecaptchaToken): Observable<any> {
    let fd = new FormData();

    const config = {
    url: 'https://script.google.com/macros/s/AKfycbwbOfNWBUolpGj7AjJc387EUdyK4d_y4UaSyd2C0t4LTZAc8qxUa7cLQIxCdQnVcmyARA/exec',
    type: 'POST',
    mode: 'no-cors',
    }

    Object.entries(token).forEach(([key, value]) => {
      fd.append(key, value);
    })

    return this.http.post(config.url, fd)
  }

  // NOTE: 陣列產生 headers 設定
  private handleHttpHeader(list: any[]): Headers {
    const res: any = {};
    list.forEach((item) => {
      res[item.key] = item.value;
    });

    return new Headers({ res });
  }

  public getOrders(payload: string): Observable<any> {
    const url = `${environment.apiUrl}/api/order/${payload}`
    return this.http.get(url, {responseType: 'json'})
  }
  public getInnOrders(): Observable<any> {
    const url = `${environment.apiUrl}/api/order/inn`
    return this.http.get(url, {responseType: 'json'})
  }
  public sendCampOrders(payload: any): Observable<any> {
    const check_url = `${environment.apiUrl}/api/order/${payload.area}/${payload.order_id}?timestamp=${payload.created_time}&startDate=${payload.start_date}&nights=${payload.nights}`
    const url = `${environment.apiUrl}/api/order/${payload.area}`
    return new Observable(subsriber => {
      // Check Duplicate Api
      this.http.get(check_url, {responseType: 'json'}).subscribe((res: any) => {
        if(res && res.status === 'failed') {
          subsriber.next(false);
          subsriber.complete();

          return
        }

        this.sendEmail(payload).subscribe((res: any) => {
          const obj = Object.assign(payload, {
            event_id: res.event_id
          });

          this.http.post(url, obj).subscribe((res) => {
            if(res) {
              subsriber.next(true);
              subsriber.complete();
            }
          })
        });
      })

    })
  }
  public sendEmail(payload: any){
    const url = `${environment.apiUrl}/api/mailsend`
    return this.http.post(url, payload);
  }
  public getHoliday(): Observable<any> {
    const url = `${environment.apiUrl}/api/back/holiday`
    return this.http.get(url, {responseType: 'json'})
  }
}

