import { CalendarComponent } from './calendar.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HolidayResolver } from 'src/app/resolver/holiday.resolver';

const routes: Routes = [
  {
    path: '',
    component: CalendarComponent,
    resolve: {
      holiday: HolidayResolver // 在本機測試日曆功能，執行 npm run build:ssr:prodMode:ForTest
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CalendarRoutingModule { }
