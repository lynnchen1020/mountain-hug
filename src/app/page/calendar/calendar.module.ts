import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarRoutingModule } from './calendar-routing.module';
import { ShareModule } from 'src/app/module/share/share.module';
import { CalendarComponent } from './calendar.component';


@NgModule({
  declarations: [CalendarComponent],
  imports: [
    CommonModule,
    ShareModule,
    CalendarRoutingModule
  ]
})
export class CalendarModule { }
