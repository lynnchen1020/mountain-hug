import { OrderService } from './../../service/order.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { countBy, min } from 'src/app/helper/lodash';
import { find, flatten, get, indexOf } from 'lodash';
import { FeeType, OrderType, OrderTypeTentsMax } from 'src/app/enum/order-type';
import { ActivatedRoute, Router } from '@angular/router';
import { ElementService } from 'src/app/service/element.service';
import { getAreaTentsMax, getAreaTitle, getCurrentDay, getOfficialHolidays } from 'src/app/helper/helper';
import { Common } from 'src/app/enum/common';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.sass']
})
export class CalendarComponent implements OnInit, OnDestroy {
  initHolidays = false;
  orderListTemp: any[] = [];
  orderList: any[] = [];
  orderListTent: any[] = [];
  stayDateRange: any[] = [];
  dateMin = '';
  dateMax = '';
  dateStart = '';
  dateEnd = '';
  isWeekend = false;
  hasTue = false;
  isLoading = false;
  isToggleNights = false;
  currentNightsIdx = 0;
  resData: any;
  isEmpty = true;
  isShowPicker = false;
  tempDate = {};

  bookedNightsData: {
    [key: string]: string[]
  } = {}
  isBookedAvailable: {
    [key: string]: boolean
  } = {}
  bookedNightsTS: {
    [key: string]: number[]
  } = {}

  bookedStatus: {
    [key: string]: number[]
  } = {}

  dateLeave = '';
  bookedDate: string[] = [];
  dayTS = 86400000;

  // New Variables Start
  OrderType = OrderType;
  holidays: number[] = [];
  allBookedTents: {
    [key: string]: number
  } = {}

  eachDateTents: {
    [key: string]: number[]
  } = {}

  startTS: number = 0;
  leaveTS: number = 0;

  // maxTents: {
  //   [key: string]: number
  // } = {
  //   'camp_a': 4,
  //   'camp_b': 5,
  //   'camp_c': 0,
  //   'camp_d': 0,
  //   'inn': 1,
  // }

  nightsMapping = [
    {
      'title': '2天1夜',
      'nights': 1,
    },
    {
      'title': '3天2夜',
      'nights': 2,
    },
    {
      'title': '4天3夜',
      'nights': 3,
    },
  ]

  nights: number = 1;
  isShowCards: boolean = false;
  isLoadCards: boolean = false;
  dayTSBeforePrivateRoom: number = 0;
  holidayTSBeforeStartDate: number = 0;
  officialHolidayTSBeforeStartDate: number = 0;
  dayFullTS: number = 0;
  firstDayOfEveryOfficalHolidays: number = 0;
  daysBefore3of2OfficialHolidays: number[] = []

  subList: Subscription[] = [];
  getAreaTitle = getAreaTitle;
  getAreaTentsMax = getAreaTentsMax;
  OrderTypeTentsMax = OrderTypeTentsMax;
  FeeType = FeeType;
  // New Variables End

  constructor(
    public orderService: OrderService,
    private route: ActivatedRoute,
    private router: Router,
    public elementService: ElementService
  ) {
    this.getDateString();
  }

  get totalTentsAmount() {
    return OrderTypeTentsMax.CAMP_A +
      OrderTypeTentsMax.CAMP_B +
      OrderTypeTentsMax.CAMP_C +
      OrderTypeTentsMax.CAMP_D +
      OrderTypeTentsMax.INN
  }

  ngOnInit(): void {
    const sub = this.route.data.subscribe(data => {
      if(data && get(data, 'holiday')) {
        this.orderService.getUniqBookedDate(true);
        this.getHoliday(get(data, 'holiday'))
      };
    });
    if(sub) {
      this.subList.push(sub);
    }
  }

  ngOnDestroy(): void {
    if(this.subList.length > 0) {
      this.subList.forEach(sub => {
        sub.unsubscribe();
      })
    }
  }

  // new flow func start
  get dayStart() {
    return getCurrentDay(this.dateStart);
  }

  get dayLeave() {
    return getCurrentDay(this.dateLeave);
  }

  private timezoneOffsetAndDayLightTime(localTimestamp: number) {
    const localOffset = new Date().getTimezoneOffset() / 60;

    const diff = (-localOffset * 60 * 60 * 1000) - (8 * 60 * 60 * 1000);

    // 歐洲有夏令時間，夏天會撥快1小時
    const currentTimezone = moment.tz.guess();
    const dayLightSavingForOneHour = this.isDaylightSavingTime(new Date(localTimestamp), currentTimezone) ? 3600000 : 0

    return diff + dayLightSavingForOneHour
  }

  // 檢查國外時區是否為夏令時間(夏至會快ㄧ小時)
  private isDaylightSavingTime(date: any, timeZone: any) {
    return moment.tz(date, timeZone).isDST();
  }

  // date related start
  // TODO: 開放半年邏輯還需修正
  getDateString() {
    const start = new Date();
    const end = new Date();
    // Lynn 設定開始月份，由此計算往後開放6個月
    // const end = new Date('2023/9/1');

    start.setDate((start.getDate()));
    const USStartDate = start.setHours((start.getHours()));
    // const USEndDate = end.setHours((end.getHours() - 12)) + dayTS * 90; // 開放 90 days

    const isEndDate31 = end.getDate() === 31;
    // 由於31號系統計算為下個月份，因此特例修改將 31 號 ＵSEndDate timestamp 減一天
    const USEndDate = isEndDate31 ? end.setMonth(end.getMonth() + 6) - (this.dayTS * 2) : end.setMonth(end.getMonth() + 6) // 開放6個月

    const USEndDateTS = +this.getLastDateInThisMonth(new Date(USEndDate).toLocaleDateString('zh-TW'))


    this.dateMin = this.formatDate(USStartDate);

    this.dateMax = this.formatDate(USEndDateTS);

    // Lynn -> 修改日曆顯示的最大月份
    // this.dateMax = this.formatDate(1696003200000); // 帶入2023/9/30的 timestamp
    // this.dateMax = this.formatDate(1698681600000); // 帶入2023/10/31的 timestamp
    // this.dateMax = this.formatDate(1701273600000); // 帶入2023/11/30的 timestamp
    // this.dateMax = this.formatDate(1703952000000); // 帶入2023/12/31的 timestamp
    // this.dateMax = this.formatDate(1706630400000); // 帶入2024/1/31的 timestamp
    // this.dateMax = this.formatDate(1709136000000); // 帶入2024/2/29的 timestamp
    // this.dateMax = this.formatDate(1711814400000); // 帶入2024/3/31的 timestamp
    // this.dateMax = this.formatDate(1714406400000); // 帶入2024/4/30的 timestamp
    // this.dateMax = this.formatDate(1717084800000); // 帶入2024/5/31的 timestamp
    // this.dateMax = this.formatDate(1719676800000); // 帶入2024/6/30的 timestamp
    // this.dateMax = this.formatDate(1722355200000); // 帶入2024/7/31的 timestamp
    // this.dateMax = this.formatDate(1725033600000); // 帶入2024/8/31的 timestamp
    // this.dateMax = this.formatDate(1748620800000); // 帶入2025/5/31的 timestamp

    // this.dateStart = this.dateMin;
    this.dateEnd = this.dateMax;

    // const leaveTS = new Date(this.dateStart).getTime() + this.dayTS
    // this.dateLeave = new Date(leaveTS).toLocaleDateString();
  }
  private getLastDateInThisMonth(date: string) {
    const separateDay = date.split('/');
    const newDate = `${separateDay[0]}/${separateDay[1]}/20`;
    const time = new Date(newDate);
    time.setMonth(time.getMonth());
    time.setDate(0);
    return time;
  }
  private formatDate(usDate: number): string {
    const newTime: Date = new Date(usDate);
    // Date 轉成 YYYY-MM-DD
    return `${newTime.getFullYear()}/${(newTime.getMonth() + 1).toString()}/${newTime.getDate().toString()}`;
  }
  // date related end


  public navigateTo(orderType: string) {
    this.orderService.maxTentsAmount = this.countLeftTents(orderType, this.startTS, this.nights)
    this.orderService.currentArea = orderType;
    this.router.navigate([`/start-date/${orderType}`], {
      queryParams: {
        startDate: this.dateStart,
        leaveDate: this.dateLeave,
      }
    })
  }

  private changeDateToSlashFormat(dateStr: string) {
    return dateStr.split("-").join('/');
  }

  private showCardsAnimation() {
    this.isLoadCards = false;
    this.isLoading = true;

    const timeout = setTimeout(() => {
      this.isLoadCards = true;
      this.isLoading = false;
      if(timeout) {
        clearTimeout(timeout)
      }
    }, 500)
  }

  changeStartDate($event: string) {
    this.dateStart = $event
    this.isShowCards = true;
    this.showCardsAnimation();

    const startTS = new Date(this.changeDateToSlashFormat(this.dateStart)).getTime();

    const leaveTS = startTS + this.dayTS * this.nightsMapping[this.currentNightsIdx].nights
    this.dateLeave = new Date(leaveTS).toLocaleDateString('zh-TW');


    this.startTS = startTS;
    this.leaveTS = leaveTS;

    this.orderService.dateStart = this.dateStart
    this.orderService.dateLeave = this.dateLeave

    this.countDayBeforePrivateRoom();
    this.countDayBeforeHoliday();
    this.countDayFull();
    this.countDayBeforeOfficialHoliday();
    this.countFirstDayOfOfficialHoliday();



    const el = document.getElementsByClassName('wrapper')[0]
    if(el) {
      el.scrollTop = 0
    }
  }

  public changeNights(nights: number, currentIdx: number) {
    // if(this.currentNightsIdx === currentIdx) {
    //   this.showCardsAnimation();
    //   this.isToggleNights = false;
    //   return;
    // }

    this.showCardsAnimation();


    this.nights = nights;
    this.orderService.bookedNights = nights;
    this.currentNightsIdx = currentIdx;
    this.isToggleNights = false;

    const arr = Array.from({ length: nights + 1 }, (_, index) => this.startTS + index * this.dayTS)
    this.dateLeave = new Date(arr[arr.length - 1]).toLocaleDateString('zh-TW');

    this.orderService.dateStart = this.dateStart
    this.orderService.dateLeave = this.dateLeave
  }

  public countLeftTents(area: string, dateStartTS: number, nights: number) {
    const arr = Array.from({ length: nights }, (_, index) => dateStartTS + index * this.dayTS).map((ts: number) => {

      const leftTents = Number(getAreaTentsMax(area)) - countBy(this.orderService.eachDateTents[area], ts + this.timezoneOffsetAndDayLightTime(ts));
      if(leftTents === 0) {
        return 0;
      } else {
        return leftTents || getAreaTentsMax(area);
      }
    })
    return min(arr);
  }

  // new manual holiday list
  private getHoliday(data: any) {
    if(data) {
      this.holidays = data.map((item: any) => {
        return item.holiday_ts
      });
      this.initHolidays = true;
    }
  }

  // backend api
  // private getUniqBookedDate() {
  //   const sub = zip(
  //     this.orderService.getCampOrders(OrderType.CAMP_A),
  //     this.orderService.getCampOrders(OrderType.CAMP_B),
  //     this.orderService.getCampOrders(OrderType.CAMP_C),
  //     this.orderService.getCampOrders(OrderType.CAMP_D),
  //     this.orderService.getInnOrders(),
  //   ).subscribe(res => {
  //     const data: {[key: string]: number[]} = {
  //       'camp_a': flatten(get(res, '0', '').map((item: any) => item.tents_per_day)),
  //       'camp_b': flatten(get(res, '1', '').map((item: any) => item.tents_per_day)),
  //       'camp_c': flatten(get(res, '2', '').map((item: any) => item.tents_per_day)),
  //       'camp_d': flatten(get(res, '3', '').map((item: any) => item.tents_per_day)),
  //       'inn': flatten(get(res, '4', '').map((item: any) => item.tents_per_day)),
  //     }

  //     this.eachDateTents = data;
  //     this.orderService.eachDateTents = this.eachDateTents;

  //     // 算出各區剩餘帳數
  //     // const eachDayLeftTents = {
  //     //   'uniqBookedDateA': uniq(flatten(get(res, '0', '').map((item: any) => item.tents_per_day))),
  //     //   'uniqBookedDateB': uniq(flatten(get(res, '1', '').map((item: any) => item.tents_per_day))),
  //     //   'uniqBookedDateC': uniq(flatten(get(res, '2', '').map((item: any) => item.tents_per_day))),
  //     //   'uniqBookedDateD': uniq(flatten(get(res, '3', '').map((item: any) => item.tents_per_day))),
  //     // }
  //     // console.log('eachDayLeftTents', eachDayLeftTents)


  //     // 算出每天總剩餘帳數
  //     const uniqBookedTSKeys = uniq(
  //       concat(
  //         flatten(data[OrderType.CAMP_A]),
  //         flatten(data[OrderType.CAMP_B]),
  //         flatten(data[OrderType.CAMP_C]),
  //         flatten(data[OrderType.CAMP_D]),
  //         flatten(data[OrderType.INN])
  //       )
  //     )
  //     const countTentsList = concat(
  //       flatten(data[OrderType.CAMP_A]),
  //       flatten(data[OrderType.CAMP_B]),
  //       flatten(data[OrderType.CAMP_C]),
  //       flatten(data[OrderType.CAMP_D]),
  //       flatten(data[OrderType.INN])
  //     )
  //     const obj: any = {}
  //     uniqBookedTSKeys.forEach(uniqTS => {
  //       obj[String(uniqTS)] = countBy(countTentsList, uniqTS)
  //     })

  //     // 顯示在日曆上每天剩餘的帳數
  //     this.allBookedTents = obj;
  //   })

  //   if(sub) {
  //     this.subList.push(sub);
  //   }
  // }

  // 檢查是否30天以上，是的話為包區模式
  isPrivateRoom(): boolean {
    const curTS = new Date().getTime();
    return (curTS + this.dayTS * Common.IS_PRIVATE_ROOM_LIMIT) - this.startTS + this.timezoneOffsetAndDayLightTime(curTS)  < 0
  }

  // 判斷幾天幾夜顯示邏輯
  // 限包區的前一天/兩天
  public displayPrivateRoomNights(daysBeforeTS: number, nights: number, stayOverNights: number) {
    if(stayOverNights === 2) {
      return daysBeforeTS === this.dayTS && nights > stayOverNights;
    }

    return daysBeforeTS === 0 && nights > stayOverNights
  }
  // 排休假的前一天/兩天
  public displayHolidayNights(daysBeforeTS: number, nights: number, stayOverNights: number) {
    if(stayOverNights === 2) {
      return daysBeforeTS === this.dayTS * 2 && nights > stayOverNights
    }

    return daysBeforeTS === this.dayTS && nights > stayOverNights
  }
  // 客滿的前一天/兩天
  public displayFullNights(daysBeforeTS: number, nights: number, stayOverNights: number) {
    if(stayOverNights === 2) {
      return daysBeforeTS === this.dayTS * 2 && nights > stayOverNights
    }

    return daysBeforeTS === this.dayTS && nights > stayOverNights
  }
  // 國定假日前一天/兩天
  public displayOfficialHolidayNights(daysBeforeTS: number, nights: number, stayOverNights: number) {
    if(stayOverNights === 2) {
      return daysBeforeTS === this.dayTS * 2 && nights > stayOverNights
    }

    return daysBeforeTS === this.dayTS && nights > stayOverNights
  }

  // 國定假日第一天
  public displayFirstDayOfficialHolidayNights(daysBeforeTS: number, nights: number, stayOverNights: number) {
    return daysBeforeTS === this.startTS && (nights !== stayOverNights)
  }

  countDayBeforePrivateRoom() {
    const curTS = new Date().toLocaleDateString('zh-TW');
    const ts = new Date(curTS).getTime();
    this.dayTSBeforePrivateRoom = (ts + this.dayTS * Common.IS_PRIVATE_ROOM_LIMIT) - this.startTS;
  }

  countDayBeforeHoliday() {
    let ts = find(this.holidays, (ts) => {
      return ts > this.startTS
    })
    const countTS = ts ? ts : 0;
    this.holidayTSBeforeStartDate = countTS - this.startTS;
  }

  countDayFull() {
    let arr: any[] = []
    Object.keys(this.orderService.allBookedTents).map(ts => {
      if(this.orderService.allBookedTents[ts] > 9) {
        arr.push(ts);
      }
    })
    const countTS = find(arr, (ts) => {
      return Number(ts) > this.startTS
    })

    this.dayFullTS = countTS - this.startTS
  }

  countFirstDayOfOfficialHoliday() {
    const firstDayOfEveryOfficalHolidays_2024 = getOfficialHolidays().year_2024
    const firstDayOfEveryOfficalHolidays_2025 = getOfficialHolidays().year_2025
    const firstDayOfEveryOfficalHolidays_All = firstDayOfEveryOfficalHolidays_2024.concat(firstDayOfEveryOfficalHolidays_2025)
    const firstDayOfEveryOfficalHolidays = firstDayOfEveryOfficalHolidays_All
      .map(days => days)
      .map(day => {
        if(day.length > 1) {
          return new Date(day[0]).getTime() + this.timezoneOffsetAndDayLightTime(new Date(day[0]).getTime());
        }
        return 0;
    })

    // TODO: 如果連假長度超過 5 天，把第三天也放入3天2夜的限制
    const thiDayOfEveryOfficialHolidays = getOfficialHolidays().year_2024
      .filter(days => days.length > 4)
      .map(day => {
      return new Date(day[2]).getTime() + this.timezoneOffsetAndDayLightTime(new Date(day[2]).getTime());
    })

    const combine3of2OfficialHolidays = firstDayOfEveryOfficalHolidays.concat(thiDayOfEveryOfficialHolidays)

    const pos = combine3of2OfficialHolidays.indexOf(this.startTS)
    this.firstDayOfEveryOfficalHolidays = combine3of2OfficialHolidays[pos]

    if(this.firstDayOfEveryOfficalHolidays === this.startTS) {
      // 連假第一天，只能選 3天2夜
      this.changeNights(2, 1)
    }
  }

  countDayBeforeOfficialHoliday() {
    const firstDayOfEveryOfficalHolidays_2024 = getOfficialHolidays().year_2024
    const firstDayOfEveryOfficalHolidays_2025 = getOfficialHolidays().year_2025
    const firstDayOfEveryOfficalHolidays_All = firstDayOfEveryOfficalHolidays_2024.concat(firstDayOfEveryOfficalHolidays_2025)

    const firstDayOfEveryOfficalHolidays = firstDayOfEveryOfficalHolidays_All
      .map(days => days)
      .map(day => {
      return new Date(day[0]).getTime() + this.timezoneOffsetAndDayLightTime(new Date(day[0]).getTime());
    })
    let ts = find(firstDayOfEveryOfficalHolidays, (ts) => {
      return ts > this.startTS
    })
    const countTS = ts ? ts : 0;
    this.officialHolidayTSBeforeStartDate = countTS - this.startTS;
  }

  // public pickNights(idx: number, currentIdx: number) {
  //   this.nights = idx;
  //   this.currentNightsIdx = currentIdx;
  //   this.isToggleNights = false;
  // }

  public toggleNights() {
    this.isToggleNights = !this.isToggleNights;
  }

}
