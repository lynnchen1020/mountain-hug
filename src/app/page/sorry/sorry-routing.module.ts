import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SorryComponent } from './sorry.component';

const routes: Routes = [
  {
    path: '',
    component: SorryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SorryRoutingModule { }
