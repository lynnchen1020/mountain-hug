import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SorryRoutingModule } from './sorry-routing.module';
import { ShareModule } from 'src/app/module/share/share.module';
import { SorryComponent } from './sorry.component';


@NgModule({
  declarations: [SorryComponent],
  imports: [
    CommonModule,
    ShareModule,
    SorryRoutingModule
  ]
})
export class SorryModule { }
