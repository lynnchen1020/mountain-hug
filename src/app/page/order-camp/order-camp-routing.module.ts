import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderCampComponent } from './order-camp.component';

const routes: Routes = [
  {
    path: '',
    component: OrderCampComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderCampRoutingModule { }
