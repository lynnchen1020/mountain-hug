import {
  AfterContentChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChildren,
} from '@angular/core';
import {
  FormBuilder,
  FormControlName,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { flatten } from 'lodash';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import {
  Observable,
  fromEvent,
  merge,
  debounceTime,
  Subscription,
  Subject,
} from 'rxjs';
import { ConfirmBoxComponent } from 'src/app/components/modal/confirm-box/confirm-box.component';
import { Common } from 'src/app/enum/common';
import { FeeType, OrderPreRatio, OrderType } from 'src/app/enum/order-type';
import {
  getCurrentDay,
  getAreaTitle,
  getAreaTentsMax,
  getOfficialHolidays,
} from 'src/app/helper/helper';
import { countBy, get, intersection, min } from 'src/app/helper/lodash';
import { ApiService } from 'src/app/service/api.service';
import { ElementService } from 'src/app/service/element.service';
import { ModalNewService } from 'src/app/service/modal-new.service';
import { OrderService } from 'src/app/service/order.service';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-order-camp',
  templateUrl: './order-camp.component.html',
  styleUrls: ['./order-camp.component.sass'],
})
export class OrderCampComponent
  implements OnInit, OnDestroy, AfterViewInit, AfterContentChecked
{
  // TODO: 整理 code 把 OrderComponent 拔掉
  @ViewChildren(FormControlName, { read: ElementRef })
  order: any;
  tentPreDayAndTents: { [key: string]: any } = {};
  dateRange: number[] = [];
  getAreaTitle = getAreaTitle;
  isLoading = false;
  subList?: Subscription[] = [];
  tentsRange: number[] = [];
  status = '';
  dayTS = 24 * 60 * 60 * 1000;
  stayOverDays: number[] = [];
  currentGalleryImg = '';
  gallery: string[] = [''];
  OrderType = OrderType;
  OrderPreRatio = OrderPreRatio;
  dayCheckIn = '0';
  dayCheckOut = '0';
  checkOverbooked$ = new Subject();

  // Form default
  form: FormGroup | undefined;
  formControls: ElementRef[] = [];
  formControlNameMapping: {
    [key: string]: string;
  };
  validationMessages!: {
    [key: string]: { [key: string]: string | { [key: string]: string } };
  };
  message: { [key: string]: string } = {};

  FeeType = FeeType;

  constructor(
    protected router: Router,
    protected apiService: ApiService,
    protected route: ActivatedRoute,
    protected modService: ModalNewService,
    protected recaptchaV3Service: ReCaptchaV3Service,
    protected formBuilder: FormBuilder,
    protected cdref: ChangeDetectorRef,
    public elementService: ElementService,
    public orderService: OrderService
  ) {
    this.order = {
      campArea: '',
      name: '',
      mobile: '',
      email: '',
      startDate: '',
      endDate: '',
      nights: 1,
      tents: 1,
      // people: 0,
      // five_digits: '',
      fee: 0,
      paid: false,
    };

    this.form = this.formBuilder.group({
      name: [
        //Name
        '',
        [Validators.required],
      ],
      phone: [
        //Phone
        '',
        [Validators.required, Validators.pattern('^[0-9]*$')],
      ],
      email: [
        //Email
        '',
        [Validators.required, Validators.email],
      ],
      // nights: [ //Nights
      //   '',
      //   [
      //     Validators.required,
      //   ]
      // ],
      tents: [
        //Tents
        '',
        [Validators.required],
      ],
      // five_digits: [ //5 Digits
      //   '',
      //   [
      //     Validators.required,
      //     Validators.pattern("^((\\+91-?)|0)?[0-9]{5}$")
      //   ]
      // ],
    });

    this.formControlNameMapping = {
      name: 'name',
      phone: 'phone',
      email: 'email',
      tents: 'tents',
      // people: OrderTent.PEOPLE,
      nights: 'nights',
      // five_digits: OrderTent.FIVE_DIGITS,
    };

    this.validationMessages = {
      name: {
        required: '請輸入訂位人姓名',
      },
      phone: {
        required: '請輸入訂位人聯絡電話',
        pattern: '請輸入正確的電話格式(請勿包含特殊符號，例如: -, _)',
      },
      email: {
        required: '請輸入您的 Email',
        email: '請輸入正確的 Email 格式',
      },
      // five_digits: {
      //   required: "請輸入您匯款帳號末5碼",
      //   pattern: "請輸入正確數字格式"
      // },
    };

    this.currentGalleryImg = "url('/assets/img/tent/img_1.jpg')";
    this.gallery = [
      "url('/assets/img/tent/img_1.jpg')",
      "url('/assets/img/tent/img_2.jpg')",
      "url('/assets/img/tent/img_3.jpg')",
      "url('/assets/img/tent/img_4.jpg')",
      "url('/assets/img/tent/img_5.jpg')",
      "url('/assets/img/tent/img_6.jpg')",
      "url('/assets/img/tent/img_7.jpg')",
      "url('/assets/img/tent/img_8.jpg')",
      "url('/assets/img/tent/img_9.jpg')",
      "url('/assets/img/tent/img_10.jpg')",
      "url('/assets/img/tent/img_11.jpg')",
    ];
  }

  totalFee() {
    // 陣列裡有 5 6 就算假日
    // TODO: getStayOverDays 這個方法攸關國定假日的費用，要特別注意，找時間重構
    const normalDaysFee =
      this.order.tents *
      FeeType.NORMAL *
      this.getStayOverDays(
        new Date(this.orderService.dateStart).getTime(),
        'normal'
      );
    const weekendDaysFee =
      this.order.tents *
      FeeType.WEEKEND *
      this.getStayOverDays(
        new Date(this.orderService.dateStart).getTime(),
        'weekend'
      );
    return weekendDaysFee + normalDaysFee || 0;
  }

  tentsFee() {
    const normalDaysFee =
      this.order.tents *
      FeeType.NORMAL *
      this.getStayOverDays(
        new Date(this.orderService.dateStart).getTime(),
        'normal'
      );
    const weekendDaysFee =
      this.order.tents *
      FeeType.WEEKEND *
      this.getStayOverDays(
        new Date(this.orderService.dateStart).getTime(),
        'weekend'
      );
    return normalDaysFee + weekendDaysFee || 0;
  }

  // 計算訂金
  prePaidFee() {
    return this.totalFee() * OrderPreRatio.TENT || 0;
  }

  // 計算尾款
  restFee() {
    return this.totalFee() - this.prePaidFee() || 0;
  }

  get dateCheckIn() {
    return this.route.snapshot.queryParams['startDate'];
  }

  get dateCheckInTS() {
    const checkInDate = this.orderService.dateStart;
    return new Date(checkInDate).getTime();
  }

  // overNights() {
  //   const checkInDate = this.dateCheckIn.split('/').join(',');
  //   const checkOutDate = this.dateCheckOut.split('/').join(',');
  //   const minus = new Date(checkOutDate).getTime() - new Date(checkInDate).getTime();
  //   console.log('in:', checkInDate, new Date(checkInDate).getTime(), 'out:', checkOutDate, new Date(checkOutDate).getTime(), '相減：', minus / this.dayTS)
  //   // return
  // }

  ngOnInit(): void {
    // super.ngOnInit();
    if (
      this.orderService.dateStart === '' ||
      this.orderService.dateLeave === ''
    ) {
      alert(
        '你尋找的網頁使用了你輸入的資料。返回該頁會重複你剛才的行動，請返回重新選擇入住日期'
      );
      this.router.navigate(['/pick-tent'], { replaceUrl: true });
    }
    this.bindEventNew();
    this.initFormNew();
  }

  ngOnDestroy(): void {
    this.destroyEventNew();
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  ngAfterViewInit(): void {
    const addBlurs: Observable<any>[] = this.formControls.map(
      (formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur')
    );
    merge(this.form?.valueChanges, ...addBlurs)
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.message = this.invalidInputsNew(this.form!);
      });
  }

  invalidInputsNew(formgroup: FormGroup): { [key: string]: string } {
    let messages: { [key: string]: string | any } = {};
    for (const input in formgroup.controls) {
      const key = formgroup.controls[input];
      if (key instanceof FormGroup) {
        const nestedGroupMessages = this.invalidInputsNew(key);
        Object.assign(messages, nestedGroupMessages);
      } else {
        if (this.validationMessages[input]) {
          messages[input] = '';
          if (key.errors && (key.dirty || key.touched)) {
            Object.keys(key.errors).map((messageKey) => {
              if (this.validationMessages[input][messageKey]) {
                messages[input] = this.validationMessages[input][messageKey];
              }
            });
          }
        }
      }
    }
    return messages;
  }

  // 檢查國外時區是否為夏令時間(夏至會快ㄧ小時)
  private isDaylightSavingTime(date: any, timeZone: any) {
    return moment.tz(date, timeZone).isDST();
  }

  private timezoneOffsetAndDayLightTime(localTimestamp: number) {
    const localOffset = new Date().getTimezoneOffset() / 60;

    const diff = (-localOffset * 60 * 60 * 1000) - (8 * 60 * 60 * 1000);

    // 歐洲有夏令時間，夏天會撥快1小時
    const currentTimezone = moment.tz.guess();
    const dayLightSavingForOneHour = this.isDaylightSavingTime(new Date(localTimestamp), currentTimezone) ? 3600000 : 0

    return Number(diff + dayLightSavingForOneHour)
  }

  // private getPrivateHolidays() {
  //   const sub = this.apiService.getHoliday().subscribe(data => {
  //     if(data) {
  //       this.privateHolidays = data.map((item: any) => {
  //         return item.holiday_ts
  //       });

  //       if(sub) {
  //         sub.unsubscribe();
  //       }
  //     }
  //   })
  // }

  bindEventNew() {
    const sub2 = this.orderService.checkIfAvailableSub$.subscribe(
      (isBooked: any) => {
        const sub3 = this.apiService.getHoliday().subscribe((data) => {
          if (data) {
            const holidays = data.map((item: any) => {
              return item.holiday_ts;
            });
            if (isBooked) {
              const overbooked = get(
                this.tentPreDayAndTents,
                'dateRange',
                []
              ).map((ts: number) => {
                if (ts) {
                  return (
                    Number(getAreaTentsMax(this.orderService.currentArea)) -
                      isBooked[this.orderService.currentArea][ts] <
                      this.tentPreDayAndTents['tents'] || holidays.includes(ts)
                  );
                }
              });

              if (overbooked.includes(true)) {
                this.isLoading = false;
                alert(
                  '很抱歉，您選擇的日期區間已被其他客人預約，請重新選擇日期，謝謝！'
                );
                this.checkOverbooked$.next(true);
                this.router.navigate(['/pick-tent'], { replaceUrl: true });

                return;
              }
              this.checkOverbooked$.next(false);
              // this.submit();
            }
          }
          if (sub3) {
            sub3.unsubscribe();
          }
        });
      }
    );

    this.subList?.push(sub2);
  }

  destroyEventNew() {
    if (this.subList && this.subList.length > 0) {
      this.subList.forEach((sub) => {
        sub.unsubscribe();
      });
    }
  }

  initFormNew() {
    this.getTentsRange();
    this.order.tents = this.tentsRange[0];

    const timeout = setTimeout(() => {
      this.order.tents = this.tentsRange[0];

      this.order.nights = this.orderService.getBookedNights();
      if (timeout) {
        clearTimeout(timeout);
      }
    }, 100);

    this.form = this.formBuilder.group({
      name: [
        //Name
        '',
        [Validators.required],
      ],
      phone: [
        //Phone
        '',
        [Validators.required, Validators.pattern('^[0-9]*$')],
      ],
      email: [
        //Email
        '',
        [Validators.required, Validators.email],
      ],
      nights: [
        //Nights
        '',
        [Validators.required],
      ],
      tents: [
        //Tents
        '',
        [Validators.required],
      ],
      // five_digits: [ //5 Digits
      //   '',
      //   [
      //     Validators.required,
      //     Validators.pattern("^((\\+91-?)|0)?[0-9]{5}$")
      //   ]
      // ],
    });

    const sub = this.form.valueChanges.subscribe(() => {
      const { dirty, pristine, valid, errors, invalid, value } = this.form!;
      this.status = JSON.stringify({
        dirty,
        pristine,
        valid,
        errors,
        invalid,
        value,
      });
    });

    if (this.subList) {
      this.subList.push(sub);
    }
  }

  public changeImg(imgUrl: string) {
    this.currentGalleryImg = imgUrl;
  }

  public countLeftTents(area: string, dateStartTS: number, nights: number) {
    const arr = Array.from(
      { length: nights },
      (_, index) => dateStartTS + index * this.dayTS
    ).map((ts: number) => {
      const leftTents =
        Number(getAreaTentsMax(area)) -
        countBy(this.orderService.eachDateTents[area], ts);
      if (leftTents === 0) {
        return 0;
      } else {
        return leftTents || getAreaTentsMax(area);
      }
    });
    return min(arr);
  }

  chooseTents(value: string) {
    this.order.tents = value;
    // const totalPeople = (+this.order.adults) + (+this.order.children)
    // this.addBreakfastRange = Array.from({length: totalPeople + 1}, (v, i) => i)

    // if(this.order.addBreakfast > totalPeople) {
    //   this.order.addBreakfast = 0
    // }
  }

  getStayOverDays(startTS: number, type: string) {
    let stayOverHolidaysTS: any[] = [];
    this.stayOverDays = [];

    for (let i = 0; i < this.order.nights; i++) {
      const ts = startTS + this.dayTS * i;
      this.stayOverDays.push(new Date(ts).getDay());
      stayOverHolidaysTS.push(ts);
    }

    const holiday_2024 = flatten(getOfficialHolidays().year_2024);
    const holiday_2025 = flatten(getOfficialHolidays().year_2025);
    const all_holidays = holiday_2024.concat(holiday_2025);
    const holidayTS = all_holidays.map((ts) => {
      return new Date(ts).getTime();
    });

    const holidays = intersection(stayOverHolidaysTS, holidayTS).filter(
      (ts: any) => {
        return (
          new Date(ts).getDay() !== 0 &&
          new Date(ts).getDay() !== 5 &&
          new Date(ts).getDay() !== 6
        );
      }
    );

    const fridays = countBy(this.stayOverDays, 5) || 0;
    const saturdays = countBy(this.stayOverDays, 6) || 0;
    const sundays = countBy(this.stayOverDays, 0) || 0;

    const weekendDays = fridays + saturdays + sundays + holidays.length;
    const normalDays = this.stayOverDays.length - weekendDays;

    if (type === 'normal') {
      return normalDays;
    }

    return weekendDays;
  }

  // 檢查是否30天以上，是的話為包場模式
  isPrivateRoom() {
    const curDT = new Date().toLocaleDateString();
    const curTS = new Date(curDT).getTime();
    return (
      curTS + this.timezoneOffsetAndDayLightTime(curTS) + this.dayTS * Common.IS_PRIVATE_ROOM_LIMIT - this.dateCheckInTS + this.timezoneOffsetAndDayLightTime(curTS) < 0
    );
  }

  getTentsRange(): number[] {
    if (this.isPrivateRoom()) {
      // 包場
      if (this.orderService.currentArea === 'inn') {
        return (this.tentsRange = this.genArray(
          this.orderService.maxTents[this.orderService.currentArea],
          this.orderService.maxTents[this.orderService.currentArea]
        ));
      }
      // 因為先前訂單需散帳預訂，因此要算出正確可預約的剩餘帳數
      return (this.tentsRange = this.genArray(
        this.countLeftTents(
          this.orderService.currentArea,
          this.dateCheckInTS,
          this.orderService.bookedNights
        ),
        this.countLeftTents(
          this.orderService.currentArea,
          this.dateCheckInTS,
          this.orderService.bookedNights
        )
      ));

      // 下面這行開放是可以在限包場時選擇散帳數量預訂(只能本機操作，不能上版喔) 先block起來
      // return this.tentsRange = this.genArray(1, this.orderService.maxTentsAmount);
    } else {
      // 散帳
      return (this.tentsRange = this.genArray(
        1,
        this.orderService.maxTentsAmount
      ));
    }
  }

  public genArray(min: number, max: number): number[] {
    return Array.from({ length: max - min + 1 }, (v, k) => k + min);
  }
  // genArray(arg0: number, arg1: number): number[] {
  //   throw new Error('Method not implemented.');
  // }

  protected getTentPerDay() {
    const dateRange: number[] = [];
    const tents_per_day: number[] = [];
    for (let i = 0; i < this.orderService.bookedNights; i++) {
      const ts = this.dateCheckInTS + this.dayTS * i;
      dateRange.push(ts);
    }

    this.dateRange = dateRange;

    dateRange.forEach((ts) => {
      for (let i = 0; i < Number(this.order.tents); i++) {
        tents_per_day.push(ts);
      }
    });
    return tents_per_day;
  }

  beforeSubmitCheck() {
    this.isLoading = true;
    if (-new Date().getTimezoneOffset() / 60 !== 8) {
      alert(
        '系統偵測您的ip位址異常，如需訂位請使用FB Messenger聯繫協助訂位，謝謝'
      );

      this.router.navigate(['/pick-tent'], { replaceUrl: true });
      return;
    }
    this.getTentPerDay();
    this.tentPreDayAndTents = {
      dateRange: this.dateRange,
      tents: +this.order.tents,
    };
    this.orderService.getUniqBookedDate(false);
    this.submit();
  }

  protected sendOrder(order: any) {
    this.apiService.sendCampOrders(order).subscribe((res) => {
      // 以下是sendCampOrders內容
      if (!res) {
        alert('很抱歉，目前系統繁忙，請稍後再試。');
        this.form?.reset();
        this.router.navigate(['/home'], { replaceUrl: true });
        return;
      }

      if (res) {
        this.form?.reset();
        this.router.navigate(['/thanks'], { replaceUrl: true });
      }
      this.elementService.isNotScrollable = false;
    });
  }

  submit() {
    if (this.form?.invalid) {
      console.log('invaaaaaalid', this.form);
      return;
    }

    // 點擊下一步後禁用表單
    Object.keys(this.form!.controls).forEach((key) => {
      this.form?.controls[key].disable();
    });

    // --- 未啟用 recaptcah Start --- //
    // const formOrder: {[key: string]: string | any} = {};
    // const formValue = this.form?.getRawValue();
    // Object.entries(formValue).forEach(([key,value]) => {
    //   formOrder[this.formControlNameMapping[key]] = value;
    // })

    // const order = {
    //   [OrderTent.ORDER_ID]: `c_${String(new Date().getTime())}`,
    //   [OrderTent.TENT_AREA]: this.orderService.currentArea,
    //   [OrderTent.START_YEAR]: this.getYMD(this.orderService.dateStart, 0),
    //   [OrderTent.START_MONTH]: this.getYMD(this.orderService.dateStart, 1),
    //   [OrderTent.START_DAY]: this.getYMD(this.orderService.dateStart, 2),
    //   [OrderTent.END_YEAR]: this.getYMD(this.orderService.dateLeave, 0),
    //   [OrderTent.END_MONTH]: this.getYMD(this.orderService.dateLeave, 1),
    //   [OrderTent.END_DAY]: this.getYMD(this.orderService.dateLeave, 2),
    //   [OrderTent.TOTAL_FEE]: this.totalFee.toString(),
    //   [OrderTent.NIGHTS]: this.order.nights.toString(),
    //   [OrderTent.TENTS]: this.order.tents.toString(),
    //   [OrderTent.PREPAID_FEE]: this.prePaidFee.toString(), //計算5成訂金
    //   [OrderTent.REST_FEE]: this.restFee.toString(), //計算尾款
    //   [OrderTent.IS_PAID_PRE_FEE]: '否',
    //   [OrderTent.IS_PAID_REST_FEE]: '否',
    //   [OrderTent.ORDER_SOURCE]: "網站訂單"
    // }

    // const orderCombine = Object.assign(order, formOrder)

    // const modal = this.modService.open(ConfrimOrderComponent, {
    //   title: '露營區訂位確認',
    //   orderType: OrderType.TENT,
    //   order: orderCombine,
    //   paymentInfo: {
    //     tentsFee: this.tentsFee,
    //     // peoplesFee: this.peoplesFee,
    //     dayCheckIn: getCurrentDay(this.orderService.dateStart),
    //     dayCheckOut: getCurrentDay(this.orderService.dateLeave)
    //   }
    // });

    // this.elementService.isNotScrollable = true

    // modal.onSubmit.subscribe(() => {
    //   this.apiService.sendForm(OrderType.TENT, orderCombine).subscribe(res => {
    //     if(res) {
    //       this.form?.reset();
    //       this.router.navigate(['/thanks'], { replaceUrl: true })
    //     }
    //   });
    //   this.elementService.isNotScrollable = false
    // });

    // modal.onCancel.subscribe(() => {
    //   this.elementService.isNotScrollable = false
    // })
    // --- 未啟用 recaptcah End --- //

    //--- 啟用 recaptcha 範圍 Start --- //
    const sub = this.recaptchaV3Service
      .execute('sendCampOrders')
      .subscribe((token: string) => {
        const captcha = {
          token,
        };
        const subSheet = this.apiService
          .sendRecToken(captcha)
          .subscribe((res) => {
            if (res.success) {
              // 實際功能 start
              const formOrder: { [key: string]: string | any } = {};
              const formValue = this.form?.getRawValue();
              Object.entries(formValue).forEach(([key, value]) => {
                formOrder[this.formControlNameMapping[key]] = value;
              });

              // const order = {
              //   [OrderTent.ORDER_ID]: `c_${String(new Date().getTime())}`,
              //   [OrderTent.TENT_AREA]: this.orderService.currentArea,
              //   [OrderTent.START_YEAR]: this.getYMD(this.orderService.dateStart, 0),
              //   [OrderTent.START_MONTH]: this.getYMD(this.orderService.dateStart, 1),
              //   [OrderTent.START_DAY]: this.getYMD(this.orderService.dateStart, 2),
              //   [OrderTent.END_YEAR]: this.getYMD(this.orderService.dateLeave, 0),
              //   [OrderTent.END_MONTH]: this.getYMD(this.orderService.dateLeave, 1),
              //   [OrderTent.END_DAY]: this.getYMD(this.orderService.dateLeave, 2),
              //   [OrderTent.STAY_DATE_RANGE]: this.orderService.stayDateRange,
              //   [OrderTent.TOTAL_FEE]: this.totalFee.toString(),
              //   [OrderTent.NIGHTS]: this.order.nights.toString(),
              //   [OrderTent.TENTS]: this.order.tents.toString(),
              //   [OrderTent.PREPAID_FEE]: this.prePaidFee.toString(), //計算5成訂金
              //   [OrderTent.REST_FEE]: this.restFee.toString(), //計算尾款
              //   // [OrderTent.IS_PAID_PRE_FEE]: '否',
              //   // [OrderTent.IS_PAID_REST_FEE]: '否',
              //   [OrderTent.ORDER_SOURCE]: "網站訂單"
              // }

              // New Start
              const order = {
                order_id: `${this.orderService.currentArea}_${String(
                  new Date().getTime()
                )}`,
                area: this.orderService.currentArea,
                start_date: this.orderService.dateStart,
                timestamp_start_date: new Date(
                  this.orderService.dateStart
                ).getTime(),
                end_date: this.orderService.dateLeave,
                tents_per_day: this.getTentPerDay(),
                total_fee: this.totalFee().toString(),
                nights: this.orderService.getBookedNights()?.toString(),
                // [OrderTent.TENTS]: this.order.tents.toString(),
                prepaid_fee: this.prePaidFee().toString(), //計算5成訂金
                rest_fee: this.restFee().toString(), //計算尾款
                // [OrderTent.IS_PAID_PRE_FEE]: '否',
                // [OrderTent.IS_PAID_REST_FEE]: '否',
                tents: +this.order.tents,
                is_night_going: false,
                is_prepaid: false,
                order_source: '網站訂單',
                created_time: new Date().getTime(),
              };
              // New End

              const orderCombine = Object.assign(order, formOrder);

              const modal = this.modService.open(ConfirmBoxComponent, {
                title: '露營區訂位確認',
                orderType: this.orderService.currentArea,
                order: orderCombine,
                paymentInfo: {
                  tentsFee: this.tentsFee(),
                  // peoplesFee: this.peoplesFee,
                  dayCheckIn: getCurrentDay(this.orderService.dateStart),
                  dayCheckOut: getCurrentDay(this.orderService.dateLeave),
                },
              });

              this.isLoading = false;
              this.elementService.isNotScrollable = true;

              // check if date is already booked

              modal.onSubmit.subscribe(() => {
                this.isLoading = true;
                if (-new Date().getTimezoneOffset() / 60 !== 8) {
                  alert(
                    '系統偵測您的ip位址異常，如需訂位請使用FB Messenger聯繫協助訂位，謝謝'
                  );

                  this.router.navigate(['/pick-tent'], { replaceUrl: true });
                  return;
                }
                this.getTentPerDay();
                this.tentPreDayAndTents = {
                  dateRange: this.dateRange,
                  tents: +this.order.tents,
                };
                this.orderService.getUniqBookedDate(false);

                this.checkOverbooked$.subscribe((isBookedByOthers) => {
                  if (isBookedByOthers) {
                    this.elementService.isNotScrollable = false;
                    return;
                  }
                  this.sendOrder(order);
                });
              });

              modal.onCancel.subscribe(() => {
                this.elementService.isNotScrollable = false;

                // 點擊修改訂單解除表單禁用
                Object.keys(this.form!.controls).forEach((key) => {
                  this.form?.controls[key].enable();
                });
              });
              // 實際功能 end
            }
          });

        if (subSheet) {
          this.subList?.push(subSheet);
        }
      });

    if (sub) {
      this.subList?.push(sub);
    }
    //--- 啟用 recaptcha 範圍 End --- //
    return;
  }
}
