import { DirectivesModule } from './../../directives/directives.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderCampRoutingModule } from './order-camp-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecaptchaFormsModule, RecaptchaV3Module, RECAPTCHA_V3_SITE_KEY } from 'ng-recaptcha';
import { ShareModule } from 'src/app/module/share/share.module';
import { OrderCampComponent } from './order-camp.component';


@NgModule({
  declarations: [OrderCampComponent],
  imports: [
    DirectivesModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RecaptchaFormsModule,
    RecaptchaV3Module,
    ShareModule,
    OrderCampRoutingModule
  ],
  providers: [
    {
      provide: RECAPTCHA_V3_SITE_KEY,
      useValue: '6LdpNoEeAAAAAECLDpl_gXgapOlI_c-IcB5n0w4j',
    },
  ]
})
export class OrderCampModule { }
