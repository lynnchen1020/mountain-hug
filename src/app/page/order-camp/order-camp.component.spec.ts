import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderCampComponent } from './order-camp.component';

describe('OrderCampComponent', () => {
  let component: OrderCampComponent;
  let fixture: ComponentFixture<OrderCampComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderCampComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderCampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
