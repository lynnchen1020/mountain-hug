import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThanksRoutingModule } from './thanks-routing.module';
import { ThanksComponent } from './thanks.component';
import { ShareModule } from 'src/app/module/share/share.module';


@NgModule({
  declarations: [ThanksComponent],
  imports: [
    CommonModule,
    ShareModule,
    ThanksRoutingModule
  ]
})
export class ThanksModule { }
