import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { ConfirmBoxInnComponent } from 'src/app/components/modal/confirm-box-inn/confirm-box-inn.component';
import { OrderPreRatio } from 'src/app/enum/order-type';
import { getCurrentDay } from 'src/app/helper/helper';
import { ElementService } from 'src/app/service/element.service';
import { ModalNewService } from 'src/app/service/modal-new.service';
import { OrderService } from 'src/app/service/order.service';
import { OrderCampComponent } from '../order-camp/order-camp.component';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-order-inn',
  templateUrl: './order-inn.component.html',
  styleUrls: ['./order-inn.component.sass']
})
export class OrderInnComponent extends OrderCampComponent implements OnInit, OnDestroy {
  peopleRange: number[] = [1, 2, 3, 4, 5, 6]
  adultsRange: number[] = [1, 2, 3, 4, 5, 6]
  addBreakfastRange: number[] = Array.from({length: 2}, (v, i) => i)
  childrenRange: number[] = Array.from({length: 6}, (v, i) => i)
  addBedRange: number[] = Array.from({length: 3}, (v, i) => i)
  override checkOverbooked$ = new Subject();

  constructor(
    router: Router,
    apiService: ApiService,
    route: ActivatedRoute,
    modService: ModalNewService,
    recaptchaV3Service: ReCaptchaV3Service,
    formBuilder: FormBuilder,
    cdref: ChangeDetectorRef,
    elementService: ElementService,
    orderService: OrderService) {
    super(router, apiService, route, modService, recaptchaV3Service, formBuilder, cdref, elementService, orderService);

    this.order = {
      name: '',
      phone: '',
      email: '',
      startDate: '',
      endDate: '',
      nights: 1,
      adults: this.adultsRange[0],
      children: this.childrenRange[0],
      tents: 1,
      prePaidFee: 0,
      isPrePaidFee: false,
      restFee: 0,
      totalFee: 0,
      isPaidTotal: false,
      addBreakfast: this.addBreakfastRange[0],
      add_bed: this.addBedRange[0]
    }

    this.formControlNameMapping = {
      name: 'name',
      phone: 'phone',
      email: 'email',
      people: 'people',
      nights: 'nights',
      adults: 'adults',
      children: 'children',
      add_bed: 'add_bed'
    };

    this.currentGalleryImg = "url('/assets/img/homestay/img_1.jpg')";
    this.gallery = [
      "url('/assets/img/homestay/img_1.jpg')",
      "url('/assets/img/homestay/img_2.jpg')",
      "url('/assets/img/homestay/img_3.jpg')",
      "url('/assets/img/homestay/img_4.jpg')",
      "url('/assets/img/homestay/img_5.jpg')",
      "url('/assets/img/homestay/img_6.jpg')",
      "url('/assets/img/homestay/img_7.jpg')",
      "url('/assets/img/homestay/img_8.jpg')",
      "url('/assets/img/homestay/img_9.jpg')",
      "url('/assets/img/homestay/img_10.jpg')",
      "url('/assets/img/homestay/img_11.jpg')",
      "url('/assets/img/homestay/img_12.jpg')",
      "url('/assets/img/homestay/img_13.jpg')",
    ];
  }

  override totalFee() {
    // 陣列裡有 5 6 就算假日
    const normalDaysFee = 4500 * this.getStayOverDays(new Date(this.orderService.dateStart).getTime(), 'normal')
    const weekendDaysFee = 5800 * this.getStayOverDays(new Date(this.orderService.dateStart).getTime(), 'weekend')
    return (weekendDaysFee + normalDaysFee + this.breakfastFee + this.bedFee()) || 0;

    // return 3000
  }

  nightsTotalFee() {
    // 陣列裡有 5 6 0 就算假日
    const normalDaysFee = 4500 * this.getStayOverDays(new Date(this.orderService.dateStart).getTime(), 'normal')
    const weekendDaysFee = 5800 * this.getStayOverDays(new Date(this.orderService.dateStart).getTime(), 'weekend')

    return weekendDaysFee + normalDaysFee
  }

  override prePaidFee() {
    return (this.totalFee() * OrderPreRatio.INN) || 0;
  }

  bedFee() {
    const fee = this.order.add_bed !== 0 ? this.order.add_bed * 500 : 0
    return fee;
  }

  get breakfastFee() {
    const fee = this.order.addBreakfast !== 0 ?
    +this.order.addBreakfast * this.order.nights * 150 : 0;

    return fee || 0;
  }

  override ngOnInit(): void {
    // super.ngOnInit();
    if(this.orderService.dateStart === '' || this.orderService.dateLeave === '') {
      alert('你尋找的網頁使用了你輸入的資料。返回該頁會重複你剛才的行動，請返回重新選擇入住日期')
      this.router.navigate(['/pick-tent'], { replaceUrl: true })
    }
    super.bindEventNew();
    this.initFormNew();
  }

  override ngOnDestroy(): void {
    this.destroyEventNew();
  }


  override initFormNew() {
    this.getTentsRange();
    const timeout = setTimeout(() => {
      this.order.tents = this.tentsRange[0];
      this.chooseAdults('1')
      this.chooseChildren('0')
      this.order.add_bed = 0
      this.order.nights = this.orderService.getBookedNights();
      if(timeout) {
        clearTimeout(timeout)
      }
    }, 50)

    // this.order.people = this.getPeopleRange()[0];

    this.form = this.formBuilder.group(
      {
        name: [ //Name
          '',
          [
            Validators.required,
            // Validators.minLength(3),
            // Validators.maxLength(5)
          ]
        ],
        phone: [ //Phone
          '',
          [
            Validators.required,
            Validators.pattern("^[0-9]*$"),
          ]
        ],
        email: [ //Email
          '',
          [
            Validators.required,
            Validators.email
          ]
        ],
        adults: [
          '',
          [
            Validators.required,
          ]
        ],
        children: [
          '',
          [
            Validators.required,
          ]
        ],
        add_bed: [
          '',
          [
            Validators.required,
          ]
        ],
        // addBreakfast: [
        //   '',
        //   [
        //     Validators.required,
        //   ]
        // ],
        request: [ // Request
          '',
        ],
        // five_digits: [ //5 Digits
        //   '',
        //   [
        //     Validators.required,
        //     Validators.pattern("^((\\+91-?)|0)?[0-9]{5}$")
        //   ]
        // ],
      }
    )

    const sub = this.form.valueChanges.subscribe(
      () => {
       const { dirty, pristine, valid, errors, invalid, value } = this.form!;
       this.status = JSON.stringify({ dirty, pristine, valid, errors, invalid, value })
      }
    )

    if(this.subList) {
      this.subList.push(sub);
    }
  }

  chooseAdults(value: string) {
    this.order.adults = value;
    switch (value) {
      case "1":
        this.childrenRange = Array.from({length: 6}, (v, i) => i)
        break;
      case "2":
        this.childrenRange = Array.from({length: 5}, (v, i) => i)
        break;
      case "3":
        this.childrenRange = Array.from({length: 4}, (v, i) => i)
        break;
      case '4':
        this.childrenRange = Array.from({length: 3}, (v, i) => i)
        break;
      case '5':
        this.childrenRange = Array.from({length: 2}, (v, i) => i)
        break;
      case '6':
        this.childrenRange = Array.from({length: 1}, (v, i) => i)
        break;
      default: []
    }

    const totalPeople = (+this.order.adults) + (+this.order.children)

    this.addBreakfastRange = Array.from({length: totalPeople + 1}, (v, i) => i)

    if((+this.order.adults + +this.order.children) > 6) {
      this.order.children = "0"
      this.addBreakfastRange = Array.from({length: (+this.order.adults) + 1}, (v, i) => i)
    }

    if(this.order.addBreakfast > totalPeople) {
      this.order.addBreakfast = 0
    }
  }

  chooseChildren(value: string) {
    this.order.children = value;
    const totalPeople = (+this.order.adults) + (+this.order.children)
    this.addBreakfastRange = Array.from({length: totalPeople + 1}, (v, i) => i)

    if(this.order.addBreakfast > totalPeople) {
      this.order.addBreakfast = 0
    }
  }

  override sendOrder(order: any) {
    this.apiService.sendCampOrders(order).subscribe(res => {
      // 以下是sendCampOrders內容
      if(!res) {
        alert('很抱歉，目前系統繁忙，請稍後再試。')
        this.form?.reset();
        this.router.navigate(['/home'], { replaceUrl: true })
        return
      }

      if(res) {
        this.form?.reset();
        this.router.navigate(['/thanks'], { replaceUrl: true })
      }
      this.elementService.isNotScrollable = false
    })
  }

  override submit() {
    if (this.form?.invalid) {
      console.log('invaaaaaalid', this.form)
      return;
    }

    // 點擊下一步後禁用表單
    Object.keys(this.form!.controls).forEach(key => {
      this.form?.controls[key].disable();
    })

    // --- 未啟用 recaptcah Start --- //
    // const formOrder: {[key: string]: string | any} = {};
    // const formValue = this.form?.getRawValue();
    // Object.entries(formValue).forEach(([key,value]) => {
    //   formOrder[this.formControlNameMapping[key]] = value;
    // })

    // const order = {
    //   [OrderTent.ORDER_ID]: `c_${String(new Date().getTime())}`,
    //   [OrderTent.TENT_AREA]: this.area,
    //   [OrderTent.START_YEAR]: this.getYMD(this.orderService.dateStart, 0),
    //   [OrderTent.START_MONTH]: this.getYMD(this.orderService.dateStart, 1),
    //   [OrderTent.START_DAY]: this.getYMD(this.orderService.dateStart, 2),
    //   [OrderTent.END_YEAR]: this.getYMD(this.orderService.dateLeave, 0),
    //   [OrderTent.END_MONTH]: this.getYMD(this.orderService.dateLeave, 1),
    //   [OrderTent.END_DAY]: this.getYMD(this.orderService.dateLeave, 2),
    //   [OrderTent.TOTAL_FEE]: this.totalFee.toString(),
    //   [OrderTent.NIGHTS]: this.order.nights.toString(),
    //   [OrderTent.TENTS]: this.order.tents.toString(),
    //   [OrderTent.PREPAID_FEE]: this.prePaidFee.toString(), //計算5成訂金
    //   [OrderTent.REST_FEE]: this.restFee.toString(), //計算尾款
    //   [OrderTent.IS_PAID_PRE_FEE]: '否',
    //   [OrderTent.IS_PAID_REST_FEE]: '否',
    //   [OrderTent.ORDER_SOURCE]: "網站訂單"
    // }

    // const orderCombine = Object.assign(order, formOrder)

    // const modal = this.modService.open(ConfrimOrderComponent, {
    //   title: '露營區訂位確認',
    //   orderType: OrderType.TENT,
    //   order: orderCombine,
    //   paymentInfo: {
    //     tentsFee: this.tentsFee,
    //     // peoplesFee: this.peoplesFee,
    //     dayCheckIn: getCurrentDay(this.orderService.dateStart),
    //     dayCheckOut: getCurrentDay(this.orderService.dateLeave)
    //   }
    // });

    // this.elementService.isNotScrollable = true

    // modal.onSubmit.subscribe(() => {
    //   this.apiService.sendForm(OrderType.TENT, orderCombine).subscribe(res => {
    //     if(res) {
    //       this.form?.reset();
    //       this.router.navigate(['/thanks'], { replaceUrl: true })
    //     }
    //   });
    //   this.elementService.isNotScrollable = false
    // });

    // modal.onCancel.subscribe(() => {
    //   this.elementService.isNotScrollable = false
    // })
    // --- 未啟用 recaptcah End --- //


    //--- 啟用 recaptcha 範圍 Start --- //
    const sub = this.recaptchaV3Service.execute('sendCampOrders').subscribe((token: string) => {
      const captcha = {
        token
      }
      const subSheet = this.apiService.sendRecToken(captcha).subscribe(res => {
        if(res.success) {
          // 實際功能 start
          const formOrder: {[key: string]: string | any} = {};
          const formValue = this.form?.getRawValue();
          Object.entries(formValue).forEach(([key,value]) => {
            formOrder[this.formControlNameMapping[key]] = value;
          })

          // const order = {
          //   [OrderTent.ORDER_ID]: `c_${String(new Date().getTime())}`,
          //   [OrderTent.TENT_AREA]: this.area,
          //   [OrderTent.START_YEAR]: this.getYMD(this.orderService.dateStart, 0),
          //   [OrderTent.START_MONTH]: this.getYMD(this.orderService.dateStart, 1),
          //   [OrderTent.START_DAY]: this.getYMD(this.orderService.dateStart, 2),
          //   [OrderTent.END_YEAR]: this.getYMD(this.orderService.dateLeave, 0),
          //   [OrderTent.END_MONTH]: this.getYMD(this.orderService.dateLeave, 1),
          //   [OrderTent.END_DAY]: this.getYMD(this.orderService.dateLeave, 2),
          //   [OrderTent.STAY_DATE_RANGE]: this.orderService.stayDateRange,
          //   [OrderTent.TOTAL_FEE]: this.totalFee.toString(),
          //   [OrderTent.NIGHTS]: this.order.nights.toString(),
          //   [OrderTent.TENTS]: this.order.tents.toString(),
          //   [OrderTent.PREPAID_FEE]: this.prePaidFee.toString(), //計算5成訂金
          //   [OrderTent.REST_FEE]: this.restFee.toString(), //計算尾款
          //   // [OrderTent.IS_PAID_PRE_FEE]: '否',
          //   // [OrderTent.IS_PAID_REST_FEE]: '否',
          //   [OrderTent.ORDER_SOURCE]: "網站訂單"
          // }
          // New Start
          const order = {
            order_id: `${this.orderService.currentArea}_${String(new Date().getTime())}`,
            area: this.orderService.currentArea,
            start_date: this.orderService.dateStart,
            end_date: this.orderService.dateLeave,
            tents_per_day: this.getTentPerDay(),
            total_fee: this.totalFee().toString(),
            nights: this.orderService.getBookedNights()?.toString(),
            // [OrderTent.TENTS]: this.order.tents.toString(),
            prepaid_fee: this.prePaidFee().toString(), //計算5成訂金
            rest_fee: this.restFee().toString(), //計算尾款
            // [OrderTent.IS_PAID_PRE_FEE]: '否',
            // [OrderTent.IS_PAID_REST_FEE]: '否',
            tents: 1,
            is_night_going: false,
            is_prepaid: false,
            order_source: "網站訂單",
            created_time: new Date().getTime()
          }
          // New End

          const orderCombine = Object.assign(order, formOrder)

          const modal = this.modService.open(ConfirmBoxInnComponent, {
            title: '宿屋訂位確認',
            orderType: this.orderService.currentArea,
            order: orderCombine,
            paymentInfo: {
              // tentsFee: this.tentsFee,
              // peoplesFee: this.peoplesFee,
              dayCheckIn: getCurrentDay(this.orderService.dateStart),
              dayCheckOut: getCurrentDay(this.orderService.dateLeave),
              addBedFee: this.bedFee(),
              addBreakfastFee: this.breakfastFee,
              nightsTotalFee: this.nightsTotalFee()
            }
          });

          this.isLoading = false;
          this.elementService.isNotScrollable = true

          modal.onSubmit.subscribe(() => {
            this.isLoading = true;
            if((-(new Date().getTimezoneOffset()) / 60) !== 8) {
              alert("系統偵測您的ip位址異常，如需訂位請使用FB Messenger聯繫協助訂位，謝謝")

              this.router.navigate(['/pick-tent'], { replaceUrl: true })
              return;
            }
            this.getTentPerDay();
            this.tentPreDayAndTents = {
              dateRange: this.dateRange,
              tents: +this.order.tents,
            }
            this.orderService.getUniqBookedDate(false)

            this.checkOverbooked$.subscribe(isBookedByOthers => {
              if(isBookedByOthers) {
                this.elementService.isNotScrollable = false
                return;
              }
              this.sendOrder(order)
            })
          });

          modal.onCancel.subscribe(() => {
            this.elementService.isNotScrollable = false

            // 點擊修改訂單解除表單禁用
            Object.keys(this.form!.controls).forEach(key => {
              this.form?.controls[key].enable();
            })
          })
          // 實際功能 end
        }
      })

      if(subSheet) {
        this.subList?.push(subSheet);
      }
    });

    if(sub) {
      this.subList?.push(sub);
    }
    //--- 啟用 recaptcha 範圍 End --- //
    return ;
  }

}
