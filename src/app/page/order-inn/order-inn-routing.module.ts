import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderInnComponent } from './order-inn.component';

const routes: Routes = [
  {
    path: '',
    component: OrderInnComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderInnRoutingModule { }
