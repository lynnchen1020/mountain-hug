import { NgModule, Directive } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderInnRoutingModule } from './order-inn-routing.module';
import { RecaptchaFormsModule, RecaptchaV3Module, RECAPTCHA_V3_SITE_KEY } from 'ng-recaptcha';
import { OrderInnComponent } from './order-inn.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShareModule } from 'src/app/module/share/share.module';
import { DirectivesModule } from 'src/app/directives/directives.module';


@NgModule({
  declarations: [OrderInnComponent],
  imports: [
    DirectivesModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RecaptchaFormsModule,
    RecaptchaV3Module,
    ShareModule,
    OrderInnRoutingModule
  ],
  providers: [
    {
      provide: RECAPTCHA_V3_SITE_KEY,
      useValue: '6LdpNoEeAAAAAECLDpl_gXgapOlI_c-IcB5n0w4j',
    },
  ]
})
export class OrderInnModule { }
