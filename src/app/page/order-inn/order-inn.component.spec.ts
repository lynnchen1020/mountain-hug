import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderInnComponent } from './order-inn.component';

describe('OrderInnComponent', () => {
  let component: OrderInnComponent;
  let fixture: ComponentFixture<OrderInnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderInnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderInnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
