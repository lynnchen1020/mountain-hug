import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PickTentRoutingModule } from './pick-tent-routing.module';
import { PickTentComponent } from './pick-tent.component';
import { ShareModule } from 'src/app/module/share/share.module';


@NgModule({
  declarations: [PickTentComponent],
  imports: [
    CommonModule,
    ShareModule,
    PickTentRoutingModule
  ]
})
export class PickTentModule { }
