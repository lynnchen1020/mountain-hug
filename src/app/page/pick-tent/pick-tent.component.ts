
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { OrderType } from 'src/app/enum/order-type';
import { getCurrentDay } from 'src/app/helper/helper';
import { IDate } from 'src/app/models/IDateTime';
import { ApiService } from 'src/app/service/api.service';
import { ElementService } from 'src/app/service/element.service';
import { OrderService } from 'src/app/service/order.service';

@Component({
  selector: 'app-pick-tent',
  templateUrl: './pick-tent.component.html',
  styleUrls: ['./pick-tent.component.sass']
})
export class PickTentComponent implements OnInit {
  orderListTemp: any[] = [];
  orderList: any[] = [];
  orderListTent: any[] = [];
  stayDateRange: any[] = [];
  dateMin = '';
  dateMax = '';
  dateStart = '';
  dateEnd = '';
  nights = 1;
  isWeekend = false;
  hasTue = false;
  isLoading = true;
  isToggleNights = false;
  currentNightsIdx = 0;
  resData: any;
  isEmpty = true;
  OrderType = OrderType;
  isShowPicker = false;
  tempDate = {};

  bookedNightsData: {
    [key: string]: string[]
  } = {}
  isBookedAvailable: {
    [key: string]: boolean
  } = {}
  bookedNightsTS: {
    [key: string]: number[]
  } = {}

  bookedStatus: {
    [key: string]: number[]
  } = {}

  nightsMapping = [
    {
      'title': '2天1夜',
      'nights': 1,
    },
    {
      'title': '3天2夜',
      'nights': 2,
    },
    {
      'title': '4天3夜',
      'nights': 3,
    },
  ]

  dateLeave = '';
  bookedDate: string[] = [];
  dayTS = 86400000;

  subList: Subscription[] = [];

  constructor(
    public elementService: ElementService,
    private router: Router,
    public orderService: OrderService) {
      this.getDateString();
  }

  get dayStart() {
    return getCurrentDay(this.dateStart);
  }

  get dayLeave() {
    return getCurrentDay(this.dateLeave);
  }

  ngOnInit(): void {
    this.orderService.getTentAndTentsHomestayBookedReservation(true);
    this.orderService.isInitData$.subscribe((data: any) => {
      if(data) {
        this.isShowPicker = true;
      }
    })
    // this.getTentAndTentsHomestayBookedReservation(true)
    this.isEmpty = false;
  }


  // date related
  getDateString() {
    const start = new Date();
    const end = new Date();

    start.setDate((start.getDate()));

    const USStartDate = start.setHours((start.getHours() + 12));
    // const USEndDate = end.setHours((end.getHours() - 12)) + dayTS * 90; // 開放 90 days

    const USEndDate = end.setMonth((end.getMonth() + 6)) // 開放6個月
    const USEndDateTS = +this.getLastDateInThisMonth(new Date(USEndDate).toLocaleDateString())


    this.dateMin = this.formatDate(USStartDate);
    this.dateMax = this.formatDate(USEndDateTS);
    // this.dateStart = this.dateMin;
    this.dateEnd = this.dateMax;

    // const leaveTS = new Date(this.dateStart).getTime() + this.dayTS
    // this.dateLeave = new Date(leaveTS).toLocaleDateString();
  }

  private getLastDateInThisMonth(date: string) {
    const separateDay = date.split('/');
    const newDate = `${separateDay[0]}/${separateDay[1]}/20`;
    const time = new Date(newDate);
    time.setMonth(time.getMonth() + 1);
    time.setDate(0);
    return time;
  }

  formatDate(usDate: number): string {
    const newTime: Date = new Date(usDate);
    // Date 轉成 YYYY-MM-DD
    return `${newTime.getFullYear()}/${(newTime.getMonth() + 1).toString()}/${newTime.getDate().toString()}`;
  }

  // date related ednd
  togglePicker($event: boolean) {
    this.isShowPicker = $event;
  }

  // private isBookedDate(bookedNightsData: any) {
  //   return (map(bookedNightsData, this.dateStrIntoArray).map((stayRange: any) => {
  //     return intersection(this.stayDateRange, stayRange).length > 0;
  //   })).includes(true)
  // }

  // private dateToTSArr(dateStrArr: string[]): number[] {
  //   const splitArr = dateStrArr.map(str => {
  //     return str.split(',')
  //   })

  //   const dateTSrArr = flatten(splitArr).map((item: any) => {
  //     return new Date(item).getTime()
  //   })

  //   return dateTSrArr
  // }

  // Result
  // getTentAndTentsHomestayBookedReservation(isInit: boolean) {
  //   this.resData = '';
  //   const sub = zip(
  //     this.getTentsHomestayReservationData(),
  //     this.getHomestayReservationData(),
  //     this.getTentReservationData('A'),
  //     this.getTentReservationData('A1'),
  //     this.getTentReservationData('A2')
  //     )
  //     .pipe(
  //       tap(x => x.concat(x))
  //     ).subscribe((data: any[]) => {

  //       if(data && isInit) {
  //         this.isLoading = false;
  //         this.bookedNightsData = {
  //           "tent_homestay": data[0],
  //           "homestay": data[1],
  //           "tent_A": data[2],
  //           "tent_A1": data[3],
  //           "tent_A2": data[4],
  //         }

  //         console.log('所有入住日期', this.bookedNightsData)

  //         this.bookedNightsTS = {
  //           "tent_homestay": this.dateToTSArr(this.bookedNightsData['tent_homestay']),
  //           "homestay": this.dateToTSArr(this.bookedNightsData['homestay']),
  //           "tent_A": this.dateToTSArr(this.bookedNightsData['tent_A']),
  //           "tent_A1": this.dateToTSArr(this.bookedNightsData['tent_A1']),
  //           "tent_A2": this.dateToTSArr(this.bookedNightsData['tent_A2'])
  //         }

  //         // console.log('bookedNightsTS', this.bookedNightsTS)
  //         return
  //       }

  //       if(data) {
  //         this.resData = data;
  //         this.isLoading = false;
  //         this.bookedNightsData = {
  //           "tent_homestay": data[0],
  //           "homestay": data[1],
  //           "tent_A": data[2],
  //           "tent_A1": data[3],
  //           "tent_A2": data[4],
  //         }

  //         this.bookedStatus = {
  //           'tent_homestay': this.isBookedDate(this.bookedNightsData['tent_homestay']),
  //           'homestay': this.isBookedDate(this.bookedNightsData['homestay']),
  //           'tent_A': this.isBookedDate(this.bookedNightsData['tent_A']),
  //           'tent_A1': this.isBookedDate(this.bookedNightsData['tent_A1']),
  //           'tent_A2': this.isBookedDate(this.bookedNightsData['tent_A2']),
  //         }

  //         this.isBookedAvailable = {
  //           'tent_homestay': !this.bookedStatus['tent_homestay'] && (!this.bookedStatus['homestay'] && !this.bookedStatus['tent_A']) && (!this.bookedStatus['tent_A1'] && !this.bookedStatus['tent_A2']),
  //           'homestay': !this.bookedStatus['homestay'] && !this.bookedStatus['tent_homestay'],
  //           'tent_A': !this.bookedStatus['tent_A'] && !this.bookedStatus['tent_homestay'] && (!this.bookedStatus['tent_A1'] && !this.bookedStatus['tent_A2']),
  //           'tent_A1': !this.bookedStatus['tent_A1'] && !this.bookedStatus['tent_homestay'] && !this.bookedStatus['tent_A'] && !this.isWeekend,
  //           'tent_A2': !this.bookedStatus['tent_A2'] && !this.bookedStatus['tent_homestay'] && !this.bookedStatus['tent_A'] && !this.isWeekend,
  //         }

  //         // console.log('已預約狀況', this.bookedStatus)

  //         this.isEmpty = !(this.isBookedAvailable['tent_homestay'] && !this.hasTuesday()) &&
  //         !(this.isBookedAvailable['homestay'] && !this.hasTuesday()) &&
  //         !(this.isBookedAvailable['tent_A'] && !this.hasTuesday()) &&
  //         !(this.isBookedAvailable['tent_A1'] && !this.hasTuesday()) &&
  //         !(this.isBookedAvailable['tent_A2'] && !this.hasTuesday())
  //         // 過夜日期是否被預訂
  //         // const isBooked = tentHomeStayRange.map((stayRange: any) => {
  //         //   return intersection(this.stayDateRange, stayRange).length > 0;
  //         // })

  //         // if(isBooked.includes(true)) {
  //         //   this.bookedStatus.tent_homestay = true
  //         //   this.bookedStatus.homestay = false
  //         //   this.bookedStatus.tent_A = false
  //         // } else {
  //         //   this.bookedStatus.tent_homestay = false
  //         //   this.bookedStatus.homestay = false
  //         //   this.bookedStatus.tent_A = false
  //         // }

  //         // console.log("bookedStatus", this.bookedStatus)
  //       }

  //       if(sub) {
  //         sub.unsubscribe()
  //       }
  //     })
  // }

  // new flow func start
  changeStartDate($event: string) {
    this.dateStart = $event
    // const leaveTS = new Date(this.dateStart).getTime() + this.dayTS * this.nightsMapping[this.currentNightsIdx].nights
    // this.dateLeave = new Date(leaveTS).toLocaleDateString();

    // console.log('dateLeave', this.dateLeave)

    // this.getDateString()
    // console.log('start:', this.dateStart, 'end:',  this.dateLeave)
  }
  // changeLeaveDate($event: string) {
  //   this.dateLeave = $event
  //   // this.getDateString()
  //   console.log('start:', this.dateStart, 'end:',  this.dateLeave)
  // }
  changeDateToSlashFormat(dateStr: string) {
    return dateStr.split("-").join('/');
  }

  public pickNights(idx: number, currentIdx: number) {
    this.nights = idx;
    this.currentNightsIdx = currentIdx;
    this.isToggleNights = false;
  }

  // public hasTuesday(): boolean {
  //   const hasTue = this.stayDateRange.map(date => {
  //     return new Date(date).getDay() === 2;
  //   }).includes(true)
  //   const isTue = new Date(this.dateLeave).getDay() === 2;

  //   return isTue || hasTue;
  // }


  public search() {

    this.isLoading = true;
    this.isToggleNights = false;
    const startTS = new Date(this.changeDateToSlashFormat(this.dateStart)).getTime();
    const leaveTS = startTS + this.dayTS * this.nightsMapping[this.currentNightsIdx].nights
    this.dateLeave = new Date(leaveTS).toLocaleDateString();

    if(leaveTS <= startTS) {
      // console.log('離開日期不得小於入住日期')
      return
    }

    this.tempDate = {
      dateStart: this.dateStart,
      dayStart: this.dayStart,
      dateLeave: this.dateLeave,
      dayLeave: this.dayLeave,
      nightsTitle: this.nightsMapping[this.currentNightsIdx].title
    }

    // console.log('送出 start:', this.dateStart, 'end:',  this.dateLeave)

    let stayDateRange: any[] = []
    // const nights = (leaveTS - startTS) / dayTS
    let addedNightTS = 0
    for(let i = 0; i < this.nights; i++) {
      addedNightTS = startTS + this.dayTS * i
      stayDateRange.push(addedNightTS)
    }

    this.stayDateRange = stayDateRange.map(date => new Date(date).toLocaleDateString())


    // // 判斷客人是否選取含有週末的日期區間
    // const isWeekend = stayDateRange.map(ts => {
    //   return new Date(ts).getDay() === 5 || new Date(ts).getDay() === 6;
    // })
    // this.isWeekend = isWeekend.includes(true);

    // // console.log('this.stayDateRange', this.stayDateRange)

    // // 判斷客人是否有選取到星期二公休日
    // const hasTue = stayDateRange.map(date => {
    //   return new Date(date).getDay() === 2;
    // })
    // const isTue = new Date(this.dateLeave).getDay() === 2;

    // this.hasTue = hasTue.includes(true) || isTue;

    this.orderService.dateStart = this.dateStart
    this.orderService.dateLeave = this.dateLeave
    this.orderService.getTentAndTentsHomestayBookedReservation(false)

    // API 需傳 入住日期字串 array 上去
    // const stayNightRangeStrings = stayDateRange.map(date => new Date(date).toLocaleDateString()).toString();

    // console.log(stayNightRangeStrings)
  }

  private dateStrIntoArray(dateStr: string) {
    return dateStr.split(',');
  }
  // new flow func end


  // // get sheet data 大包場
  // getTentsHomestayReservationData() {
  //   return new Observable(observer => {
  //     const orderType = OrderType.TENT_HOMESTAY;
  //     const sub = this.apiService.getCookerFilter(orderType).subscribe(res => {

  //       const orderKey: string[] = [
  //         'timestampUID',
  //         'order_id',
  //         'name',
  //         'phone',
  //         'email',
  //         'startDate',
  //         'endDate',
  //         'stayDateRange',
  //         'nights',
  //         'add_bed',
  //         'five_digits',
  //         'prepaid_fee',
  //         'is_paid_pre_fee',
  //         'rest_fee',
  //         'is_paid_rest_fee',
  //         'total_fee',
  //         'order_source',
  //         'is_cancel_order'
  //       ]

  //       if(res) {
  //         this.orderListTemp.length = 0
  //         res.rows.forEach((order: string[], i: number) => {
  //           this.orderListTemp.push({});

  //           order.forEach((val: string, j) => {
  //             this.orderListTemp[i][orderKey[j]] = val;
  //           });
  //         });
  //         this.orderListTemp = this.orderListTemp.filter(order => {
  //           return order.is_cancel_order === 'FALSE';
  //         })
  //         let bookedArr: any[] = []
  //         this.orderListTemp.forEach((order: any) => {

  //           bookedArr.push(order.stayDateRange);
  //         });

  //         observer.next(bookedArr)
  //         observer.complete()
  //       }
  //     });

  //     this.subList?.push(sub);
  //   })
  // }

  // // get sheet data 宿屋
  // getHomestayReservationData() {
  //   return new Observable(observer => {
  //     const orderType = OrderType.HOMESTAY;
  //     const sub = this.apiService.getCookerFilter(orderType).subscribe(res => {

  //       const orderKey: string[] = [
  //         'timestampUID',
  //         'order_id',
  //         'name',
  //         'phone',
  //         'email',
  //         'startDate',
  //         'endDate',
  //         'stayDateRange',
  //         'nights',
  //         'adults',
  //         'children',
  //         'add_bed',
  //         'add_breakfast',
  //         'request',
  //         'five_digits',
  //         'prepaid_fee',
  //         'is_paid_pre_fee',
  //         'rest_fee',
  //         'is_paid_rest_fee',
  //         'total_fee',
  //         'order_source',
  //         'is_cancel_order'
  //       ]

  //       if(res) {
  //         this.orderList.length = 0
  //         res.rows.forEach((order: string[], i: number) => {
  //           this.orderList.push({});

  //           order.forEach((val: string, j) => {
  //             this.orderList[i][orderKey[j]] = val;
  //           });
  //         });

  //         this.orderList = this.orderList.filter(order => {
  //           return order.is_cancel_order === 'FALSE';
  //         })
  //         let bookedArr: any[] = []
  //         this.orderList?.forEach((order: any) => {
  //           bookedArr.push(order.stayDateRange);
  //         });
  //         observer.next(bookedArr)
  //         observer.complete()
  //       }
  //     });

  //     this.subList?.push(sub);
  //   })
  // }

  // // get sheet data 宿屋
  // getTentReservationData(campArea: string) {
  //   return new Observable(observer => {
  //     const orderType = OrderType.TENT
  //     const sub = this.apiService.getCookerFilter(orderType).subscribe(res => {

  //       const orderKey: string[] = [
  //         'timestampUID',
  //         'order_id',
  //         'name',
  //         'campArea',
  //         'mobile',
  //         'email',
  //         'startDate',
  //         'endDate',
  //         'stayDateRange',
  //         'nights',
  //         'tents',
  //         'five_digits',
  //         'prepaid_fee',
  //         'is_paid_pre_fee',
  //         'rest_fee',
  //         'is_paid_rest_fee',
  //         'total_fee',
  //         'order_source',
  //         'is_cancel_order'
  //       ]

  //       if(res) {
  //         this.orderListTent.length = 0
  //         res.rows.forEach((order: string[], i: number) => {
  //           this.orderListTent.push({});

  //           order.forEach((val: string, j) => {
  //             this.orderListTent[i][orderKey[j]] = val;
  //           });
  //         });
  //         this.orderListTent = this.orderListTent.filter(order => {
  //           return order.is_cancel_order === 'FALSE';
  //         })

  //         let filteredList: any[] = [];
  //         let bookedArr: any[] = []

  //         filteredList = this.orderListTent.filter(order => {
  //           return campArea === order.campArea;
  //         })


  //         filteredList.forEach((order: any) => {

  //           bookedArr.push(order.stayDateRange);
  //         });
  //         observer.next(bookedArr)
  //         observer.complete()
  //       }
  //     });

  //     this.subList?.push(sub);
  //   })
  // }

  public toggleNights() {
    this.isToggleNights = !this.isToggleNights;
  }

  private getTS(bookedDate: string): IDate {
    const reg = /[^\(\)]+(?=\))/g
    const str = bookedDate.match(reg)
    const dateObj = String(str).split(',');

    const obj: IDate = {
      year: Number(dateObj[0]),
      month: Number(dateObj[1]),
      day: Number(dateObj[2]),
    }
    return obj;
  }

  public navigateTo(orderType: string, destination: string, isAvailable: boolean) {
    if(!isAvailable) {
      return;
    }

    if(orderType === OrderType.TENT) {
      this.router.navigate([`/start-date/${destination}/order`], {
        queryParams: {
          startDate: this.dateStart,
          leaveDate: this.dateLeave,
        }
      })

      return
    }

    if(orderType === OrderType.HOMESTAY) {
      this.router.navigate([`/homestay-start-date/homestay-order`], {
        queryParams: {
          startDate: this.dateStart,
          leaveDate: this.dateLeave,
        }
      })
      return
    }

    if(orderType === OrderType.TENT_HOMESTAY) {
      this.router.navigate([`tent-homestay-start-date/tent-homestay-order`], {
        queryParams: {
          startDate: this.dateStart,
          leaveDate: this.dateLeave,
        }
      })
      return
    }

    if(orderType === OrderType.TENT_A1_HOMESTAY) {
      this.router.navigate([`tent-a1-homestay-start-date/tent-a1-homestay-order`], {
        queryParams: {
          startDate: this.dateStart,
          leaveDate: this.dateLeave,
        }
      })
      return
    }
  }

}
