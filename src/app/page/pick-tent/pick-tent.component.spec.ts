import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PickTentComponent } from './pick-tent.component';

describe('PickTentComponent', () => {
  let component: PickTentComponent;
  let fixture: ComponentFixture<PickTentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PickTentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PickTentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
