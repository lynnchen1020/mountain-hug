import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PickTentComponent } from './pick-tent.component';

const routes: Routes = [
  {
    path: '',
    component: PickTentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PickTentRoutingModule { }
