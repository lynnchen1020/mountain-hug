
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { ShareModule } from 'src/app/module/share/share.module';
import { DirectivesModule } from 'src/app/directives/directives.module';


@NgModule({
  declarations: [HomeComponent],
  imports: [
    DirectivesModule,
    CommonModule,
    ShareModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
