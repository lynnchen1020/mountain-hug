import { ConfrimOrderComponent } from './../../components/modal/confrim-order/confrim-order.component';
import { Component, OnInit, HostListener, AfterViewInit, PLATFORM_ID, Inject, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ModalNewService } from 'src/app/service/modal-new.service';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { ElementService } from 'src/app/service/element.service';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('sectionEnv') sectionEnv!: ElementRef;
  @ViewChild('wrapper') wrapper!: ElementRef;

  faCoffee = faCoffee;
  showQuickBook = false;
  timeout: any;
  scrollListener: any;
  sectionEnvOffsetTop = 0

  constructor(
    @Inject(DOCUMENT) private document: Document,
    @Inject(PLATFORM_ID) private platformId: any,
    private modService: ModalNewService,
    public elementService: ElementService,) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    if(isPlatformBrowser(this.platformId)) {
      this.timeout = setTimeout(() => {
        this.sectionEnvOffsetTop = this.sectionEnv.nativeElement.getBoundingClientRect().top;
      }, 500)

      this.scrollListener = this.wrapper.nativeElement.addEventListener('scroll', () => {
        if (this.wrapper.nativeElement.scrollTop - (this.sectionEnvOffsetTop + 500) > 0) {
          this.showQuickBook = true;
        } else {
          this.showQuickBook = false;
        }
      })
    }

  }

  ngOnDestroy() {
    if(this.timeout) {
      clearTimeout(this.timeout);
    }
    if(this.scrollListener) {
      this.scrollListener.removeEventListener()
    }
  }

  openModal() {
    this.modService.open(ConfrimOrderComponent, {
      title: 'Hi',
      msg: 'no msg'
    })
  }

}
