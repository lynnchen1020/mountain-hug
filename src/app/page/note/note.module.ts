import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NoteRoutingModule } from './note-routing.module';
import { NoteComponent } from './note.component';
import { ShareModule } from 'src/app/module/share/share.module';


@NgModule({
  declarations: [NoteComponent],
  imports: [
    CommonModule,
    ShareModule,
    NoteRoutingModule
  ]
})
export class NoteModule { }
