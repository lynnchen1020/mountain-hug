import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderCampBComponent } from './order-camp-b.component';

const routes: Routes = [
  {
    path: '',
    component: OrderCampBComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderCampBRoutingModule { }
