import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { ElementService } from 'src/app/service/element.service';
import { ModalNewService } from 'src/app/service/modal-new.service';
import { OrderService } from 'src/app/service/order.service';
import { OrderCampComponent } from '../order-camp/order-camp.component';
import { ApiService } from 'src/app/service/api.service';
import { FeeType } from 'src/app/enum/order-type';

@Component({
  selector: 'app-order-camp-b',
  templateUrl: './order-camp-b.component.html',
  styleUrls: ['./order-camp-b.component.sass']
})
export class OrderCampBComponent extends OrderCampComponent implements OnInit, OnDestroy {
  constructor(
    router: Router,
    apiService: ApiService,
    route: ActivatedRoute,
    modService: ModalNewService,
    recaptchaV3Service: ReCaptchaV3Service,
    formBuilder: FormBuilder,
    cdref: ChangeDetectorRef,
    elementService: ElementService,
    orderService: OrderService) {
    super(router, apiService, route, modService, recaptchaV3Service, formBuilder, cdref, elementService, orderService)

    this.currentGalleryImg = "url('/assets/img/b/img_1.jpg')";
    this.gallery = [
      "url('/assets/img/b/img_1.jpg')",
      "url('/assets/img/b/img_2.jpg')",
      "url('/assets/img/b/img_3.jpg')",
      "url('/assets/img/b/img_4.jpg')",
      "url('/assets/img/b/img_5.jpg')",
      "url('/assets/img/b/img_6.jpg')",
      "url('/assets/img/b/img_7.jpg')",
      "url('/assets/img/b/img_8.jpg')",
      "url('/assets/img/b/img_9.jpg')",
      "url('/assets/img/b/img_10.jpg')",
      "url('/assets/img/b/img_11.jpg')",
      "url('/assets/img/b/img_12.jpg')",
      "url('/assets/img/b/img_13.jpg')",
      "url('/assets/img/b/img_14.jpg')",
    ];
  }

  override totalFee() {
    // 陣列裡有 5 6 就算假日
    const normalDaysFee = this.order.tents * FeeType.NORMAL * this.getStayOverDays(new Date(this.orderService.dateStart).getTime(), 'normal')
    const weekendDaysFee = this.order.tents * FeeType.WEEKEND * this.getStayOverDays(new Date(this.orderService.dateStart).getTime(), 'weekend')
    return (weekendDaysFee + normalDaysFee) || 0;
  }

  override tentsFee() {
    const normalDaysFee = this.order.tents * FeeType.NORMAL * this.getStayOverDays(new Date(this.orderService.dateStart).getTime(), 'normal')
    const weekendDaysFee = this.order.tents * FeeType.WEEKEND * this.getStayOverDays(new Date(this.orderService.dateStart).getTime(), 'weekend')
    return normalDaysFee + weekendDaysFee || 0
  }

  override ngOnInit(): void {
    super.ngOnInit();
  }

  override ngOnDestroy(): void {
    super.ngOnDestroy()
  }

}
