import { DirectivesModule } from './../../directives/directives.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderCampBRoutingModule } from './order-camp-b-routing.module';
import { OrderCampBComponent } from './order-camp-b.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecaptchaFormsModule, RecaptchaV3Module, RECAPTCHA_V3_SITE_KEY } from 'ng-recaptcha';
import { ShareModule } from 'src/app/module/share/share.module';


@NgModule({
  declarations: [OrderCampBComponent],
  imports: [
    DirectivesModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RecaptchaFormsModule,
    RecaptchaV3Module,
    ShareModule,
    OrderCampBRoutingModule
  ],
  providers: [
    {
      provide: RECAPTCHA_V3_SITE_KEY,
      useValue: '6LdpNoEeAAAAAECLDpl_gXgapOlI_c-IcB5n0w4j',
    },
  ]
})
export class OrderCampBModule { }
