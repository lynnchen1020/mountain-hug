import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderCampBComponent } from './order-camp-b.component';

describe('OrderCampBComponent', () => {
  let component: OrderCampBComponent;
  let fixture: ComponentFixture<OrderCampBComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderCampBComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderCampBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
