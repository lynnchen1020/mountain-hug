import { TestBed } from '@angular/core/testing';

import { HolidayResolver } from './holiday.resolver';

describe('HolidayResolver', () => {
  let resolver: HolidayResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(HolidayResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
