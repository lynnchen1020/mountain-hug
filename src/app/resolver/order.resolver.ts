import { OrderService } from 'src/app/service/order.service';
import { Injectable } from '@angular/core';
import {Resolve,
} from '@angular/router';
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderResolver implements Resolve<boolean> {
  constructor(private orderService: OrderService) {}
  resolve(): Observable<boolean> {
    return new Observable(subscriber => {
      subscriber.next()
      this.orderService.getUniqBookedDate(true)
    })
  }
}
