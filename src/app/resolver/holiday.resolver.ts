import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { Observable, filter, map, of, toArray } from 'rxjs';
import { ApiService } from '../service/api.service';

@Injectable({
  providedIn: 'root'
})
export class HolidayResolver implements Resolve<boolean> {
  constructor(private apiService: ApiService, private readonly router: Router) {

  }
  resolve(): Observable<boolean> {
    return new Observable(subsciber => {
      this.apiService.getHoliday().subscribe(list => {
        if(list) {
          subsciber.next(list)
          subsciber.complete()
        }
      })
    })
    // return this.apiService.getHoliday().pipe(
    //   toArray(),
    //     // Check if the array is not empty
    //   filter(list => list.length > 0),
    //   // Return a boolean value indicating whether there is a holiday
    //   map(list => {
    //     return list.length > 0})
    // )
  }
}
