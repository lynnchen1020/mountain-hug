import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

// function bootstrap() {
//   platformBrowserDynamic().bootstrapModule(AppModule)
//   .catch(err => console.error(err));
// };


// if (document.readyState === 'complete') {
//   bootstrap();
// } else {
//   document.addEventListener('DOMContentLoaded', bootstrap);
// }

// TODO: !!! Must test at no one's online, hydration flickering test start
// 1 - SSR version of the app loaded [content OK]
document.addEventListener('DOMContentLoaded', () => {
  // 2 - DOMContentLoaded triggers [content OK]
  setTimeout(() => {
    // 3 - Before AppModule get’s bootstrapped [First flick (partial)]
  }, 100);

  platformBrowserDynamic().bootstrapModule(AppModule)
  .then(() => {
    // 4 - Immediately after the AppModule get’s bootstrapped [Second flick (full)]

    setTimeout(() => {
      // 5 - Some instants after AppModule get’s bootstrapped [content OK]
    }, 300);
  })
  .catch(err => console.log(err));
});
// TODO: !!! Must test at no one's online, hydration flickering test end
