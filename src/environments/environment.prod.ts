export const environment = {
  production: true,
  appUrl: "https://www.mountainhug.com/",
  apiUrl: "https://back-mountain-hug.herokuapp.com"
};
